﻿
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace _10GuardarImagenes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnExaminar_Click(object sender, EventArgs e)
        {
            try
            {
                this.ofdFoto.ShowDialog();
                if (!ofdFoto.FileName.Equals(String.Empty))
                {
                    txtRuta.Text = ofdFoto.FileName;
                    imgFoto.Load(ofdFoto.FileName);
                }
               

            }
            catch (Exception )
            {
                MessageBox.Show($"No selecciono imagen.");

            }
            
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string cadenaConexion = string.Format("Data Source = K080PLAB111PC08; Initial Catalog = db_senati; User ID =senati; Password =senati");
            SqlConnection conexion = new SqlConnection(cadenaConexion);

            conexion.Open();
            
        string query = string.Format($"INSERT INTO IMAGENES VALUES(@nombreImg,@tamañoImg,@formatoImg,@rutaImg,@imagenImg)");
            SqlCommand command = new SqlCommand(query, conexion);

            //Definicion del tipo de dato de los parametros
            command.Parameters.Add("@nombreImg",SqlDbType.NVarChar);
            command.Parameters.Add("@tamañoImg", SqlDbType.NVarChar);
            command.Parameters.Add("@formatoImg", SqlDbType.NVarChar);
            command.Parameters.Add("@rutaImg", SqlDbType.NChar);
            command.Parameters.Add("@imagenImg", SqlDbType.Image);

            //Asignacion de valores a los parametros
            command.Parameters["@nombreImg"].Value = txtNombre.Text;
            command.Parameters["@tamañoImg"].Value = txtTamanio.Text;
            command.Parameters["@formatoImg"].Value = txtFormato.Text;
            command.Parameters["@rutaImg"].Value = txtRuta.Text;

            //Asignacion de memoria para conversion a bits de la imagen

            MemoryStream ms = new MemoryStream();
            imgFoto.Image.Save(ms,ImageFormat.Png);
            command.Parameters["@imagenImg"].Value = ms.GetBuffer();

            int  numFilasAfectadas=command.ExecuteNonQuery();

            if(numFilasAfectadas>0)
            {
                MessageBox.Show("Inserción de imagen correcta..."
                    , "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Error al cargar la imagen..."
                   , "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            conexion.Close();

            

        }

        private void btnCargar_Click(object sender, EventArgs e)
        {
            string cadenaConexion = string.Format("Data Source = K080PLAB111PC08; Initial Catalog = db_senati; User ID =senati; Password =senati");
            SqlConnection conexion = new SqlConnection(cadenaConexion);

            conexion.Open();

            int _idImg = int.Parse(txtID.Text);
            

            string query = string.Format($"SELECT ImagenImg from IMAGENES where idImg={_idImg}");

            SqlDataAdapter da = new SqlDataAdapter(query,conexion);
            DataSet ds = new DataSet();
            da.Fill(ds,"IMAGENES");
            byte[] datos = new byte[0];
            DataRow dr = ds.Tables["IMAGENES"].Rows[0];
            datos = (byte[])dr["imagenImg"];
            MemoryStream ms = new MemoryStream(datos);

            this.imgFoto.Image = System.Drawing.Bitmap.FromStream(ms);
            conexion.Close();


                 
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
           

        }

        private void activarIdImagenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtID.Enabled = true;
        }

        private void desactivarIdImagenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtID.Enabled = false;
        }

        private void imgFoto_Click(object sender, EventArgs e)
        {
            ofdFoto.Filter = "Archivo de imagen (JPEG) |*jpeg|Archivos JPG|*.jpg|Todos los archivos|*.*";

            DialogResult resultado = ofdFoto.ShowDialog();
            if(resultado==DialogResult.OK)
            {
                imgFoto.Image = Image.FromFile(ofdFoto.FileName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string SERVER = "35.202.148.81";
            string DATABASE= "db_peru";
            string USERID = "senati";
            string PASSWORD = "senati";
            

            string cadenaConexion = string.Format($"server={SERVER};database={DATABASE};userID={USERID};password={PASSWORD};sslmode=none;");

            MySqlConnection conexion = new MySqlConnection(cadenaConexion);
            


            conexion.Open();

            string _nombreImg = txtNombre.Text;
            string _tamanioImg = txtTamanio.Text;
            string _formatoImg = txtFormato.Text;
            string _rutaImg = txtRuta.Text;

            string query = string.Format($"INSERT INTO JHERRY_IMAGENES VALUES('0','{_nombreImg}','{_tamanioImg}','{_formatoImg}','{_rutaImg}',@imagenImg)");
            MySqlCommand command=new MySqlCommand(query, conexion);
            MemoryStream ms = new MemoryStream();

            imgFoto.Image.Save(ms,ImageFormat.Jpeg);
            byte[] aByte = ms.ToArray();
            command.Parameters.AddWithValue("imagenImg", aByte);

            int i = command.ExecuteNonQuery();
            conexion.Close();
        }
    }
}
