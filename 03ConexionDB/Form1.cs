﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03ConexionDB
{
    public partial class FrmConexion : Form
    {
        public FrmConexion()
        {
            InitializeComponent();
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            try
            {
                string server = txtHost.Text;
                string database = txtDatos.Text;
                string userID = txtUsuario.Text;
                string password = txtContraseña.Text;

                MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
                builder.Server = server;
                builder.Database = database;
                builder.UserID = userID;
                builder.Password = password;
                builder.SslMode = 0;

                string cadena = builder.ToString();

                MySqlConnection conexion = new MySqlConnection(cadena);
                conexion.Open();

                MessageBox.Show("Conexion exitosa.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex )
            {
                MessageBox.Show($"Se ha producido un error de conexion{ex.Message}", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
               
            }
        }

        private void FrmConexion_Load(object sender, EventArgs e)
        {

        }

        private void btnDesconectar_Click(object sender, EventArgs e)
        {

        }
    }
}
