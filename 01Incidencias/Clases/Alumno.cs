﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01Incidencias.Clases
{
    public class Alumno : Persona
    {
        private string semestreAlum;
        private int cantCursosAlumn;
        private double notaLabAlumn;
        private double notaTeoriaAlum;

        public Alumno(string codPersona, string apepatPersona, string apematPersona, string dniPersona, char sexoPersona, DateTime fecNacPersona, byte fotoPersona, string semestreAlum, int cantCursosAlumn, double notaLabAlumn, double notaTeoriaAlum)
            : base(codPersona, apepatPersona, apematPersona, dniPersona, sexoPersona, fecNacPersona, fotoPersona)
        {
            this.SemestreAlum = semestreAlum;
            this.CantCursosAlumn = cantCursosAlumn;
            this.NotaLabAlumn = notaLabAlumn;
            this.NotaTeoriaAlum = notaTeoriaAlum;
        }

        public string SemestreAlum { get => semestreAlum; set => semestreAlum = value; }
        public int CantCursosAlumn { get => cantCursosAlumn; set => cantCursosAlumn = value; }
        public double NotaLabAlumn { get => notaLabAlumn; set => notaLabAlumn = value; }
        public double NotaTeoriaAlum { get => notaTeoriaAlum; set => notaTeoriaAlum = value; }
    }
}
