﻿namespace _02CasoIncidencias.Formularios
{
    partial class FrmAlumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDniAlum = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.rbSexoAlumF = new System.Windows.Forms.RadioButton();
            this.rbSexoAlumM = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.txtYearOfBirthAlum = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEdadAlum = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNomAlum = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtApematAlum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtApepatAlum = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCodAlum = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtCursoMatAlum = new System.Windows.Forms.TextBox();
            this.cboSemestreAlum = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtPromFinalAlum = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNotaActitudAlum = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtExamenParcialAlum = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNotaPracticaAlum = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnRegistrarAlum = new System.Windows.Forms.Button();
            this.btnCancelarAlum = new System.Windows.Forms.Button();
            this.btnSalirAlumno = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtDniAlum);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.rbSexoAlumF);
            this.groupBox1.Controls.Add(this.rbSexoAlumM);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtYearOfBirthAlum);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtEdadAlum);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtNomAlum);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtApematAlum);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtApepatAlum);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtCodAlum);
            this.groupBox1.Location = new System.Drawing.Point(29, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(495, 335);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(307, 249);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 17);
            this.label15.TabIndex = 32;
            this.label15.Text = "(1918-2018)";
            // 
            // txtDniAlum
            // 
            this.txtDniAlum.Location = new System.Drawing.Point(191, 180);
            this.txtDniAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtDniAlum.MaxLength = 8;
            this.txtDniAlum.Name = "txtDniAlum";
            this.txtDniAlum.Size = new System.Drawing.Size(107, 22);
            this.txtDniAlum.TabIndex = 31;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(51, 183);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 17);
            this.label14.TabIndex = 30;
            this.label14.Text = "DNI:";
            // 
            // rbSexoAlumF
            // 
            this.rbSexoAlumF.AutoSize = true;
            this.rbSexoAlumF.Location = new System.Drawing.Point(257, 206);
            this.rbSexoAlumF.Margin = new System.Windows.Forms.Padding(4);
            this.rbSexoAlumF.Name = "rbSexoAlumF";
            this.rbSexoAlumF.Size = new System.Drawing.Size(37, 21);
            this.rbSexoAlumF.TabIndex = 29;
            this.rbSexoAlumF.Text = "F";
            this.rbSexoAlumF.UseVisualStyleBackColor = true;
            // 
            // rbSexoAlumM
            // 
            this.rbSexoAlumM.AutoSize = true;
            this.rbSexoAlumM.Checked = true;
            this.rbSexoAlumM.Location = new System.Drawing.Point(191, 206);
            this.rbSexoAlumM.Margin = new System.Windows.Forms.Padding(4);
            this.rbSexoAlumM.Name = "rbSexoAlumM";
            this.rbSexoAlumM.Size = new System.Drawing.Size(40, 21);
            this.rbSexoAlumM.TabIndex = 28;
            this.rbSexoAlumM.TabStop = true;
            this.rbSexoAlumM.Text = "M";
            this.rbSexoAlumM.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(51, 284);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 17);
            this.label7.TabIndex = 27;
            this.label7.Text = "Edad:";
            // 
            // txtYearOfBirthAlum
            // 
            this.txtYearOfBirthAlum.Location = new System.Drawing.Point(191, 245);
            this.txtYearOfBirthAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtYearOfBirthAlum.MaxLength = 4;
            this.txtYearOfBirthAlum.MinimumSize = new System.Drawing.Size(4, 4);
            this.txtYearOfBirthAlum.Name = "txtYearOfBirthAlum";
            this.txtYearOfBirthAlum.Size = new System.Drawing.Size(107, 22);
            this.txtYearOfBirthAlum.TabIndex = 26;
            this.txtYearOfBirthAlum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYearOfBirthAlum_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(51, 249);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 17);
            this.label6.TabIndex = 25;
            this.label6.Text = "Año Nacimiento:";
            // 
            // txtEdadAlum
            // 
            this.txtEdadAlum.Enabled = false;
            this.txtEdadAlum.Location = new System.Drawing.Point(191, 281);
            this.txtEdadAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtEdadAlum.Name = "txtEdadAlum";
            this.txtEdadAlum.Size = new System.Drawing.Size(107, 22);
            this.txtEdadAlum.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(51, 212);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 17);
            this.label5.TabIndex = 23;
            this.label5.Text = "Sexo:";
            // 
            // txtNomAlum
            // 
            this.txtNomAlum.Location = new System.Drawing.Point(191, 148);
            this.txtNomAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtNomAlum.Name = "txtNomAlum";
            this.txtNomAlum.Size = new System.Drawing.Size(273, 22);
            this.txtNomAlum.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 151);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 21;
            this.label4.Text = "Nombres:";
            // 
            // txtApematAlum
            // 
            this.txtApematAlum.Location = new System.Drawing.Point(191, 107);
            this.txtApematAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtApematAlum.Name = "txtApematAlum";
            this.txtApematAlum.Size = new System.Drawing.Size(273, 22);
            this.txtApematAlum.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 111);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 17);
            this.label3.TabIndex = 19;
            this.label3.Text = "Apellido Materno:";
            // 
            // txtApepatAlum
            // 
            this.txtApepatAlum.Location = new System.Drawing.Point(191, 71);
            this.txtApepatAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtApepatAlum.Name = "txtApepatAlum";
            this.txtApepatAlum.Size = new System.Drawing.Size(273, 22);
            this.txtApepatAlum.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 75);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "Apellido Paterno:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Codigo (ID):";
            // 
            // txtCodAlum
            // 
            this.txtCodAlum.Enabled = false;
            this.txtCodAlum.Location = new System.Drawing.Point(191, 37);
            this.txtCodAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtCodAlum.Name = "txtCodAlum";
            this.txtCodAlum.Size = new System.Drawing.Size(132, 22);
            this.txtCodAlum.TabIndex = 15;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCursoMatAlum);
            this.groupBox2.Controls.Add(this.cboSemestreAlum);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(580, 23);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(459, 142);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos Academicos";
            // 
            // txtCursoMatAlum
            // 
            this.txtCursoMatAlum.Location = new System.Drawing.Point(199, 71);
            this.txtCursoMatAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtCursoMatAlum.Name = "txtCursoMatAlum";
            this.txtCursoMatAlum.Size = new System.Drawing.Size(219, 22);
            this.txtCursoMatAlum.TabIndex = 30;
            // 
            // cboSemestreAlum
            // 
            this.cboSemestreAlum.FormattingEnabled = true;
            this.cboSemestreAlum.Location = new System.Drawing.Point(199, 30);
            this.cboSemestreAlum.Margin = new System.Windows.Forms.Padding(4);
            this.cboSemestreAlum.Name = "cboSemestreAlum";
            this.cboSemestreAlum.Size = new System.Drawing.Size(219, 24);
            this.cboSemestreAlum.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 71);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "Curso Matriculado:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(33, 37);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Semestre:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtPromFinalAlum);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtNotaActitudAlum);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtExamenParcialAlum);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtNotaPracticaAlum);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(580, 177);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(459, 172);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Calificaciones";
            // 
            // txtPromFinalAlum
            // 
            this.txtPromFinalAlum.Enabled = false;
            this.txtPromFinalAlum.Location = new System.Drawing.Point(173, 126);
            this.txtPromFinalAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtPromFinalAlum.Name = "txtPromFinalAlum";
            this.txtPromFinalAlum.Size = new System.Drawing.Size(107, 22);
            this.txtPromFinalAlum.TabIndex = 37;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(33, 129);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 17);
            this.label13.TabIndex = 36;
            this.label13.Text = "Promedio Final:";
            // 
            // txtNotaActitudAlum
            // 
            this.txtNotaActitudAlum.Location = new System.Drawing.Point(173, 94);
            this.txtNotaActitudAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtNotaActitudAlum.Name = "txtNotaActitudAlum";
            this.txtNotaActitudAlum.Size = new System.Drawing.Size(107, 22);
            this.txtNotaActitudAlum.TabIndex = 35;
            this.txtNotaActitudAlum.TextChanged += new System.EventHandler(this.txtNotaActitudAlum_TextChanged);
            this.txtNotaActitudAlum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNotaActitudAlum_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(33, 97);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 17);
            this.label12.TabIndex = 34;
            this.label12.Text = "Nota Actitudinal:";
            // 
            // txtExamenParcialAlum
            // 
            this.txtExamenParcialAlum.Location = new System.Drawing.Point(173, 62);
            this.txtExamenParcialAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtExamenParcialAlum.Name = "txtExamenParcialAlum";
            this.txtExamenParcialAlum.Size = new System.Drawing.Size(107, 22);
            this.txtExamenParcialAlum.TabIndex = 33;
            this.txtExamenParcialAlum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtExamenParcialAlum_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(33, 65);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 17);
            this.label11.TabIndex = 32;
            this.label11.Text = "Examen Parcial:";
            // 
            // txtNotaPracticaAlum
            // 
            this.txtNotaPracticaAlum.Location = new System.Drawing.Point(173, 30);
            this.txtNotaPracticaAlum.Margin = new System.Windows.Forms.Padding(4);
            this.txtNotaPracticaAlum.Name = "txtNotaPracticaAlum";
            this.txtNotaPracticaAlum.Size = new System.Drawing.Size(107, 22);
            this.txtNotaPracticaAlum.TabIndex = 31;
            this.txtNotaPracticaAlum.TextChanged += new System.EventHandler(this.txtNotaPracticaAlum_TextChanged);
            this.txtNotaPracticaAlum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNotaPracticaAlum_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(33, 33);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 17);
            this.label10.TabIndex = 30;
            this.label10.Text = "Nota Practica:";
            // 
            // btnRegistrarAlum
            // 
            this.btnRegistrarAlum.Location = new System.Drawing.Point(395, 384);
            this.btnRegistrarAlum.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegistrarAlum.Name = "btnRegistrarAlum";
            this.btnRegistrarAlum.Size = new System.Drawing.Size(100, 28);
            this.btnRegistrarAlum.TabIndex = 19;
            this.btnRegistrarAlum.Text = "Registrar";
            this.btnRegistrarAlum.UseVisualStyleBackColor = true;
            this.btnRegistrarAlum.Click += new System.EventHandler(this.btnRegistrarAlum_Click);
            // 
            // btnCancelarAlum
            // 
            this.btnCancelarAlum.Location = new System.Drawing.Point(509, 384);
            this.btnCancelarAlum.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancelarAlum.Name = "btnCancelarAlum";
            this.btnCancelarAlum.Size = new System.Drawing.Size(100, 28);
            this.btnCancelarAlum.TabIndex = 20;
            this.btnCancelarAlum.Text = "Cancelar";
            this.btnCancelarAlum.UseVisualStyleBackColor = true;
            // 
            // btnSalirAlumno
            // 
            this.btnSalirAlumno.Location = new System.Drawing.Point(627, 384);
            this.btnSalirAlumno.Margin = new System.Windows.Forms.Padding(4);
            this.btnSalirAlumno.Name = "btnSalirAlumno";
            this.btnSalirAlumno.Size = new System.Drawing.Size(100, 28);
            this.btnSalirAlumno.TabIndex = 21;
            this.btnSalirAlumno.Text = "Salir";
            this.btnSalirAlumno.UseVisualStyleBackColor = true;
            this.btnSalirAlumno.Click += new System.EventHandler(this.btnSalirAlumno_Click);
            // 
            // FrmAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 449);
            this.Controls.Add(this.btnSalirAlumno);
            this.Controls.Add(this.btnCancelarAlum);
            this.Controls.Add(this.btnRegistrarAlum);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmAlumnos";
            this.Text = "Registrar nuevos alumnos";
            this.Load += new System.EventHandler(this.FrmAlumnos_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbSexoAlumF;
        private System.Windows.Forms.RadioButton rbSexoAlumM;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtYearOfBirthAlum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEdadAlum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNomAlum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtApematAlum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtApepatAlum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCodAlum;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtCursoMatAlum;
        private System.Windows.Forms.ComboBox cboSemestreAlum;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtPromFinalAlum;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNotaActitudAlum;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtExamenParcialAlum;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNotaPracticaAlum;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnRegistrarAlum;
        private System.Windows.Forms.Button btnCancelarAlum;
        private System.Windows.Forms.Button btnSalirAlumno;
        private System.Windows.Forms.TextBox txtDniAlum;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
    }
}