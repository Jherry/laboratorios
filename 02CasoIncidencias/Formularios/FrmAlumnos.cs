﻿using _02CasoIncidencias.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02CasoIncidencias.Formularios
{
    public partial class FrmAlumnos : Form
    {
        public FrmAlumnos()
        {
            InitializeComponent();
        }

        private void btnSalirAlumno_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void FrmAlumnos_Load(object sender, EventArgs e)
        {
            //Cargar Semestres
            cboSemestreAlum.Items.Add("--Seleccione opción--");
            cboSemestreAlum.Items.Add("I");
            cboSemestreAlum.Items.Add("II");
            cboSemestreAlum.Items.Add("III");
            cboSemestreAlum.Items.Add("IV");
            cboSemestreAlum.Items.Add("V");
            cboSemestreAlum.Items.Add("VI");
            cboSemestreAlum.SelectedIndex = 0;
        }


        private void btnRegistrarAlum_Click(object sender, EventArgs e)
        {

            string _apepatAlum = txtApepatAlum.Text;
            string _apematAlum = txtApematAlum.Text;
            string _nomAlumn = txtNomAlum.Text;
            char _sexoAlum = (rbSexoAlumM.Checked?'M':'F'); //Operador ternario
            string _dniAlum = txtDniAlum.Text;
            int _yearOfBirth = int.Parse(txtYearOfBirthAlum.Text);
            int _edad = int.Parse(txtEdadAlum.Text);
            string _semestreAlum = cboSemestreAlum.SelectedItem.ToString();
            string _cursoMatAlum = txtCursoMatAlum.Text;
            double _notaPracticaAlum = double.Parse(txtNotaPracticaAlum.Text);
            double _notaActitudAlum = double.Parse(txtNotaActitudAlum.Text);
            double _examenParcial = double.Parse(txtExamenParcialAlum.Text);
            double _promFinalAlum = double.Parse(txtPromFinalAlum.Text);


            //generarCodigo(); //Generar el codigo
            string _codAlumno = Utils.getSecuencial(_apepatAlum, _apematAlum, _nomAlumn, Utils.PREFIJO_ALUM);
            txtCodAlum.Text = _codAlumno;

            //Instanciando el objeto alumno
            Alumno alumno = new Alumno(
                _codAlumno, _apepatAlum, _apematAlum, _nomAlumn, _sexoAlum, _dniAlum, _yearOfBirth, _edad, _semestreAlum,
                _cursoMatAlum, _notaPracticaAlum, _notaActitudAlum, _examenParcial, _promFinalAlum);

            DatosPersona.addAlumno(alumno); //Agrega el alumno creado
            MessageBox.Show("Alumno agregado con exito", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void txtYearOfBirthAlum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (Char)Keys.Enter))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            else
            {
                if (this.txtYearOfBirthAlum.Text.Length == 4)
                {
                   int yearOfBirth = int.Parse(txtYearOfBirthAlum.Text);
                   int edad = Utils.getEdad(yearOfBirth);

                    if (edad==-1) //-1: representa a error
                    {
                        MessageBox.Show("Verifique nuevamente su año de nacimiento","Advertencia", MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                        txtYearOfBirthAlum.Clear();
                        txtEdadAlum.Clear();
                        txtYearOfBirthAlum.Focus();
                    }
                    else
                    {
                        txtEdadAlum.Text = edad.ToString();
                    }
                }
            }
        }

        private void txtNotaPracticaAlum_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNotaPracticaAlum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (Char)Keys.Enter) && (e.KeyChar!=Utils.PUNTO))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtExamenParcialAlum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (Char)Keys.Enter) && (e.KeyChar != Utils.PUNTO))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtNotaActitudAlum_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNotaActitudAlum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (Char)Keys.Enter) && (e.KeyChar != Utils.PUNTO))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            else
            {
                if (this.txtNotaActitudAlum.Text.Length >= 2)
                {
                    double _notapractica = double.Parse(txtNotaPracticaAlum.Text);
                    double _notateoria = double.Parse(txtNotaActitudAlum.Text);
                    double _notaparcial = double.Parse(txtExamenParcialAlum.Text);

                    double promedio = (_notaparcial+_notapractica+_notateoria) / 3;
                    txtPromFinalAlum.Text = promedio.ToString("n2"); //<- Formato de 2 decimales
                }
            }
        }
    }
}
