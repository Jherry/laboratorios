﻿namespace _02CasoIncidencias.Formularios
{
    partial class FrmInstructores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbSexoInstF = new System.Windows.Forms.RadioButton();
            this.rbSexoInstM = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.txtYearOldInst = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEdadInst = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNomInst = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtApematInst = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtApepatInst = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCodInst = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtEscolaridadInst = new System.Windows.Forms.TextBox();
            this.txtSueldoTotalInst = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtHorasSemanaInst = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSalarioHoraInst = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cboTipoContratoInst = new System.Windows.Forms.ComboBox();
            this.cboEspecialidadInst = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCancelarInst = new System.Windows.Forms.Button();
            this.btnRegistrarInst = new System.Windows.Forms.Button();
            this.btnSalirInst = new System.Windows.Forms.Button();
            this.txtDniInst = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDniInst);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.rbSexoInstF);
            this.groupBox1.Controls.Add(this.rbSexoInstM);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtYearOldInst);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtEdadInst);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtNomInst);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtApematInst);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtApepatInst);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtCodInst);
            this.groupBox1.Location = new System.Drawing.Point(39, 34);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(481, 319);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // rbSexoInstF
            // 
            this.rbSexoInstF.AutoSize = true;
            this.rbSexoInstF.Location = new System.Drawing.Point(237, 208);
            this.rbSexoInstF.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbSexoInstF.Name = "rbSexoInstF";
            this.rbSexoInstF.Size = new System.Drawing.Size(37, 21);
            this.rbSexoInstF.TabIndex = 29;
            this.rbSexoInstF.Text = "F";
            this.rbSexoInstF.UseVisualStyleBackColor = true;
            // 
            // rbSexoInstM
            // 
            this.rbSexoInstM.AutoSize = true;
            this.rbSexoInstM.Checked = true;
            this.rbSexoInstM.Location = new System.Drawing.Point(171, 208);
            this.rbSexoInstM.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbSexoInstM.Name = "rbSexoInstM";
            this.rbSexoInstM.Size = new System.Drawing.Size(40, 21);
            this.rbSexoInstM.TabIndex = 28;
            this.rbSexoInstM.TabStop = true;
            this.rbSexoInstM.Text = "M";
            this.rbSexoInstM.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 280);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 17);
            this.label7.TabIndex = 27;
            this.label7.Text = "Edad:";
            // 
            // txtYearOldInst
            // 
            this.txtYearOldInst.Location = new System.Drawing.Point(171, 244);
            this.txtYearOldInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtYearOldInst.Name = "txtYearOldInst";
            this.txtYearOldInst.Size = new System.Drawing.Size(107, 22);
            this.txtYearOldInst.TabIndex = 26;
            this.txtYearOldInst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYearOldInst_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 248);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 17);
            this.label6.TabIndex = 25;
            this.label6.Text = "Año Nacimiento:";
            // 
            // txtEdadInst
            // 
            this.txtEdadInst.Enabled = false;
            this.txtEdadInst.Location = new System.Drawing.Point(171, 276);
            this.txtEdadInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEdadInst.Name = "txtEdadInst";
            this.txtEdadInst.Size = new System.Drawing.Size(107, 22);
            this.txtEdadInst.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 215);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 17);
            this.label5.TabIndex = 23;
            this.label5.Text = "Sexo:";
            // 
            // txtNomInst
            // 
            this.txtNomInst.Location = new System.Drawing.Point(171, 138);
            this.txtNomInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNomInst.Name = "txtNomInst";
            this.txtNomInst.Size = new System.Drawing.Size(273, 22);
            this.txtNomInst.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 142);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 21;
            this.label4.Text = "Nombres:";
            // 
            // txtApematInst
            // 
            this.txtApematInst.Location = new System.Drawing.Point(171, 107);
            this.txtApematInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtApematInst.Name = "txtApematInst";
            this.txtApematInst.Size = new System.Drawing.Size(273, 22);
            this.txtApematInst.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 111);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 17);
            this.label3.TabIndex = 19;
            this.label3.Text = "Apellido Materno:";
            // 
            // txtApepatInst
            // 
            this.txtApepatInst.Location = new System.Drawing.Point(171, 71);
            this.txtApepatInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtApepatInst.Name = "txtApepatInst";
            this.txtApepatInst.Size = new System.Drawing.Size(273, 22);
            this.txtApepatInst.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 75);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "Apellido Persona:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Codigo (ID):";
            // 
            // txtCodInst
            // 
            this.txtCodInst.Enabled = false;
            this.txtCodInst.Location = new System.Drawing.Point(171, 37);
            this.txtCodInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCodInst.Name = "txtCodInst";
            this.txtCodInst.Size = new System.Drawing.Size(132, 22);
            this.txtCodInst.TabIndex = 15;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtEscolaridadInst);
            this.groupBox2.Controls.Add(this.txtSueldoTotalInst);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtHorasSemanaInst);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtSalarioHoraInst);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.cboTipoContratoInst);
            this.groupBox2.Controls.Add(this.cboEspecialidadInst);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(547, 34);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(467, 302);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos Academicos";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(157, 218);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(25, 17);
            this.label16.TabIndex = 42;
            this.label16.Text = "S/.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(157, 181);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 17);
            this.label15.TabIndex = 41;
            this.label15.Text = "S/.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(157, 151);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(25, 17);
            this.label14.TabIndex = 40;
            this.label14.Text = "S/.";
            // 
            // txtEscolaridadInst
            // 
            this.txtEscolaridadInst.Enabled = false;
            this.txtEscolaridadInst.Location = new System.Drawing.Point(194, 178);
            this.txtEscolaridadInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEscolaridadInst.Name = "txtEscolaridadInst";
            this.txtEscolaridadInst.Size = new System.Drawing.Size(92, 22);
            this.txtEscolaridadInst.TabIndex = 39;
            this.txtEscolaridadInst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEscolaridadInst_KeyPress);
            // 
            // txtSueldoTotalInst
            // 
            this.txtSueldoTotalInst.Enabled = false;
            this.txtSueldoTotalInst.Location = new System.Drawing.Point(194, 213);
            this.txtSueldoTotalInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSueldoTotalInst.Name = "txtSueldoTotalInst";
            this.txtSueldoTotalInst.Size = new System.Drawing.Size(92, 22);
            this.txtSueldoTotalInst.TabIndex = 38;
            this.txtSueldoTotalInst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSueldoTotalInst_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(32, 219);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 17);
            this.label13.TabIndex = 37;
            this.label13.Text = "Sueldo Total:";
            // 
            // txtHorasSemanaInst
            // 
            this.txtHorasSemanaInst.Location = new System.Drawing.Point(194, 113);
            this.txtHorasSemanaInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtHorasSemanaInst.Name = "txtHorasSemanaInst";
            this.txtHorasSemanaInst.Size = new System.Drawing.Size(92, 22);
            this.txtHorasSemanaInst.TabIndex = 31;
            this.txtHorasSemanaInst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHorasSemanaInst_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(32, 186);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 17);
            this.label12.TabIndex = 36;
            this.label12.Text = "Escolaridad (8%):";
            // 
            // txtSalarioHoraInst
            // 
            this.txtSalarioHoraInst.Location = new System.Drawing.Point(194, 146);
            this.txtSalarioHoraInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSalarioHoraInst.Name = "txtSalarioHoraInst";
            this.txtSalarioHoraInst.Size = new System.Drawing.Size(92, 22);
            this.txtSalarioHoraInst.TabIndex = 30;
            this.txtSalarioHoraInst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalarioHoraInst_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(32, 151);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 17);
            this.label11.TabIndex = 35;
            this.label11.Text = "Salario Hora:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(32, 116);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 17);
            this.label10.TabIndex = 34;
            this.label10.Text = "Horas Semana:";
            // 
            // cboTipoContratoInst
            // 
            this.cboTipoContratoInst.FormattingEnabled = true;
            this.cboTipoContratoInst.Location = new System.Drawing.Point(160, 70);
            this.cboTipoContratoInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboTipoContratoInst.Name = "cboTipoContratoInst";
            this.cboTipoContratoInst.Size = new System.Drawing.Size(265, 24);
            this.cboTipoContratoInst.TabIndex = 33;
            // 
            // cboEspecialidadInst
            // 
            this.cboEspecialidadInst.FormattingEnabled = true;
            this.cboEspecialidadInst.Location = new System.Drawing.Point(160, 34);
            this.cboEspecialidadInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboEspecialidadInst.Name = "cboEspecialidadInst";
            this.cboEspecialidadInst.Size = new System.Drawing.Size(265, 24);
            this.cboEspecialidadInst.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(32, 70);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 17);
            this.label8.TabIndex = 31;
            this.label8.Text = "Tipo Contrato:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 42);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 17);
            this.label9.TabIndex = 30;
            this.label9.Text = "Especialidad:";
            // 
            // btnCancelarInst
            // 
            this.btnCancelarInst.Location = new System.Drawing.Point(491, 361);
            this.btnCancelarInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancelarInst.Name = "btnCancelarInst";
            this.btnCancelarInst.Size = new System.Drawing.Size(100, 28);
            this.btnCancelarInst.TabIndex = 31;
            this.btnCancelarInst.Text = "Cancelar";
            this.btnCancelarInst.UseVisualStyleBackColor = true;
            // 
            // btnRegistrarInst
            // 
            this.btnRegistrarInst.Location = new System.Drawing.Point(379, 361);
            this.btnRegistrarInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRegistrarInst.Name = "btnRegistrarInst";
            this.btnRegistrarInst.Size = new System.Drawing.Size(100, 28);
            this.btnRegistrarInst.TabIndex = 30;
            this.btnRegistrarInst.Text = "Registrar";
            this.btnRegistrarInst.UseVisualStyleBackColor = true;
            this.btnRegistrarInst.Click += new System.EventHandler(this.btnRegistrarInst_Click);
            // 
            // btnSalirInst
            // 
            this.btnSalirInst.Location = new System.Drawing.Point(601, 361);
            this.btnSalirInst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSalirInst.Name = "btnSalirInst";
            this.btnSalirInst.Size = new System.Drawing.Size(100, 28);
            this.btnSalirInst.TabIndex = 33;
            this.btnSalirInst.Text = "Salir";
            this.btnSalirInst.UseVisualStyleBackColor = true;
            this.btnSalirInst.Click += new System.EventHandler(this.btnSalirInst_Click);
            // 
            // txtDniInst
            // 
            this.txtDniInst.Location = new System.Drawing.Point(171, 174);
            this.txtDniInst.Margin = new System.Windows.Forms.Padding(4);
            this.txtDniInst.MaxLength = 8;
            this.txtDniInst.Name = "txtDniInst";
            this.txtDniInst.Size = new System.Drawing.Size(107, 22);
            this.txtDniInst.TabIndex = 33;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(31, 177);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 17);
            this.label17.TabIndex = 32;
            this.label17.Text = "DNI:";
            // 
            // FrmInstructores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 434);
            this.Controls.Add(this.btnSalirInst);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCancelarInst);
            this.Controls.Add(this.btnRegistrarInst);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmInstructores";
            this.Text = "Registrar nuevos instructores";
            this.Load += new System.EventHandler(this.FrmInstructores_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbSexoInstF;
        private System.Windows.Forms.RadioButton rbSexoInstM;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtYearOldInst;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEdadInst;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNomInst;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtApematInst;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtApepatInst;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCodInst;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtEscolaridadInst;
        private System.Windows.Forms.TextBox txtSueldoTotalInst;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtHorasSemanaInst;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSalarioHoraInst;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboTipoContratoInst;
        private System.Windows.Forms.ComboBox cboEspecialidadInst;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnCancelarInst;
        private System.Windows.Forms.Button btnRegistrarInst;
        private System.Windows.Forms.Button btnSalirInst;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtDniInst;
        private System.Windows.Forms.Label label17;
    }
}