﻿using _02CasoIncidencias.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02_CasoIncidencias.Formularios
{
    public partial class frmDatosAlumnos : Form
    {
        public frmDatosAlumnos()
        {
            InitializeComponent();
        }
        static int numeroPersona = 0;
        private void frmDatosAlumnos_Load(object sender, EventArgs e)
        {
            foreach (Alumno alumno in DatosPersona.obtenerAlumnos())
            {
                dgvDatosAlumnos.Rows.Add(alumno.CodPersona, alumno.ApepatPersona,alumno.ApematPersona, alumno.NomPersona,
                    alumno.SexoPersona, alumno.DniPersona, alumno.YearOfBirthPersona,alumno.EdadPersona,
                    alumno.SemestreAlum,alumno.CursoMatAlum,alumno.NotaPracticaAlum,alumno.NotaActitudAlum,
                    alumno.ExamenParcialAlum,alumno.PromFinalAlum);
            }
            
        }
    }
}
