﻿using _02CasoIncidencias.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02_CasoIncidencias.Formularios
{
    public partial class frmDatosInstructores : Form
    {
        public frmDatosInstructores()
        {
            InitializeComponent();
        }

        private void frmDatosInstructores_Load(object sender, EventArgs e)
        {
            foreach (Instructor instructor in DatosPersona.obtenerInstructores())
            {
                dgvDatosInstructores.Rows.Add(instructor.CodPersona, instructor.ApepatPersona,
                    instructor.ApematPersona, instructor.NomPersona, instructor.SexoPersona,
                    instructor.DniPersona, instructor.YearOfBirthPersona, instructor.EdadPersona,
                    instructor.EspecialidadInst, instructor.TipoContratoInst, instructor.HorasSemanaInst,
                    instructor.SalarioHoraInst, instructor.EscolaridadInst,instructor.SueldoTotalInst);
            }
        }
    }
}
