﻿namespace _02_CasoIncidencias.Formularios
{
    partial class frmDatosInstructores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDatosInstructores = new System.Windows.Forms.DataGridView();
            this.codInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apePatInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apeMatInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sexoInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dniInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yearOfBirthInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.edadInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.especialidadInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoContratoInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horasSemanaInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salarioHorasInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.escolaridadInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sueldoTotalInst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosInstructores)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDatosInstructores
            // 
            this.dgvDatosInstructores.AllowUserToAddRows = false;
            this.dgvDatosInstructores.AllowUserToDeleteRows = false;
            this.dgvDatosInstructores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosInstructores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codInst,
            this.apePatInst,
            this.apeMatInst,
            this.nomInst,
            this.sexoInst,
            this.dniInst,
            this.yearOfBirthInst,
            this.edadInst,
            this.especialidadInst,
            this.tipoContratoInst,
            this.horasSemanaInst,
            this.salarioHorasInst,
            this.escolaridadInst,
            this.sueldoTotalInst});
            this.dgvDatosInstructores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDatosInstructores.Location = new System.Drawing.Point(0, 0);
            this.dgvDatosInstructores.Name = "dgvDatosInstructores";
            this.dgvDatosInstructores.ReadOnly = true;
            this.dgvDatosInstructores.RowTemplate.Height = 24;
            this.dgvDatosInstructores.Size = new System.Drawing.Size(1504, 561);
            this.dgvDatosInstructores.TabIndex = 1;
            // 
            // codInst
            // 
            this.codInst.HeaderText = "COD INSTRUCTOR";
            this.codInst.Name = "codInst";
            this.codInst.ReadOnly = true;
            this.codInst.Width = 120;
            // 
            // apePatInst
            // 
            this.apePatInst.HeaderText = "APELLIDO PATERNO";
            this.apePatInst.Name = "apePatInst";
            this.apePatInst.ReadOnly = true;
            // 
            // apeMatInst
            // 
            this.apeMatInst.HeaderText = "APELLIDO MATERNO";
            this.apeMatInst.Name = "apeMatInst";
            this.apeMatInst.ReadOnly = true;
            // 
            // nomInst
            // 
            this.nomInst.HeaderText = "NOMBRES";
            this.nomInst.Name = "nomInst";
            this.nomInst.ReadOnly = true;
            // 
            // sexoInst
            // 
            this.sexoInst.HeaderText = "SEXO";
            this.sexoInst.Name = "sexoInst";
            this.sexoInst.ReadOnly = true;
            // 
            // dniInst
            // 
            this.dniInst.HeaderText = "DNI";
            this.dniInst.Name = "dniInst";
            this.dniInst.ReadOnly = true;
            // 
            // yearOfBirthInst
            // 
            this.yearOfBirthInst.HeaderText = "AÑO DE NACIMIENTO";
            this.yearOfBirthInst.Name = "yearOfBirthInst";
            this.yearOfBirthInst.ReadOnly = true;
            // 
            // edadInst
            // 
            this.edadInst.HeaderText = "EDAD";
            this.edadInst.Name = "edadInst";
            this.edadInst.ReadOnly = true;
            // 
            // especialidadInst
            // 
            this.especialidadInst.HeaderText = "ESPECIALIDAD";
            this.especialidadInst.Name = "especialidadInst";
            this.especialidadInst.ReadOnly = true;
            this.especialidadInst.Width = 120;
            // 
            // tipoContratoInst
            // 
            this.tipoContratoInst.HeaderText = "TIPO CONTRATO";
            this.tipoContratoInst.Name = "tipoContratoInst";
            this.tipoContratoInst.ReadOnly = true;
            // 
            // horasSemanaInst
            // 
            this.horasSemanaInst.HeaderText = "HORAS SEMANA";
            this.horasSemanaInst.Name = "horasSemanaInst";
            this.horasSemanaInst.ReadOnly = true;
            // 
            // salarioHorasInst
            // 
            this.salarioHorasInst.HeaderText = "SALARIO/HORAS";
            this.salarioHorasInst.Name = "salarioHorasInst";
            this.salarioHorasInst.ReadOnly = true;
            this.salarioHorasInst.Width = 120;
            // 
            // escolaridadInst
            // 
            this.escolaridadInst.HeaderText = "ESCOLARIDAD";
            this.escolaridadInst.Name = "escolaridadInst";
            this.escolaridadInst.ReadOnly = true;
            this.escolaridadInst.Width = 120;
            // 
            // sueldoTotalInst
            // 
            this.sueldoTotalInst.HeaderText = "SUELDO TOTAL";
            this.sueldoTotalInst.Name = "sueldoTotalInst";
            this.sueldoTotalInst.ReadOnly = true;
            // 
            // frmDatosInstructores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1504, 561);
            this.Controls.Add(this.dgvDatosInstructores);
            this.Name = "frmDatosInstructores";
            this.Text = "frmDatosInstructores";
            this.Load += new System.EventHandler(this.frmDatosInstructores_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosInstructores)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDatosInstructores;
        private System.Windows.Forms.DataGridViewTextBoxColumn codInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn apePatInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn apeMatInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn sexoInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn dniInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn yearOfBirthInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn edadInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn especialidadInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoContratoInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn horasSemanaInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn salarioHorasInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn escolaridadInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn sueldoTotalInst;
    }
}