﻿using _02CasoIncidencias.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02CasoIncidencias.Formularios
{
    public partial class FrmInstructores : Form
    {
        public FrmInstructores()
        {
            InitializeComponent();
        }

        private void btnSalirInst_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void FrmInstructores_Load(object sender, EventArgs e)
        {
            cboEspecialidadInst.Items.Add("Selecciona una opción");
            cboEspecialidadInst.Items.Add("Química");
            cboEspecialidadInst.Items.Add("Física");
            cboEspecialidadInst.Items.Add("Biología");
            cboEspecialidadInst.SelectedIndex = 0;

            cboTipoContratoInst.Items.Add("Selecciona una opción");
            cboTipoContratoInst.Items.Add("Full - time");
            cboTipoContratoInst.Items.Add("Part - time");
            cboTipoContratoInst.SelectedIndex = 0;

        }

        private void btnRegistrarInst_Click(object sender, EventArgs e)
        {
            string _apepatInst = txtApepatInst.Text;
            string _apematInst = txtApematInst.Text;
            string _nomInst = txtNomInst.Text;
            char _sexoInst = (rbSexoInstM.Checked ? 'M' : 'F'); //Operador ternario
            //string _dniAlum = txtdnni.Text;
            int _yearOfBirth = int.Parse(txtYearOldInst.Text);
            string _dniInst = txtDniInst.Text;
            int _edad = int.Parse(txtEdadInst.Text);
            string _especialidadInst = cboEspecialidadInst.SelectedItem.ToString();
            string _tipoContratoInst = cboTipoContratoInst.SelectedItem.ToString();
            
            int _horasSemInst = int.Parse(txtHorasSemanaInst.Text);
            double _salarioHoraInst = double.Parse(txtSalarioHoraInst.Text);
            double _escolaridadInst = double.Parse(txtEscolaridadInst.Text);
            double _sueldoTotalInst = double.Parse(txtSueldoTotalInst.Text);


            //generarCodigo(); //Generar el codigo
            string _codInst = Utils.getSecuencial(_apepatInst, _apematInst, _nomInst, Utils.PREFIJO_INST);
            txtCodInst.Text = _codInst;

            //Instanciando el objeto alumno
            Instructor instructor = new Instructor(_codInst,_apepatInst,_apematInst,_nomInst,_sexoInst,_dniInst,_yearOfBirth,_edad,_especialidadInst,_tipoContratoInst,_horasSemInst,_salarioHoraInst,_escolaridadInst,_sueldoTotalInst);

            DatosPersona.addInstructor(instructor); //Agrega el alumno creado
            MessageBox.Show("Instructor agregado con exito", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void txtYearOldInst_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (Char)Keys.Enter))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            else
            {
                if (this.txtYearOldInst.Text.Length == 4)
                {
                    int yearOfBirth = int.Parse(txtYearOldInst.Text);
                    int edad = Utils.getEdad(yearOfBirth);

                    if (edad == -1) //-1: representa a error
                    {
                        MessageBox.Show("Verifique nuevamente su año de nacimiento", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        txtYearOldInst.Clear();
                        txtEdadInst.Clear();
                        txtYearOldInst.Focus();
                    }
                    else
                    {
                        txtEdadInst.Text = edad.ToString();
                    }
                }
            }
        }

        private void txtHorasSemanaInst_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (Char)Keys.Enter) && (e.KeyChar != Utils.PUNTO))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtSalarioHoraInst_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (Char)Keys.Enter) && (e.KeyChar != Utils.PUNTO))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            else
            {
                if (this.txtSalarioHoraInst.Text.Length > 0)
                {
                    if(txtSueldoTotalInst.Text.Equals(string.Empty))
                    {
                        MessageBox.Show("Presione enter para calcular su Sueldo y Escolaridad", "Importante!");
                    }
                    
                    double _horaSemana = double.Parse(txtHorasSemanaInst.Text);
                    double _salarioHora = double.Parse(txtSalarioHoraInst.Text);
                    double sueldo = _horaSemana*_salarioHora;
                    double escolaridad = sueldo * 0.08;
                    
                    txtSueldoTotalInst.Text = sueldo.ToString(/*"C2"*/);
                    txtEscolaridadInst.Text = escolaridad.ToString(/*"C2"*/);
                }
            }
        }

        private void txtEscolaridadInst_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txtSueldoTotalInst_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
    }
}
