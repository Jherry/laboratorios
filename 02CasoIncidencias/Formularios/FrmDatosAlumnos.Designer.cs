﻿namespace _02_CasoIncidencias.Formularios
{
    partial class frmDatosAlumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDatosAlumnos = new System.Windows.Forms.DataGridView();
            this.codAlumno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apePatAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apeMatAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sexoAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dniAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yearOfBirthAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.edadAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.semestreAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cursoMatAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.notaPraticaAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.notaActitudAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.examenParcialAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.promFinalAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosAlumnos)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDatosAlumnos
            // 
            this.dgvDatosAlumnos.AllowUserToAddRows = false;
            this.dgvDatosAlumnos.AllowUserToDeleteRows = false;
            this.dgvDatosAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosAlumnos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codAlumno,
            this.apePatAlum,
            this.apeMatAlum,
            this.nomAlum,
            this.sexoAlum,
            this.dniAlum,
            this.yearOfBirthAlum,
            this.edadAlum,
            this.semestreAlum,
            this.cursoMatAlum,
            this.notaPraticaAlum,
            this.notaActitudAlum,
            this.examenParcialAlum,
            this.promFinalAlum});
            this.dgvDatosAlumnos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDatosAlumnos.Location = new System.Drawing.Point(0, 0);
            this.dgvDatosAlumnos.Name = "dgvDatosAlumnos";
            this.dgvDatosAlumnos.ReadOnly = true;
            this.dgvDatosAlumnos.RowTemplate.Height = 24;
            this.dgvDatosAlumnos.Size = new System.Drawing.Size(1442, 509);
            this.dgvDatosAlumnos.TabIndex = 0;
            // 
            // codAlumno
            // 
            this.codAlumno.HeaderText = "COD ALUMN";
            this.codAlumno.Name = "codAlumno";
            this.codAlumno.ReadOnly = true;
            // 
            // apePatAlum
            // 
            this.apePatAlum.HeaderText = "APELLIDO PATERNO";
            this.apePatAlum.Name = "apePatAlum";
            this.apePatAlum.ReadOnly = true;
            // 
            // apeMatAlum
            // 
            this.apeMatAlum.HeaderText = "APELLIDO MATERNO";
            this.apeMatAlum.Name = "apeMatAlum";
            this.apeMatAlum.ReadOnly = true;
            // 
            // nomAlum
            // 
            this.nomAlum.HeaderText = "NOMBRES";
            this.nomAlum.Name = "nomAlum";
            this.nomAlum.ReadOnly = true;
            // 
            // sexoAlum
            // 
            this.sexoAlum.HeaderText = "SEXO";
            this.sexoAlum.Name = "sexoAlum";
            this.sexoAlum.ReadOnly = true;
            // 
            // dniAlum
            // 
            this.dniAlum.HeaderText = "DNI";
            this.dniAlum.Name = "dniAlum";
            this.dniAlum.ReadOnly = true;
            // 
            // yearOfBirthAlum
            // 
            this.yearOfBirthAlum.HeaderText = "AÑO DE NACIMIENTO";
            this.yearOfBirthAlum.Name = "yearOfBirthAlum";
            this.yearOfBirthAlum.ReadOnly = true;
            // 
            // edadAlum
            // 
            this.edadAlum.HeaderText = "EDAD";
            this.edadAlum.Name = "edadAlum";
            this.edadAlum.ReadOnly = true;
            // 
            // semestreAlum
            // 
            this.semestreAlum.HeaderText = "SEMESTRE";
            this.semestreAlum.Name = "semestreAlum";
            this.semestreAlum.ReadOnly = true;
            // 
            // cursoMatAlum
            // 
            this.cursoMatAlum.HeaderText = "CURSO MATRICULADO";
            this.cursoMatAlum.Name = "cursoMatAlum";
            this.cursoMatAlum.ReadOnly = true;
            // 
            // notaPraticaAlum
            // 
            this.notaPraticaAlum.HeaderText = "NOTA PRACTICA";
            this.notaPraticaAlum.Name = "notaPraticaAlum";
            this.notaPraticaAlum.ReadOnly = true;
            // 
            // notaActitudAlum
            // 
            this.notaActitudAlum.HeaderText = "NOTA ACTITUD";
            this.notaActitudAlum.Name = "notaActitudAlum";
            this.notaActitudAlum.ReadOnly = true;
            // 
            // examenParcialAlum
            // 
            this.examenParcialAlum.HeaderText = "EXAMEN PARCIAL";
            this.examenParcialAlum.Name = "examenParcialAlum";
            this.examenParcialAlum.ReadOnly = true;
            // 
            // promFinalAlum
            // 
            this.promFinalAlum.HeaderText = "PROMEDIO FINAL";
            this.promFinalAlum.Name = "promFinalAlum";
            this.promFinalAlum.ReadOnly = true;
            // 
            // frmDatosAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1442, 509);
            this.Controls.Add(this.dgvDatosAlumnos);
            this.Name = "frmDatosAlumnos";
            this.Text = "Reporte de alumnos registrados";
            this.Load += new System.EventHandler(this.frmDatosAlumnos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosAlumnos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDatosAlumnos;
        private System.Windows.Forms.DataGridViewTextBoxColumn codAlumno;
        private System.Windows.Forms.DataGridViewTextBoxColumn apePatAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn apeMatAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn sexoAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn dniAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn yearOfBirthAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn edadAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn semestreAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn cursoMatAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn notaPraticaAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn notaActitudAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn examenParcialAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn promFinalAlum;
    }
}