﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02CasoIncidencias.Clases
{
    public class Persona
    {
        private string codPersona;
        private string apepatPersona;
        private string apematPersona;
        private string nomPersona;
        private char sexoPersona;
        private string dniPersona;
        private int yearOfBirthPersona;
        private int edadPersona;

        //Constructor con argumentos
        public Persona(string codPersona, string apepatPersona, string apematPersona, string nomPersona, char sexoPersona, string dniPersona, int yearOldPersona, int edadPersona)
        {
            this.CodPersona = codPersona;
            this.ApepatPersona = apepatPersona;
            this.ApematPersona = apematPersona;
            this.NomPersona = nomPersona;
            this.SexoPersona = sexoPersona;
            this.DniPersona = dniPersona;
            this.YearOfBirthPersona = yearOfBirthPersona;
            this.EdadPersona = edadPersona;
        }

        //Encapsulamiento
        public string CodPersona { get => codPersona; set => codPersona = value; }
        public string ApepatPersona { get => apepatPersona; set => apepatPersona = value; }
        public string ApematPersona { get => apematPersona; set => apematPersona = value; }
        public string NomPersona { get => nomPersona; set => nomPersona = value; }
        public char SexoPersona { get => sexoPersona; set => sexoPersona = value; }
        public string DniPersona { get => dniPersona; set => dniPersona = value; }
        public int YearOfBirthPersona { get => yearOfBirthPersona; set => yearOfBirthPersona = value; }
        public int EdadPersona { get => edadPersona; set => edadPersona = value; }

        //Métodos
        public virtual string generarCodigo(string apepatPersona, string apematPersona, string nomPersona)
        {
            char letraNombre = char.Parse(nomPersona.Substring(0, 1));
            char letraApepat = apepatPersona[0];
            char letraApemat = char.Parse(apematPersona.Substring(0, 1));
            int yearCurrent = Utils.getCurrentCurrent();


            return null;
        }

        public virtual int obtenerEdad()
        {
            return 0;
        }
    }
}
