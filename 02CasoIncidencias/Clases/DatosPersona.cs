﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02CasoIncidencias.Clases
{
    public static class DatosPersona
    {
        //private static ArrayList arrAlumnos= new ArrayList();
        private static List<Alumno> lsAlumnos = new List<Alumno>();
        //private static ArrayList arrInstructores = new ArrayList();
        private static List<Instructor> lsInstructores = new List<Instructor>();

        public static void addAlumno(Alumno alumno)
        {
            if (alumno!=null)
            {
                lsAlumnos.Add(alumno);
            }
        }
        public static int obtenerLongitudListaAlumn()
        {
            int longitud = lsAlumnos.Count;

            return ++longitud;
        }
        public static List<Alumno> obtenerAlumnos()
        {
            return lsAlumnos;
        }
        public static void addInstructor(Instructor instructor)
        {
            if (instructor != null)
            {
                lsInstructores.Add(instructor);
            }
        }
        public static int obtenerLongitudListaInst()
        {
            int longitud = lsInstructores.Count;

            return ++longitud;
        }
        public static List<Instructor> obtenerInstructores()
        {
            return lsInstructores;
        }

    }
}
