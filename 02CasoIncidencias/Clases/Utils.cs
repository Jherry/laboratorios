﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02CasoIncidencias.Clases
{
    public static class Utils
    {
        public static char PUNTO = '.';

        public static string PREFIJO_ALUM = "ALUM";
        public static string PREFIJO_INST = "INST";

        public static int getCurrentCurrent()
        {
            DateTime dt = DateTime.Today;
            return dt.Year;
        }

        public static string getSecuencial(string apepat, string apemat, string nom, string prefijo)
        {
            string letraNombre = nom.Substring(0, 1);
            string letraApepat = apepat[0].ToString();
            string letraApemat = apemat.Substring(0, 1);
            string yearCurrent = Utils.getCurrentCurrent().ToString();


            string secuencia = DatosPersona.obtenerLongitudListaAlumn().ToString();
            return prefijo+yearCurrent+letraNombre+letraApepat+letraApemat+secuencia;
        }

        public static int getEdad(int yearOfBirth)
        {
            DateTime fechaActual = DateTime.Today;
            int yearCurrent = fechaActual.Year;
            int edad = yearCurrent - yearOfBirth;
            if (edad<1||edad>100)
            {
                return -1;
            }

            return edad;
        }

    }
}
