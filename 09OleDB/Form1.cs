﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _09OleDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private static string PATH = "C:/dasdadsd/laboratorios/DB_SENATI.xlsx;";
        private void btnConexion_Click(object sender, EventArgs e)
        {
            try
            {
                /* string cadenaConexion = String.Format("Provider = Microsoft.Jet.OLEDB.4.0" +
                 " Data Source = C:/Developear/laboratorios/DB_SENATI.xlsx;" +
             "Extended Properties = \"Excel 8.0;HDR=Yes;IMEX=1;\"");*/

                string cadenaConexion = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;" +
                     $"Data Source={PATH}" +
                     "Extended Properties='Excel 12.0 Xml;HDR=YES;IMEX=1;'");



                OleDbConnection conexion = default(OleDbConnection);
                conexion = new OleDbConnection(cadenaConexion);

                conexion.Open();//abrir conexion

                OleDbCommand query = default(OleDbCommand);
                query = new OleDbCommand("Select *from [USUARIO$]", conexion);

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = query;

                DataSet ds = new DataSet();

                adapter.Fill(ds);

                dgvUusario.DataSource = ds.Tables[0];

                conexion.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show($"ERROR:{ex.Message}");
                    
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
