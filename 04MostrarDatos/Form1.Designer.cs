﻿namespace _04MostrarDatos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.dvMostrarDatos = new System.Windows.Forms.DataGridView();
            this.btnMostrar = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.codAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apepatAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apematAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomAlum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dvMostrarDatos)).BeginInit();
            this.SuspendLayout();
            // 
            // dvMostrarDatos
            // 
            this.dvMostrarDatos.AllowUserToAddRows = false;
            this.dvMostrarDatos.AllowUserToDeleteRows = false;
            this.dvMostrarDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvMostrarDatos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codAlum,
            this.apepatAlum,
            this.apematAlum,
            this.nomAlum});
            this.dvMostrarDatos.Location = new System.Drawing.Point(12, 82);
            this.dvMostrarDatos.Name = "dvMostrarDatos";
            this.dvMostrarDatos.ReadOnly = true;
            this.dvMostrarDatos.Size = new System.Drawing.Size(776, 213);
            this.dvMostrarDatos.TabIndex = 0;
            // 
            // btnMostrar
            // 
            this.btnMostrar.Location = new System.Drawing.Point(258, 357);
            this.btnMostrar.Name = "btnMostrar";
            this.btnMostrar.Size = new System.Drawing.Size(117, 23);
            this.btnMostrar.TabIndex = 1;
            this.btnMostrar.Text = "Mostrar Datos";
            this.btnMostrar.UseVisualStyleBackColor = true;
            this.btnMostrar.Click += new System.EventHandler(this.btnMostrar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(454, 357);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 2;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            // 
            // codAlum
            // 
            this.codAlum.HeaderText = "CODIGO DE ALUMNO";
            this.codAlum.Name = "codAlum";
            this.codAlum.ReadOnly = true;
            // 
            // apepatAlum
            // 
            this.apepatAlum.HeaderText = "APELLIDO PATERNO";
            this.apepatAlum.Name = "apepatAlum";
            this.apepatAlum.ReadOnly = true;
            // 
            // apematAlum
            // 
            this.apematAlum.HeaderText = "APELLIDO PATERNO";
            this.apematAlum.Name = "apematAlum";
            this.apematAlum.ReadOnly = true;
            // 
            // nomAlum
            // 
            this.nomAlum.HeaderText = "NOMBRE";
            this.nomAlum.Name = "nomAlum";
            this.nomAlum.ReadOnly = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnMostrar);
            this.Controls.Add(this.dvMostrarDatos);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dvMostrarDatos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dvMostrarDatos;
        private System.Windows.Forms.Button btnMostrar;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.DataGridViewTextBoxColumn codAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn apepatAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn apematAlum;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomAlum;
    }
}

