﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _04MostrarDatos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            try
            {
                MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
                builder.Server = DatosConexion.SERVER;
                builder.Database = DatosConexion.DATABASE;
                builder.UserID = DatosConexion.USERID;
                builder.Password = DatosConexion.PASSWORD;
                builder.SslMode = 0;

                string cadena = builder.ToString();

                MySqlConnection conexion = new MySqlConnection(cadena);
                conexion.Open();

                string query = "SELECT * FROM db_senati.JHERRY_ALUMNOS";

                //Prepara la conexión
                MySqlConnection databaseConnection = new MySqlConnection(cadena);
                MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
                commandDatabase.CommandTimeout = 60;
                MySqlDataReader reader;

                try
                {
                    databaseConnection.Open(); //Abre la base de datos
                    reader = commandDatabase.ExecuteReader(); //Ejecuta la consulta

                    if (reader.HasRows) //Verifica la existencia de filas
                    {
                        while (reader.Read())
                        {
                            //string[] row = { reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3)};
                            dvMostrarDatos.Rows.Add(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));
                        }
                    }
                    else
                    {
                        MessageBox.Show("No se econtraron datos.", "Error");
                        databaseConnection.Close();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("{ex.Message}", "Error!");
                }


                //MessageBox.Show("Conexion exitosa", "Excelente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error de conexion{ex.Message}", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
