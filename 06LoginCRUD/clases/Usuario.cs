﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_LoginCRUD.Clases
{
    public class Usuario
    {
        private int iduser;
        private string usuarioUser;
        private string claveUser;
        private string rolUser;
        private string emailUser;
        private char estadoUser;

        public int Iduser { get => iduser; set => iduser = value; }
        public string UsuarioUser { get => usuarioUser; set => usuarioUser = value; }
        public string ClaveUser { get => claveUser; set => claveUser = value; }
        public string RolUser { get => rolUser; set => rolUser = value; }
        public string EmailUser { get => emailUser; set => emailUser = value; }
        public char EstadoUser { get => estadoUser; set => estadoUser = value; }

        public Usuario()
        {
        }

        public Usuario(int iduser, string usuarioUser, string claveUser, string rolUser, string emailUser, char estadoUser)
        {
            this.Iduser = iduser;
            this.UsuarioUser = usuarioUser;
            this.ClaveUser = claveUser;
            this.RolUser = rolUser;
            this.EmailUser = emailUser;
            this.EstadoUser = estadoUser;
        }

        public static bool validarCredenciales(MySqlConnection conexion, string usuario, string password)
        {
            bool rpta = false;

            string query = string.Format($"SELECT * FROM JHERRY_USUARIO WHERE USUARIOUSER = " +
                $"'{usuario}' AND CLAVEUSER = '{password}';");
            conexion.Open();

            MySqlCommand command = new MySqlCommand(query, conexion);

            MySqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                rpta = true;
            }

            conexion.Close();
            return rpta;

        }

        public static int InsertarUsuario(MySqlConnection conexion, Usuario usuario)
        {
            string query = string.Format($"INSERT INTO JHERRY_USUARIO VALUES (" +
                $"'0', " +
                $"'{usuario.UsuarioUser}', " +
                $"'{usuario.ClaveUser}', " +
                $"'{usuario.RolUser}', " +
                $"'{usuario.EmailUser}', " +
                $"'{usuario.EstadoUser}' " +
                $");");
            conexion.Open();

            MySqlCommand command = new MySqlCommand(query, conexion);

            int numFilasAfectadas = command.ExecuteNonQuery();
            conexion.Close();

            return numFilasAfectadas;
        }
    }



}