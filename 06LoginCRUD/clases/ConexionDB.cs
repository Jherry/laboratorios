﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06LoginCRUD.clases
{
    class ConexionDB
    {
        public static string SERVER = "35.202.148.81";
        public static string DATABASE = "db_peru";
        public static string USERID = "senati";
        public static string PASSWORD = "senati";
        public static int SSL_MODE = 0;

        MySqlConnection conexion;
        public static MySqlConnection obtenerConexion()
        {
            string cadenaConexion = string.Format($"server={SERVER};database={DATABASE};userID={USERID};password={PASSWORD};sslmode=none;");

            MySqlConnection conexion = new MySqlConnection(cadenaConexion);
            return conexion;
        }
    }
}
