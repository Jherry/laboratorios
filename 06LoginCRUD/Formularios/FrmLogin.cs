﻿using _06_LoginCRUD.Clases;
using _06LoginCRUD.clases;
using _06LoginCRUD.Formularios;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _06LoginCRUD
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        MySqlConnection conexion = ConexionDB.obtenerConexion();

        

        

        

        private void btnIngresar_Click_1(object sender, EventArgs e)
        {
            {
                string _usuario = txtUsuario.Text;
                string _password = txtContraseña.Text;

                if (_usuario.Equals(string.Empty) || _password.Equals(string.Empty))
                {
                    MessageBox.Show("Ingrese usuario y/o password", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    bool esValido = Usuario.validarCredenciales(conexion, _usuario, _password);
                    if(esValido)
                    {
                        MessageBox.Show("Ingreso correctamente");
                    }
                }

            }
        }

        private void lnkRegistroNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FrmRegistro frmRegistro = new FrmRegistro();
            frmRegistro.Show();
        }

        private void btnCancelar_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}