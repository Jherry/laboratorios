﻿using _06_LoginCRUD.Clases;
using _06LoginCRUD.clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _06LoginCRUD.Formularios
{
    public partial class FrmRegistro : Form
    {
        public FrmRegistro()
        {
            InitializeComponent();
        }
        MySqlConnection conexion = ConexionDB.obtenerConexion();
        private void button1_Click(object sender, EventArgs e)
        {
           // string _ID = txtID.Text;
            string _usuario = txtUsuario.Text;
            string _clave = txtUsuario.Text;
            string _rol = cboRol.Text;
            string _email = txtEmail.Text;
            char _estado = (rbtA.Checked ? 'A' : 'I');

            Usuario usuario = new Usuario(0, _usuario, _clave, _rol,_email,_estado);

            int numFilasAfectadas = Usuario.InsertarUsuario(conexion, usuario);
            if(numFilasAfectadas>0)
            {
                MessageBox.Show("Insetado con exito");
            }
            else
            {
                MessageBox.Show("Error al insertar");
            }
        }
    }
}
