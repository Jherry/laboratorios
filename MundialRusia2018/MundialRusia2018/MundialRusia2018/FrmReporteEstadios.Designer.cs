﻿namespace MundialRusia2018
{
    partial class FrmReporteEstadios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.rptEstadios = new Microsoft.Reporting.WinForms.ReportViewer();
            this.DataSetMundial = new MundialRusia2018.DataSetMundial();
            this.DataTableEstadiosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnMostrarReporteEstadio = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetMundial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTableEstadiosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rptEstadios
            // 
            reportDataSource1.Name = "DataSetEstadio";
            reportDataSource1.Value = this.DataTableEstadiosBindingSource;
            this.rptEstadios.LocalReport.DataSources.Add(reportDataSource1);
            this.rptEstadios.LocalReport.ReportEmbeddedResource = "MundialRusia2018.InformeEstadio.rdlc";
            this.rptEstadios.Location = new System.Drawing.Point(12, 111);
            this.rptEstadios.Name = "rptEstadios";
            this.rptEstadios.ServerReport.BearerToken = null;
            this.rptEstadios.Size = new System.Drawing.Size(776, 269);
            this.rptEstadios.TabIndex = 0;
            // 
            // DataSetMundial
            // 
            this.DataSetMundial.DataSetName = "DataSetMundial";
            this.DataSetMundial.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DataTableEstadiosBindingSource
            // 
            this.DataTableEstadiosBindingSource.DataMember = "DataTableEstadios";
            this.DataTableEstadiosBindingSource.DataSource = this.DataSetMundial;
            // 
            // btnMostrarReporteEstadio
            // 
            this.btnMostrarReporteEstadio.Location = new System.Drawing.Point(458, 49);
            this.btnMostrarReporteEstadio.Name = "btnMostrarReporteEstadio";
            this.btnMostrarReporteEstadio.Size = new System.Drawing.Size(209, 23);
            this.btnMostrarReporteEstadio.TabIndex = 1;
            this.btnMostrarReporteEstadio.Text = "Mostrar Reporte Estadio";
            this.btnMostrarReporteEstadio.UseVisualStyleBackColor = true;
            this.btnMostrarReporteEstadio.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmReporteEstadios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnMostrarReporteEstadio);
            this.Controls.Add(this.rptEstadios);
            this.Name = "FrmReporteEstadios";
            this.Text = "FrmReporteEstadios";
            this.Load += new System.EventHandler(this.FrmReporteEstadios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataSetMundial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTableEstadiosBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptEstadios;
        private System.Windows.Forms.BindingSource DataTableEstadiosBindingSource;
        private DataSetMundial DataSetMundial;
        private System.Windows.Forms.Button btnMostrarReporteEstadio;
    }
}