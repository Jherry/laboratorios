﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MundialRusia2018.Clases
{
    public class Grupo
    {
        private int idGrup;
        private int cantEquiposGrup;
        private string descGrup;

        public Grupo()
        {
        }

        public Grupo(int idGrup, int cantEquiposGrup, string descGrup)
        {
            this.IdGrup = idGrup;
            this.CantEquiposGrup = cantEquiposGrup;
            this.DescGrup = descGrup;
        }

        public int IdGrup { get => idGrup; set => idGrup = value; }
        public int CantEquiposGrup { get => cantEquiposGrup; set => cantEquiposGrup = value; }
        public string DescGrup { get => descGrup; set => descGrup = value; }

        public static Dictionary<int, string> listarGrupos(MySqlConnection conexion)
        {
            Dictionary<int, string> mapGrupos = new Dictionary<int, string>();
            conexion.Open();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = Constantes.SP_LIST_GRUPO;
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conexion;
            MySqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Grupo grupo = new Grupo();
                    grupo.IdGrup = reader.GetInt32(0);
                    grupo.DescGrup = reader.GetString(2);

                    mapGrupos.Add(grupo.IdGrup, grupo.DescGrup);
                }
            }
            conexion.Close();
            return mapGrupos;
        }
    }
}
