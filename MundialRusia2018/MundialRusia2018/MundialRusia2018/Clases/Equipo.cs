﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MundialRusia2018.Clases
{
    public class Equipo
    {
        private static List<Equipo> mapPosiciones;
        private int idEqui;
        private int idGrup;
        private string nombreEqui;
        private string fecCreacionEqui;
        private string entrenadorEqui;
        private int cantMiembrosEqui;
        private string paisDestinoEqui;

        public Equipo()
        {
        }

        public Equipo(int idEqui, int idGrup, string nombreEqui, string fecCreacionEqui, string entrenadorEqui, int cantMiembrosEqui, string paisDestinoEqui)
        {
            this.IdEqui = idEqui;
            this.IdGrup = idGrup;
            this.NombreEqui = nombreEqui;
            this.FecCreacionEqui = fecCreacionEqui;
            this.EntrenadorEqui = entrenadorEqui;
            this.CantMiembrosEqui = cantMiembrosEqui;
            this.PaisDestinoEqui = paisDestinoEqui;
        }

        public int IdEqui { get => idEqui; set => idEqui = value; }
        public int IdGrup { get => idGrup; set => idGrup = value; }
        public string NombreEqui { get => nombreEqui; set => nombreEqui = value; }
        public string FecCreacionEqui { get => fecCreacionEqui; set => fecCreacionEqui = value; }
        public string EntrenadorEqui { get => entrenadorEqui; set => entrenadorEqui = value; }
        public int CantMiembrosEqui { get => cantMiembrosEqui; set => cantMiembrosEqui = value; }
        public string PaisDestinoEqui { get => paisDestinoEqui; set => paisDestinoEqui = value; }

        internal static List<Equipo> listarEquiposByGrupo(MySqlConnection conexion, int idGrup)
        {
            throw new NotImplementedException();
        }

        public static List<Equipo> listarEquipos(MySqlConnection conexion)
        {
            List<Equipo> lsEquipos = new List<Equipo>();
            conexion.Open();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = Constantes.SP_LIST_EQUIPO_BY_GRUPO;
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conexion;
            MySqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Posicion posicion = new Posicion();
                    posicion.IdPos = reader.GetInt32(0);
                    posicion.DescPos = reader.GetString(1);

            
                }
            }
            conexion.Close();
            return mapPosiciones;
        }

    }
}
