﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MundialRusia2018.Clases
{
    public class Posicion
    {
        private int idPos;
        private string descPos;

        public Posicion()
        {
        }

        public Posicion(int idPos, string descPos)
        {
            this.IdPos = idPos;
            this.DescPos = descPos;
        }

        public int IdPos { get => idPos; set => idPos = value; }
        public string DescPos { get => descPos; set => descPos = value; }

        //Metodos de clase
        public static Dictionary<int,string> listasPosiciones(MySqlConnection conexion)
        {
            Dictionary<int, string> mapPosiciones = new Dictionary<int, string>();
            conexion.Open();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = Constantes.SP_LIST_POSICION;
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conexion;
            MySqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Posicion posicion = new Posicion();
                    posicion.IdPos = reader.GetInt32(0);
                    posicion.DescPos = reader.GetString(1);

                    mapPosiciones.Add(posicion.IdPos, posicion.DescPos);
                }
            }
            conexion.Close();
            return mapPosiciones;
           
        }

    }
}
