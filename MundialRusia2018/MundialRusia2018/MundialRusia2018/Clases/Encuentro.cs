﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MundialRusia2018.Clases
{
    public class Encuentro
    {
        private int idEnc;
        private int idEst;
        private int idEqui;
        private string fecEnc;
        private string arbitroEnc;
        private string ganadorEnc;
        private int idEquiAEnc;
        private int idEquiBEnc;
        private string marcadorEnc;

        public Encuentro()
        {
        }

        public Encuentro(int idEnc, int idEst, int idEqui, string fecEnc, string arbitroEnc, string ganadorEnc, int idEquiAEnc, int idEquiBEnc, string marcadorEnc)
        {
            this.IdEnc = idEnc;
            this.IdEst = idEst;
            this.IdEqui = idEqui;
            this.FecEnc = fecEnc;
            this.ArbitroEnc = arbitroEnc;
            this.GanadorEnc = ganadorEnc;
            this.IdEquiAEnc = idEquiAEnc;
            this.IdEquiBEnc = idEquiBEnc;
            this.MarcadorEnc = marcadorEnc;
        }

        public int IdEnc { get => idEnc; set => idEnc = value; }
        public int IdEst { get => idEst; set => idEst = value; }
        public int IdEqui { get => idEqui; set => idEqui = value; }
        public string FecEnc { get => fecEnc; set => fecEnc = value; }
        public string ArbitroEnc { get => arbitroEnc; set => arbitroEnc = value; }
        public string GanadorEnc { get => ganadorEnc; set => ganadorEnc = value; }
        public int IdEquiAEnc { get => idEquiAEnc; set => idEquiAEnc = value; }
        public int IdEquiBEnc { get => idEquiBEnc; set => idEquiBEnc = value; }
        public string MarcadorEnc { get => marcadorEnc; set => marcadorEnc = value; }

        public static int registrarEncuentro(MySqlConnection conexion, Encuentro encuentro)
        {
            return 0;
        }
    }

}
