﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MundialRusia2018.Clases
{
    class Estadio
    {
         
        private int idEst;
        private string nombreEst;
        private string localidadEst;
        private int aforoEst;

        public Estadio()
        {
        }

        public Estadio(int idEst, string nombreEst, string localidadEst, int aforoEst)
        {
            this.IdEst = idEst;
            this.NombreEst = nombreEst;
            this.LocalidadEst = localidadEst;
            this.AforoEst = aforoEst;
        }

        public int IdEst { get => idEst; set => idEst = value; }
        public string NombreEst { get => nombreEst; set => nombreEst = value; }
        public string LocalidadEst { get => localidadEst; set => localidadEst = value; }
        public int AforoEst { get => aforoEst; set => aforoEst = value; }


        public static int insertarEstadio(MySqlConnection conexion, Estadio estadio)
        {
            /*
            string query = string.Format($"INSERT INTO ESTADIO VALUES ('{estadio.NombreEst}','{estadio.LocalidadEst}',{estadio.AforoEst});");

            conexion.Open();
            MySqlCommand command = new MySqlCommand(query,conexion);
            int i=command.ExecuteNonQuery();
            conexion.Close();
            return i;
            */
            conexion.Open();

            //Vinculación con el nombre de store procedure
            MySqlCommand command = new MySqlCommand();
            command.CommandText = Constantes.SP_INSERT_ESTADIO;
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conexion;

            //Agregar los parametros necesarios del store procedure
            //p_nombreEst,p_localidadEst,p_aforoEst
            command.Parameters.AddWithValue("p_nombreEst", estadio.NombreEst);
            command.Parameters.AddWithValue("p_localidadEst", estadio.LocalidadEst);
            command.Parameters.AddWithValue("p_aforoEst", estadio.AforoEst);

            int i = command.ExecuteNonQuery();
            conexion.Close();
            return i;

        }

        public static int actualizarEstadio(MySqlConnection conexion, Estadio estadio)
        {
            /*
            string query = string.Format($"UPDATE ESTADIO SET " +
                $"NOMBREEST = '{estadio.NombreEst}', " +
                $"LOCALIDADEST = '{estadio.LocalidadEst}', " +
                $"AFOROEST = {estadio.AforoEst} " +
                $"WHERE IDEST={estadio.IdEst};");
            conexion.Open();
            MySqlCommand command = new MySqlCommand(query, conexion);
            int i = command.ExecuteNonQuery();
            conexion.Close();
            return i;
            */
            conexion.Open();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = Constantes.SP_UPDATE_ESTADIO;
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conexion;

            command.Parameters.AddWithValue("p_nombreEst", estadio.NombreEst);
            command.Parameters.AddWithValue("p_localidadEst", estadio.LocalidadEst);
            command.Parameters.AddWithValue("p_aforoEst", estadio.AforoEst);
            command.Parameters.AddWithValue("p_idEst", estadio.IdEst);

            int i = command.ExecuteNonQuery();
            conexion.Close();
            return i;
        }

        public static int eliminarEstadio(MySqlConnection conexion, int idEst)
        {
            /*
            string query = string.Format($"DELETE FROM ESTADIO WHERE IDEST={idEst};");
            conexion.Open();
            MySqlCommand command = new MySqlCommand(query, conexion);
            int i = command.ExecuteNonQuery();
            conexion.Close();
            return i;
            */

            conexion.Open();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = Constantes.SP_DELETE_ESTADIO;
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conexion;

            command.Parameters.AddWithValue("p_idEst", idEst);
            int i = command.ExecuteNonQuery();
            conexion.Close();
            return i;
        }

        public static List<Estadio> listarEstadios(MySqlConnection conexion)
        {
            List<Estadio> lsEstadio = new List<Estadio>();

            /*
            string query = string.Format($"SELECT * FROM ESTADIO;");

            conexion.Open();
            MySqlCommand command = new MySqlCommand(query, conexion);
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Estadio estadio = new Estadio(reader.GetInt32(0),reader.GetString(1),reader.GetString(2),reader.GetInt32(3));

                    lsEstadio.Add(estadio);
                }
            }
            conexion.Close();
            return lsEstadio;

            */

            conexion.Open();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = Constantes.SP_LIST_ESTADIO;
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conexion;

            MySqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Estadio estadio = new Estadio(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3));

                    lsEstadio.Add(estadio);
                }
            }
            conexion.Close();
            return lsEstadio;
        }
    }
}
