﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MundialRusia2018.Clases
{
    public class Jugador
    {
        private int idJug;
        private int idEqui;
        private string nombresJug;
        private string apepatJug;
        private string apematJug;
        private String fecNacJug;
        private Char sexoJug;
        private int numCamisetaJug;
        private string dniJug;
        private string clubJug;

        public int IdJug { get => idJug; set => idJug = value; }
        public int IdEqui { get => idEqui; set => idEqui = value; }
        public string NombresJug { get => nombresJug; set => nombresJug = value; }
        public string ApepatJug { get => apepatJug; set => apepatJug = value; }
        public string ApematJug { get => apematJug; set => apematJug = value; }
        public string FecNacJug { get => fecNacJug; set => fecNacJug = value; }
        public char SexoJug { get => sexoJug; set => sexoJug = value; }
        public int NumCamisetaJug { get => numCamisetaJug; set => numCamisetaJug = value; }
        public string DniJug { get => dniJug; set => dniJug = value; }
        public string ClubJug { get => clubJug; set => clubJug = value; }

        public Jugador()
        {
        }

        public Jugador(int idJug, int idEqui, string nombresJug, string apepatJug, string apematJug, string fecNacJug, char sexoJug, int numCamisetaJug, string dniJug, string clubJug)
        {
            this.IdJug = idJug;
            this.IdEqui = idEqui;
            this.NombresJug = nombresJug;
            this.ApepatJug = apepatJug;
            this.ApematJug = apematJug;
            this.FecNacJug = fecNacJug;
            this.SexoJug = sexoJug;
            this.NumCamisetaJug = numCamisetaJug;
            this.DniJug = dniJug;
            this.ClubJug = clubJug;
        }

        public static int insertarJugador(MySqlConnection conexion, Jugador jugador)
        {
            /*
            string query = string.Format($"INSERT INTO JUGADOR VALUES (" +
                $"IDEQUI={jugador.IdEqui}" +
                $"NOMBRESJUG='{jugador.NombresJug}', " +
                $"APEPATJUG='{jugador.ApepatJug}', " +
                $"APEMATJUG='{jugador.ApematJug}', " +
                $"FECNACJUG='{jugador.FecNacJug}', " +
                $"SEXOJUG='{jugador.SexoJug}', " +
                $"NUMCAMISETAJUG={jugador.NumCamisetaJug}, " +
                $"DNIJUG='{jugador.DniJug}', " +
                $"CLUBJUG='{jugador.ClubJug}'" +
                $");");
            conexion.Open();
            MySqlCommand command = new MySqlCommand(query,conexion);
            int i = command.ExecuteNonQuery();
            conexion.Close();
            return i; 
            */

            conexion.Open();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = Constantes.SP_INSERT_JUGADOR;
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conexion;

            //Agregando parametros al store
            //command.Parameters.AddWithValue("p_idJug", jugador.IdJug);
            command.Parameters.AddWithValue("p_idEqui", jugador.IdEqui);
            command.Parameters.AddWithValue("p_nombresJug", jugador.NombresJug);
            command.Parameters.AddWithValue("p_apepatJug", jugador.ApepatJug); 
            command.Parameters.AddWithValue("p_apematJug", jugador.ApematJug);
            command.Parameters.AddWithValue("p_fecNacJug", jugador.FecNacJug);
            command.Parameters.AddWithValue("p_sexoJug", jugador.SexoJug);
            command.Parameters.AddWithValue("p_numCamisetaJug", jugador.NumCamisetaJug);
            command.Parameters.AddWithValue("p_dniJug", jugador.DniJug);
            command.Parameters.AddWithValue("p_clubJug", jugador.ClubJug);

            int i = command.ExecuteNonQuery();
            conexion.Close();
            return i;
        }

        public static int eliminarJugador(MySqlConnection conexion,int _idJug)
        {
            /*
            string query = string.Format($"DELETE JUGADOR WHERE IDJUG={IdJug}");
            conexion.Open();
            MySqlCommand command = new MySqlCommand(query, conexion);
            int i = command.ExecuteNonQuery();
            conexion.Close();
            return i;
            */
            conexion.Open();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = Constantes.SP_DELETE_JUGADOR;
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conexion;

            command.Parameters.AddWithValue("p_idJug", _idJug);
            int i = command.ExecuteNonQuery();
            conexion.Close();
            return i;
        }

        public static int actualizarJugador(MySqlConnection conexion, Jugador jugador)
        {
            /*
            string query = string.Format($"UPDATE JUGADOR SET ");
            conexion.Open();
            MySqlCommand command = new MySqlCommand(query, conexion);
            int i = command.ExecuteNonQuery();
            conexion.Close();
            return i;
            */
            conexion.Open();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = Constantes.SP_UPDATE_JUGADOR;
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conexion;

            command.Parameters.AddWithValue("p_idEqui", jugador.IdEqui);
            command.Parameters.AddWithValue("p_nombresJug", jugador.NombresJug);
            command.Parameters.AddWithValue("p_apepatJug", jugador.ApepatJug);
            command.Parameters.AddWithValue("p_apematJug", jugador.ApematJug);
            command.Parameters.AddWithValue("p_fecNacJug", jugador.FecNacJug);
            command.Parameters.AddWithValue("p_sexoJug", jugador.SexoJug);
            command.Parameters.AddWithValue("p_numCamisetaJug", jugador.NumCamisetaJug);
            command.Parameters.AddWithValue("p_dniJug", jugador.DniJug);
            command.Parameters.AddWithValue("p_clubJug", jugador.ClubJug);

            command.Parameters.AddWithValue("p_idJug", jugador.IdJug);

            int i = command.ExecuteNonQuery();
            conexion.Close();
            return i;
        }

        public static List<Jugador> listarJugadores(MySqlConnection conexion)
        {
            List<Jugador> lsJugador = new List<Jugador>();
            conexion.Open();
            MySqlCommand command = new MySqlCommand();
            command.CommandText = Constantes.SP_LIST_JUGADOR;
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = conexion;

            MySqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Jugador jugador = new Jugador();

                    jugador.IdJug = reader.GetInt32(0);
                    jugador.NombresJug = reader.GetString(2);
                    jugador.ApepatJug = reader.GetString(3);
                    jugador.ApematJug = reader.GetString(4);
                    //jugador.FecNacJug = reader.GetString(5);
                    jugador.SexoJug = reader.GetChar(6);
                    jugador.NumCamisetaJug= reader.GetInt32(7);
                    jugador.DniJug = reader.GetString(8);
                    jugador.ClubJug = reader.GetString(9);
                    //jugador.IdEqui = reader.GetInt32(1);

                    lsJugador.Add(jugador);
                }
            }
            conexion.Close();
            return lsJugador;
        }
    }
}
