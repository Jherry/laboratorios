﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MundialRusia2018.Clases
{
    //Clase para crear y obtener la conexión al motor de Base de Datos
    public class ConexionDB
    {
        public static string SERVER = "35.202.148.81";
        public static string DATABASE = "db_mundial";
        public static string USERID = "senati";
        public static string PASSWORD = "senati";
        public static int SSL_MODE = 0;

        public static MySqlConnection obtenerConexion()
        {
            string cadenaConexion = string.Format($"server={SERVER};database={DATABASE};userID={USERID};password={PASSWORD};sslmode=none;");

            MySqlConnection conexion = new MySqlConnection(cadenaConexion);
            return conexion;
        }
    }
}
