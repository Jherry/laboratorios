﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MundialRusia2018.Clases
{
    //Valores genéricos que serán invocados en el sistema
    public class Constantes
    {
        public static string TITLE_MSG_ERROR = "MENSAJE DE ERROR";
        public static string TITLE_MSG_EXITO = "MENSAJE DE EXITO";
        public static string TITLE_MSG_WARNING = "MENSAJE DE ADVERTENCIA";
        public static string TITLE_MSG = "MENSAJE";
        public static string TITLE_MSG_CONFIRMACION = "MENSAJE DE CONFIRMACION";

        public static string MSG_CONFIRMA_ELIMINAR = "Esta seguro de ELIMINAR el registro seleccionado?.";
        public static string MSG_CONFIRMA_ACTUALIZAR = "Esta seguro de ACTUALIZAR el registro seleccionado?.";
        public static string MSG_INSERTAR_EXITO = "Se insertó correctamente.";
        public static string MSG_ACTUALIZAR_EXITO = "Se actualizó correctamente.";
        public static string MSG_ELIMINAR_EXITO = "Se eliminó correctamente.";
        public static string MSG_ERROR_GENERICO = "Se presentó un error, comuniquese con el administrador.";
        public static string MSG_EXITO = "La transacción a sido satisfactoria.";
        public static string MSG_ERROR_NUMERO = "Solo se permiten números para este dato.";

        public static string CERO = "0";
        public static string UNO = "1";
        public static string SI = "S";
        public static string NO = "N";
        public static string MASCULINO = "M";
        public static string FEMENINO = "F";

        //NOMBRES DE STORES PROCEDURES - SP
        public static string SP_INSERT_ESTADIO = "sp_insertarEstadio";
        public static string SP_UPDATE_ESTADIO = "sp_actualizarEstadio";
        public static string SP_DELETE_ESTADIO = "sp_eliminarEstadio";
        public static string SP_LIST_ESTADIO = "sp_listarEstadio";

        public static string SP_INSERT_JUGADOR = "sp_insertarJugador";
        public static string SP_UPDATE_JUGADOR = "sp_actualizarJugador";
        public static string SP_DELETE_JUGADOR = "sp_eliminarJugador";
        public static string SP_LIST_JUGADOR = "sp_listarJugador";

        public static string SP_INSERT_ENCUENTRO = "sp_insertarEncuentro";
        public static string SP_UPDATE_ENCUENTRO = "sp_actualizarEncuentro";
        public static string SP_DELETE_ENCUENTRO = "sp_eliminarEncuentro";
        public static string SP_LIST_ENCUENTRO = "sp_listarEncuentro";

        public static string SP_INSERT_POSICION = "sp_insertarPosicion";
        public static string SP_UPDATE_POSICION = "sp_actualizarPosicion";
        public static string SP_DELETE_POSICION = "sp_eliminarPosicion";
        public static string SP_LIST_POSICION = "sp_listarPosicion";

        public static string SP_LIST_GRUPO = "sp_listarGrupo";

        public static string SP_INSERT_EQUIPO = "sp_insertarEquipo";
        public static string SP_UPDATE_EQUIPO = "sp_actualizarEquipo";
        public static string SP_DELETE_EQUIPO = "sp_eliminarEquipo";
        public static string SP_LIST_EQUIPO = "sp_listarEquipo";
        public static string SP_LIST_EQUIPO_BY_GRUPO = "sp_listarEquipoByGrupo";
        
    }
}
