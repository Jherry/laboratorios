﻿using MundialRusia2018.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MundialRusia2018.Formularios
{
    public partial class FrmRegistrarEstadio : Form
    {
        public FrmRegistrarEstadio()
        {
            InitializeComponent();
        }

        MySqlConnection conexion = ConexionDB.obtenerConexion();

        public void validaInsertar()
        {
            if (!txtNombreEst.Text.Equals(String.Empty) 
                && !txtNombreEst.Text.Equals(String.Empty) 
                && !txtAforoEst.Text.Equals(String.Empty))
            {
                btnRegistrarEst.Enabled = true;
            }
            else
            {
                btnRegistrarEst.Enabled = false;
            }
        }

        public void limpiarControles()
        {
            txtIdEst.Clear();
            txtNombreEst.Clear();
            txtLocalidadEst.Clear();
            txtAforoEst.Clear();
        }

        public void listarEstadios()
        {
            List<Estadio> lsEstadio = Estadio.listarEstadios(conexion);
            foreach (Estadio estadio in lsEstadio)
            {
                dgvEstadios.Rows.Add(estadio.IdEst, estadio.NombreEst, estadio.LocalidadEst, estadio.AforoEst);
            }
        }

        private void btnRegistrarEst_Click(object sender, EventArgs e)
        {
            //int _idEst = int.Parse(txtIdEst.Text);
            string _nombreEst = txtNombreEst.Text;
            string _localidadEst = txtLocalidadEst.Text;
            int _aforoEst = int.Parse(txtAforoEst.Text);

            Estadio estadio = new Estadio();
            estadio.NombreEst = _nombreEst;
            estadio.LocalidadEst = _localidadEst;
            estadio.AforoEst = _aforoEst;

            int cantFilasAfectadas = Estadio.insertarEstadio(conexion,estadio);

            if (cantFilasAfectadas>0)
            {
                dgvEstadios.Rows.Clear();
                listarEstadios();
                limpiarControles();
                MessageBox.Show(Constantes.MSG_INSERTAR_EXITO,Constantes.TITLE_MSG_EXITO,MessageBoxButtons.OK,MessageBoxIcon.Information);
                
            }
            else
            {
                MessageBox.Show(Constantes.MSG_ERROR_GENERICO,Constantes.TITLE_MSG_ERROR,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void FrmRegistrarEstadio_Load(object sender, EventArgs e)
        {
            listarEstadios();
        }

        private void btnEliminarEst_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Constantes.MSG_CONFIRMA_ELIMINAR,Constantes.TITLE_MSG_CONFIRMACION,MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
            {
                DataGridViewRow fila = dgvEstadios.CurrentRow;
                int _idEst = Convert.ToInt32(dgvEstadios[0, fila.Index].Value);

                int cantFilasAfectadas = Estadio.eliminarEstadio(conexion, _idEst);

                if (cantFilasAfectadas > 0)
                {
                    dgvEstadios.Rows.Clear();
                    listarEstadios();
                    limpiarControles();
                    MessageBox.Show(Constantes.MSG_ELIMINAR_EXITO, Constantes.TITLE_MSG_EXITO, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    MessageBox.Show(Constantes.MSG_ERROR_GENERICO, Constantes.TITLE_MSG_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnActualizarEst_Click(object sender, EventArgs e)
        {
            int _idEst = int.Parse(txtIdEst.Text);
            string _nombreEst = txtNombreEst.Text;
            string _localidadEst = txtLocalidadEst.Text;
            int _aforoEst = int.Parse(txtAforoEst.Text);

            Estadio estadio = new Estadio();
            estadio.IdEst = _idEst;
            estadio.NombreEst = _nombreEst;
            estadio.LocalidadEst = _localidadEst;
            estadio.AforoEst = _aforoEst;

            int cantFilasAfectadas = Estadio.actualizarEstadio(conexion, estadio);

            if (cantFilasAfectadas > 0)
            {
                dgvEstadios.Rows.Clear();
                listarEstadios();
                limpiarControles();
                MessageBox.Show(Constantes.MSG_ACTUALIZAR_EXITO, Constantes.TITLE_MSG_EXITO, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(Constantes.MSG_ERROR_GENERICO, Constantes.TITLE_MSG_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvEstadios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgvEstadios_Click(object sender, EventArgs e)
        {

            DataGridViewRow fila = dgvEstadios.CurrentRow;
            int _idEst = Convert.ToInt32(dgvEstadios[0, fila.Index].Value);
            string _nombreEst = dgvEstadios[1, fila.Index].Value.ToString();
            string _localidadEst = dgvEstadios[2, fila.Index].Value.ToString();
            int _aforoEst = Convert.ToInt32(dgvEstadios[3, fila.Index].Value);

            txtIdEst.Text = _idEst.ToString();
            txtNombreEst.Text = _nombreEst.ToString();
            txtLocalidadEst.Text = _localidadEst.ToString();
            txtAforoEst.Text = _aforoEst.ToString();

            btnRegistrarEst.Enabled = false;
        }

        private void txtIdEst_TextChanged(object sender, EventArgs e)
        {
            if (!txtIdEst.Text.Equals(String.Empty))
            {
                btnEliminarEst.Enabled = true;
                btnActualizarEst.Enabled = true;
            }
            else
            {
                btnEliminarEst.Enabled = false;
                btnActualizarEst.Enabled = false;
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }

        private void txtAforoEst_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show(Constantes.MSG_ERROR_NUMERO, Constantes.TITLE_MSG_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtNombreEst_TextChanged(object sender, EventArgs e)
        {
            validaInsertar();
        }

        private void txtLocalidadEst_TextChanged(object sender, EventArgs e)
        {
            validaInsertar();
        }

        private void txtAforoEst_TextChanged(object sender, EventArgs e)
        {
            validaInsertar();
        }
    }
}
