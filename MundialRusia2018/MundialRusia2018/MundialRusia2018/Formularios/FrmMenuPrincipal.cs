﻿using MundialRusia2018.Clases;
using MundialRusia2018.Formularios;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MundialRusia2018
{
    public partial class FrmMenuPrincipal : Form
    {
        MySqlConnection conexion = ConexionDB.obtenerConexion();

        public FrmMenuPrincipal()
        {
            InitializeComponent();
        }

        private void equiposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRegistrarEquipo frmRegistrarEquipo = new FrmRegistrarEquipo();
            frmRegistrarEquipo.MdiParent = this;
            frmRegistrarEquipo.Show();
        }

        private void gruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRegistrarGrupo frmRegistrarGrupo = new FrmRegistrarGrupo();
            frmRegistrarGrupo.MdiParent = this;
            frmRegistrarGrupo.Show();
        }

        private void estadiosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRegistrarEstadio frmRegistrarEstadio = new FrmRegistrarEstadio();
            frmRegistrarEstadio.MdiParent = this;
            frmRegistrarEstadio.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void FrmMenuPrincipal_Load(object sender, EventArgs e)
        {
     

        }

        private void registrarNuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRegistrarJugador frmRegistrarJugador = new FrmRegistrarJugador();
            frmRegistrarJugador.MdiParent = this;
            frmRegistrarJugador.Show();
        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRegistrarEncuentro frmRegistrarEncuentro = new FrmRegistrarEncuentro();
            frmRegistrarEncuentro.MdiParent = this;
            frmRegistrarEncuentro.Show();
        }

        private void reporteDeEstadiosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmReporteEstadios frmReporteEstadios = new FrmReporteEstadios();
            frmReporteEstadios.MdiParent = this;
            frmReporteEstadios.Show();
        }
    }
}
