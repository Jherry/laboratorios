﻿namespace MundialRusia2018.Formularios
{
    partial class FrmRegistrarEncuentro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegistrarEncuentro));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbFinal = new System.Windows.Forms.RadioButton();
            this.rbSemiFinal = new System.Windows.Forms.RadioButton();
            this.rbCuartos = new System.Windows.Forms.RadioButton();
            this.rbOctavos = new System.Windows.Forms.RadioButton();
            this.rbGrupos = new System.Windows.Forms.RadioButton();
            this.lstEquipos = new System.Windows.Forms.ListBox();
            this.lstEstadios = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSeleccionar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnLimpiarEnc = new System.Windows.Forms.Button();
            this.btnSalirEnc = new System.Windows.Forms.Button();
            this.btnActualizarEnc = new System.Windows.Forms.Button();
            this.btnEliminarEnc = new System.Windows.Forms.Button();
            this.btnRegistrarEnc = new System.Windows.Forms.Button();
            this.gbGrupos = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboGrupos = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbGrupos.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbFinal);
            this.groupBox1.Controls.Add(this.rbSemiFinal);
            this.groupBox1.Controls.Add(this.rbCuartos);
            this.groupBox1.Controls.Add(this.rbOctavos);
            this.groupBox1.Controls.Add(this.rbGrupos);
            this.groupBox1.Location = new System.Drawing.Point(76, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(877, 62);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleccione una etapa de juego.";
            // 
            // rbFinal
            // 
            this.rbFinal.AutoSize = true;
            this.rbFinal.Location = new System.Drawing.Point(439, 27);
            this.rbFinal.Name = "rbFinal";
            this.rbFinal.Size = new System.Drawing.Size(47, 17);
            this.rbFinal.TabIndex = 5;
            this.rbFinal.TabStop = true;
            this.rbFinal.Text = "Final";
            this.rbFinal.UseVisualStyleBackColor = true;
            // 
            // rbSemiFinal
            // 
            this.rbSemiFinal.AutoSize = true;
            this.rbSemiFinal.Location = new System.Drawing.Point(325, 27);
            this.rbSemiFinal.Name = "rbSemiFinal";
            this.rbSemiFinal.Size = new System.Drawing.Size(73, 17);
            this.rbSemiFinal.TabIndex = 4;
            this.rbSemiFinal.TabStop = true;
            this.rbSemiFinal.Text = "Semi Final";
            this.rbSemiFinal.UseVisualStyleBackColor = true;
            // 
            // rbCuartos
            // 
            this.rbCuartos.AutoSize = true;
            this.rbCuartos.Location = new System.Drawing.Point(236, 27);
            this.rbCuartos.Name = "rbCuartos";
            this.rbCuartos.Size = new System.Drawing.Size(61, 17);
            this.rbCuartos.TabIndex = 3;
            this.rbCuartos.TabStop = true;
            this.rbCuartos.Text = "Cuartos";
            this.rbCuartos.UseVisualStyleBackColor = true;
            // 
            // rbOctavos
            // 
            this.rbOctavos.AutoSize = true;
            this.rbOctavos.Location = new System.Drawing.Point(152, 27);
            this.rbOctavos.Name = "rbOctavos";
            this.rbOctavos.Size = new System.Drawing.Size(65, 17);
            this.rbOctavos.TabIndex = 2;
            this.rbOctavos.TabStop = true;
            this.rbOctavos.Text = "Octavos";
            this.rbOctavos.UseVisualStyleBackColor = true;
            // 
            // rbGrupos
            // 
            this.rbGrupos.AutoSize = true;
            this.rbGrupos.Location = new System.Drawing.Point(32, 27);
            this.rbGrupos.Name = "rbGrupos";
            this.rbGrupos.Size = new System.Drawing.Size(90, 17);
            this.rbGrupos.TabIndex = 1;
            this.rbGrupos.TabStop = true;
            this.rbGrupos.Text = "Etapa Grupos";
            this.rbGrupos.UseVisualStyleBackColor = true;
            this.rbGrupos.CheckedChanged += new System.EventHandler(this.rbGrupos_CheckedChanged);
            // 
            // lstEquipos
            // 
            this.lstEquipos.FormattingEnabled = true;
            this.lstEquipos.Location = new System.Drawing.Point(29, 203);
            this.lstEquipos.Name = "lstEquipos";
            this.lstEquipos.Size = new System.Drawing.Size(219, 199);
            this.lstEquipos.TabIndex = 3;
            // 
            // lstEstadios
            // 
            this.lstEstadios.FormattingEnabled = true;
            this.lstEstadios.Location = new System.Drawing.Point(263, 123);
            this.lstEstadios.Name = "lstEstadios";
            this.lstEstadios.Size = new System.Drawing.Size(207, 277);
            this.lstEstadios.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Equipos de Futbol";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(260, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Estadios";
            // 
            // btnSeleccionar
            // 
            this.btnSeleccionar.Location = new System.Drawing.Point(476, 215);
            this.btnSeleccionar.Name = "btnSeleccionar";
            this.btnSeleccionar.Size = new System.Drawing.Size(104, 23);
            this.btnSeleccionar.TabIndex = 8;
            this.btnSeleccionar.Text = "Seleccionar >>";
            this.btnSeleccionar.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(627, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Arbitro:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "CARLOS MANRIQUE CORONADO",
            "ALBERTO RODRIGUEZ GASPAR",
            "MARIA GUTIERREZ MARQUEZ"});
            this.comboBox1.Location = new System.Drawing.Point(692, 101);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(434, 21);
            this.comboBox1.TabIndex = 10;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.dateTimePicker1);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Location = new System.Drawing.Point(630, 154);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(496, 161);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Equiopos seleccionados";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(31, 75);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(173, 20);
            this.textBox1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(223, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 24);
            this.label5.TabIndex = 12;
            this.label5.Text = "VS";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(278, 76);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(194, 20);
            this.textBox2.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(46, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 20);
            this.label6.TabIndex = 14;
            this.label6.Text = "PRIMER EQUIPO";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(288, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(171, 20);
            this.label7.TabIndex = 15;
            this.label7.Text = "SEGUNDO EQUIPO";
            // 
            // btnLimpiarEnc
            // 
            this.btnLimpiarEnc.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiarEnc.Image")));
            this.btnLimpiarEnc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpiarEnc.Location = new System.Drawing.Point(601, 350);
            this.btnLimpiarEnc.Name = "btnLimpiarEnc";
            this.btnLimpiarEnc.Size = new System.Drawing.Size(117, 34);
            this.btnLimpiarEnc.TabIndex = 20;
            this.btnLimpiarEnc.Text = "Limpiar";
            this.btnLimpiarEnc.UseVisualStyleBackColor = true;
            // 
            // btnSalirEnc
            // 
            this.btnSalirEnc.Image = ((System.Drawing.Image)(resources.GetObject("btnSalirEnc.Image")));
            this.btnSalirEnc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalirEnc.Location = new System.Drawing.Point(1073, 350);
            this.btnSalirEnc.Name = "btnSalirEnc";
            this.btnSalirEnc.Size = new System.Drawing.Size(117, 34);
            this.btnSalirEnc.TabIndex = 19;
            this.btnSalirEnc.Text = "Salir";
            this.btnSalirEnc.UseVisualStyleBackColor = true;
            // 
            // btnActualizarEnc
            // 
            this.btnActualizarEnc.Enabled = false;
            this.btnActualizarEnc.Image = ((System.Drawing.Image)(resources.GetObject("btnActualizarEnc.Image")));
            this.btnActualizarEnc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActualizarEnc.Location = new System.Drawing.Point(954, 350);
            this.btnActualizarEnc.Name = "btnActualizarEnc";
            this.btnActualizarEnc.Size = new System.Drawing.Size(117, 34);
            this.btnActualizarEnc.TabIndex = 18;
            this.btnActualizarEnc.Text = "Actualizar";
            this.btnActualizarEnc.UseVisualStyleBackColor = true;
            // 
            // btnEliminarEnc
            // 
            this.btnEliminarEnc.Enabled = false;
            this.btnEliminarEnc.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarEnc.Image")));
            this.btnEliminarEnc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarEnc.Location = new System.Drawing.Point(836, 350);
            this.btnEliminarEnc.Name = "btnEliminarEnc";
            this.btnEliminarEnc.Size = new System.Drawing.Size(117, 34);
            this.btnEliminarEnc.TabIndex = 17;
            this.btnEliminarEnc.Text = "Eliminar";
            this.btnEliminarEnc.UseVisualStyleBackColor = true;
            // 
            // btnRegistrarEnc
            // 
            this.btnRegistrarEnc.Enabled = false;
            this.btnRegistrarEnc.Image = ((System.Drawing.Image)(resources.GetObject("btnRegistrarEnc.Image")));
            this.btnRegistrarEnc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegistrarEnc.Location = new System.Drawing.Point(718, 350);
            this.btnRegistrarEnc.Name = "btnRegistrarEnc";
            this.btnRegistrarEnc.Size = new System.Drawing.Size(117, 34);
            this.btnRegistrarEnc.TabIndex = 16;
            this.btnRegistrarEnc.Text = "Registrar";
            this.btnRegistrarEnc.UseVisualStyleBackColor = true;
            // 
            // gbGrupos
            // 
            this.gbGrupos.Controls.Add(this.label1);
            this.gbGrupos.Controls.Add(this.cboGrupos);
            this.gbGrupos.Location = new System.Drawing.Point(29, 96);
            this.gbGrupos.Name = "gbGrupos";
            this.gbGrupos.Size = new System.Drawing.Size(219, 68);
            this.gbGrupos.TabIndex = 21;
            this.gbGrupos.TabStop = false;
            this.gbGrupos.Text = "Grupos del Mundial";
            this.gbGrupos.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 7;
            // 
            // cboGrupos
            // 
            this.cboGrupos.FormattingEnabled = true;
            this.cboGrupos.Location = new System.Drawing.Point(6, 30);
            this.cboGrupos.Name = "cboGrupos";
            this.cboGrupos.Size = new System.Drawing.Size(188, 21);
            this.cboGrupos.TabIndex = 6;
            this.cboGrupos.SelectedIndexChanged += new System.EventHandler(this.cboGrupos_SelectedIndexChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(206, 116);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 119);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Fecha/Hora del partido:";
            // 
            // FrmRegistrarEncuentro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1289, 450);
            this.Controls.Add(this.gbGrupos);
            this.Controls.Add(this.btnLimpiarEnc);
            this.Controls.Add(this.btnSalirEnc);
            this.Controls.Add(this.btnActualizarEnc);
            this.Controls.Add(this.btnEliminarEnc);
            this.Controls.Add(this.btnRegistrarEnc);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSeleccionar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lstEstadios);
            this.Controls.Add(this.lstEquipos);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmRegistrarEncuentro";
            this.Text = "FrmRegistrarEncuentro";
            this.Load += new System.EventHandler(this.FrmRegistrarEncuentro_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbGrupos.ResumeLayout(false);
            this.gbGrupos.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbFinal;
        private System.Windows.Forms.RadioButton rbSemiFinal;
        private System.Windows.Forms.RadioButton rbCuartos;
        private System.Windows.Forms.RadioButton rbOctavos;
        private System.Windows.Forms.RadioButton rbGrupos;
        private System.Windows.Forms.ListBox lstEquipos;
        private System.Windows.Forms.ListBox lstEstadios;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSeleccionar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnLimpiarEnc;
        private System.Windows.Forms.Button btnSalirEnc;
        private System.Windows.Forms.Button btnActualizarEnc;
        private System.Windows.Forms.Button btnEliminarEnc;
        private System.Windows.Forms.Button btnRegistrarEnc;
        private System.Windows.Forms.GroupBox gbGrupos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboGrupos;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}