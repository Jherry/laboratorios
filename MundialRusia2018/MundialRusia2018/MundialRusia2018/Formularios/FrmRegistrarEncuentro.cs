﻿using MundialRusia2018.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MundialRusia2018.Formularios
{
    public partial class FrmRegistrarEncuentro : Form
    {
        public FrmRegistrarEncuentro()
        {
            InitializeComponent();
        }

        MySqlConnection conexion = ConexionDB.obtenerConexion();

        public void cargarGrupos()
        {
            Dictionary<int, string> mapGrupos = new Dictionary<int, string>();
            mapGrupos = Grupo.listarGrupos(conexion);
            cboGrupos.Items.Add("--Seleccione opción--");

            for (int i = 1; i <= mapGrupos.Count(); i++)
            {
                cboGrupos.Items.Add($"GRUPO - {mapGrupos[i]}");
            }
            cboGrupos.SelectedIndex = 0;
        }
        private void rbGrupos_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGrupos.Checked)
            {
                gbGrupos.Visible = true;
            }
            else
            {
                gbGrupos.Visible = false;
            }
        }

        private void FrmRegistrarEncuentro_Load(object sender, EventArgs e)
        {
            cargarGrupos();
        }

        private void cboGrupos_SelectedIndexChanged(object sender, EventArgs e)
        {
            int _idGrup = int.Parse(cboGrupos.SelectedIndex.ToString());
            List<Equipo> lsEquipos = new List<Equipo>();

            lsEquipos = Equipo.listarEquiposByGrupo(conexion,_idGrup);
        }
    }
}
