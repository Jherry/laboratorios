﻿using MundialRusia2018.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MundialRusia2018.Formularios
{
    public partial class FrmRegistrarJugador : Form
    {
        public FrmRegistrarJugador()
        {
            InitializeComponent();
        }

        MySqlConnection conexion = ConexionDB.obtenerConexion();

        public void limpiarControlesJug()
        {
            txtIdJug.Text = String.Empty;
            txtIdEqui.Text = String.Empty;
            txtApepatJug.Text = String.Empty;
            txtApepatJug.Text = String.Empty;
            txtNombresJug.Text = String.Empty;
            txtDniJug.Text = String.Empty;
            txtNumCamisetaJug.Text = String.Empty;
        }


        public void listarJugadores()
        {
            List<Jugador> lsJugador = Jugador.listarJugadores(conexion);
            foreach (Jugador jugador in lsJugador)
            {
                dgvJugador.Rows.Add(
                    jugador.IdJug, 
                    jugador.NombresJug,
                    jugador.ApepatJug, 
                    jugador.ApematJug, 
                    jugador.FecNacJug,
                    jugador.SexoJug,
                    jugador.NumCamisetaJug,
                    jugador.DniJug,
                    jugador.ClubJug,
                    jugador.IdEqui
                    );
            }
        }

        public void cargarPosiciones()
        {
            Dictionary<int, string> mapPosiciones = new Dictionary<int, string>();
            mapPosiciones = Posicion.listasPosiciones(conexion);
            cboIdPos.Items.Add("--Seleccione opción--");

            for (int i = 1; i <= mapPosiciones.Count();i++)
            {
                cboIdPos.Items.Add($"{i} - {mapPosiciones[i]}");
            }
            cboIdPos.SelectedIndex = 0;
        }

        private void FrmRegistrarJugador_Load(object sender, EventArgs e)
        {
            listarJugadores();
            cargarPosiciones();
        }

        private void btnLimpiarJug_Click(object sender, EventArgs e)
        {
            limpiarControlesJug();
        }

        private void btnRegistrarJug_Click(object sender, EventArgs e)
        {
            //int _idJug = int.Parse(txtIdJug.Text);
            //int _idEquiJug = (txtIdEqui.Text.Equals(String.Empty)?null:int.Parse(txtIdEqui.Text));
            int _idEquiJug = 1;
            string _apepatJug = txtApepatJug.Text;
            string _apematJug = txtApematJug.Text;
            string _nombresJug = txtNombresJug.Text;
            string _fecNacJug = dtpFecNacJug.Value.ToString("yyyy/MM/dd");
            string _sexoJug = (rbnSexoM.Checked ? Constantes.MASCULINO: Constantes.FEMENINO);
            int _numCamisetaJug = int.Parse(txtNumCamisetaJug.Text);
            string _dniJug = txtDniJug.Text;
            string _clubJug = txtClubJug.Text;

            Jugador jugador = new Jugador();
            jugador.IdEqui = _idEquiJug;
            jugador.ApepatJug = _apepatJug;
            jugador.ApematJug = _apematJug;
            jugador.NombresJug = _nombresJug;
            jugador.FecNacJug = _fecNacJug;
            jugador.SexoJug = Char.Parse(_sexoJug);
            jugador.DniJug = _dniJug;
            jugador.ClubJug = _clubJug;

            int filasNumAfectadas = Jugador.insertarJugador(conexion,jugador);
        }

        private void txtNumCamisetaJug_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show(Constantes.MSG_ERROR_NUMERO, Constantes.TITLE_MSG_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtDniJug_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show(Constantes.MSG_ERROR_NUMERO, Constantes.TITLE_MSG_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void btnEliminarJug_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Constantes.MSG_CONFIRMA_ELIMINAR, Constantes.TITLE_MSG_CONFIRMACION, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                DataGridViewRow fila = dgvJugador.CurrentRow;
                int _idJug = Convert.ToInt32(dgvJugador[0, fila.Index].Value);

                int cantFilasAfectadas = Jugador.eliminarJugador(conexion, _idJug);

                if (cantFilasAfectadas > 0)
                {
                    dgvJugador.Rows.Clear();
                    listarJugadores();
                    limpiarControlesJug();
                    MessageBox.Show(Constantes.MSG_ELIMINAR_EXITO, Constantes.TITLE_MSG_EXITO, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    MessageBox.Show(Constantes.MSG_ERROR_GENERICO, Constantes.TITLE_MSG_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void dgvJugador_Click(object sender, EventArgs e)
        {
            DataGridViewRow fila = dgvJugador.CurrentRow;
            int _idJug = Convert.ToInt32(dgvJugador[0, fila.Index].Value);
            string _nombreJug = dgvJugador[1, fila.Index].Value.ToString();
            string _apepatJug = dgvJugador[2, fila.Index].Value.ToString();
            string _apematJug = dgvJugador[3, fila.Index].Value.ToString();
            //string _fecnacJug = dgvJugador[4, fila.Index].Value.ToString();
            char _sexoJug = char.Parse(dgvJugador[5, fila.Index].Value.ToString());
            int _numCamisetaJug = Convert.ToInt32(dgvJugador[6, fila.Index].Value);
            string _dniJug = dgvJugador[7, fila.Index].Value.ToString();
            string _clubJug = dgvJugador[8, fila.Index].Value.ToString();

            txtIdJug.Text = _idJug.ToString();
            txtNombresJug.Text = _nombreJug.ToString();
            txtApepatJug.Text = _apepatJug.ToString();
            txtApematJug.Text = _apematJug.ToString();

            if (_sexoJug == 'M')
            {
                rbnSexoM.Checked = true;
            }
            else
            {
                rbnSexoF.Checked = true;
            }

            txtNumCamisetaJug.Text = _numCamisetaJug.ToString();
            txtDniJug.Text = _dniJug.ToString();
            txtClubJug.Text = _clubJug.ToString();

            btnActualizarJug.Enabled = true;
            btnEliminarJug.Enabled = true;
            txtIdEqui.Enabled = true;
        }

        private void btnActualizarJug_Click(object sender, EventArgs e)
        {
            int _idJug = int.Parse(txtIdJug.Text);
            int _idEqui = int.Parse(txtIdEqui.Text);
            string _apepatJug = txtApepatJug.Text;
            string _apematJug = txtApematJug.Text;
            string _nombresJug = txtNombresJug.Text;
            string _fecNacJug = dtpFecNacJug.Value.ToString("yyyy/MM/dd");
            string _sexoJug = (rbnSexoM.Checked ? Constantes.MASCULINO : Constantes.FEMENINO);
            int _numCamisetaJug = int.Parse(txtNumCamisetaJug.Text);
            string _dniJug = txtDniJug.Text;
            string _clubJug = txtClubJug.Text;

            Jugador jugador = new Jugador();
            jugador.IdJug = _idJug;
            jugador.IdEqui = _idEqui;
            jugador.ApepatJug = _apepatJug;
            jugador.ApematJug = _apematJug;
            jugador.NombresJug = _nombresJug;
            jugador.FecNacJug = _fecNacJug;
            jugador.SexoJug = Char.Parse(_sexoJug);
            jugador.DniJug = _dniJug;
            jugador.ClubJug = _clubJug;

            int cantFilasAfectadas = Jugador.actualizarJugador(conexion, jugador);

            if (cantFilasAfectadas > 0)
            {
                dgvJugador.Rows.Clear();
                listarJugadores();
                limpiarControlesJug();
                MessageBox.Show(Constantes.MSG_ACTUALIZAR_EXITO, Constantes.TITLE_MSG_EXITO, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(Constantes.MSG_ERROR_GENERICO, Constantes.TITLE_MSG_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }

}
