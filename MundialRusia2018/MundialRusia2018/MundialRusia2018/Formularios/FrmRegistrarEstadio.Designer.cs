﻿namespace MundialRusia2018.Formularios
{
    partial class FrmRegistrarEstadio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegistrarEstadio));
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtAforoEst = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLocalidadEst = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNombreEst = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIdEst = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvEstadios = new System.Windows.Forms.DataGridView();
            this.idEst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreEst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.localidadEst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aforoEst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRegistrarEst = new System.Windows.Forms.Button();
            this.btnEliminarEst = new System.Windows.Forms.Button();
            this.btnActualizarEst = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstadios)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(406, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(260, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mantenimiento de estadios";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtAforoEst);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtLocalidadEst);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtNombreEst);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtIdEst);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(37, 67);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(439, 151);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos de Estadio";
            // 
            // txtAforoEst
            // 
            this.txtAforoEst.Location = new System.Drawing.Point(145, 104);
            this.txtAforoEst.Name = "txtAforoEst";
            this.txtAforoEst.Size = new System.Drawing.Size(73, 20);
            this.txtAforoEst.TabIndex = 16;
            this.txtAforoEst.TextChanged += new System.EventHandler(this.txtAforoEst_TextChanged);
            this.txtAforoEst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAforoEst_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Aforo de Estadio:";
            // 
            // txtLocalidadEst
            // 
            this.txtLocalidadEst.Location = new System.Drawing.Point(145, 78);
            this.txtLocalidadEst.Name = "txtLocalidadEst";
            this.txtLocalidadEst.Size = new System.Drawing.Size(273, 20);
            this.txtLocalidadEst.TabIndex = 14;
            this.txtLocalidadEst.TextChanged += new System.EventHandler(this.txtLocalidadEst_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Localidad de Estadio:";
            // 
            // txtNombreEst
            // 
            this.txtNombreEst.Location = new System.Drawing.Point(145, 52);
            this.txtNombreEst.Name = "txtNombreEst";
            this.txtNombreEst.Size = new System.Drawing.Size(273, 20);
            this.txtNombreEst.TabIndex = 12;
            this.txtNombreEst.TextChanged += new System.EventHandler(this.txtNombreEst_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Nombre de Estadio:";
            // 
            // txtIdEst
            // 
            this.txtIdEst.Enabled = false;
            this.txtIdEst.Location = new System.Drawing.Point(145, 26);
            this.txtIdEst.Name = "txtIdEst";
            this.txtIdEst.Size = new System.Drawing.Size(73, 20);
            this.txtIdEst.TabIndex = 10;
            this.txtIdEst.TextChanged += new System.EventHandler(this.txtIdEst_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "ID:";
            // 
            // dgvEstadios
            // 
            this.dgvEstadios.AllowUserToAddRows = false;
            this.dgvEstadios.AllowUserToDeleteRows = false;
            this.dgvEstadios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEstadios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idEst,
            this.nombreEst,
            this.localidadEst,
            this.aforoEst});
            this.dgvEstadios.Location = new System.Drawing.Point(509, 68);
            this.dgvEstadios.Name = "dgvEstadios";
            this.dgvEstadios.ReadOnly = true;
            this.dgvEstadios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEstadios.Size = new System.Drawing.Size(656, 150);
            this.dgvEstadios.TabIndex = 10;
            this.dgvEstadios.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEstadios_CellContentClick);
            this.dgvEstadios.Click += new System.EventHandler(this.dgvEstadios_Click);
            // 
            // idEst
            // 
            this.idEst.HeaderText = "ID";
            this.idEst.Name = "idEst";
            this.idEst.ReadOnly = true;
            this.idEst.Width = 50;
            // 
            // nombreEst
            // 
            this.nombreEst.HeaderText = "NOMBRE";
            this.nombreEst.Name = "nombreEst";
            this.nombreEst.ReadOnly = true;
            this.nombreEst.Width = 250;
            // 
            // localidadEst
            // 
            this.localidadEst.HeaderText = "LOCALIDAD";
            this.localidadEst.Name = "localidadEst";
            this.localidadEst.ReadOnly = true;
            this.localidadEst.Width = 250;
            // 
            // aforoEst
            // 
            this.aforoEst.HeaderText = "AFORO";
            this.aforoEst.Name = "aforoEst";
            this.aforoEst.ReadOnly = true;
            this.aforoEst.Width = 50;
            // 
            // btnRegistrarEst
            // 
            this.btnRegistrarEst.Enabled = false;
            this.btnRegistrarEst.Image = ((System.Drawing.Image)(resources.GetObject("btnRegistrarEst.Image")));
            this.btnRegistrarEst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegistrarEst.Location = new System.Drawing.Point(154, 235);
            this.btnRegistrarEst.Name = "btnRegistrarEst";
            this.btnRegistrarEst.Size = new System.Drawing.Size(117, 34);
            this.btnRegistrarEst.TabIndex = 11;
            this.btnRegistrarEst.Text = "Registrar";
            this.btnRegistrarEst.UseVisualStyleBackColor = true;
            this.btnRegistrarEst.Click += new System.EventHandler(this.btnRegistrarEst_Click);
            // 
            // btnEliminarEst
            // 
            this.btnEliminarEst.Enabled = false;
            this.btnEliminarEst.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarEst.Image")));
            this.btnEliminarEst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarEst.Location = new System.Drawing.Point(272, 235);
            this.btnEliminarEst.Name = "btnEliminarEst";
            this.btnEliminarEst.Size = new System.Drawing.Size(117, 34);
            this.btnEliminarEst.TabIndex = 12;
            this.btnEliminarEst.Text = "Eliminar";
            this.btnEliminarEst.UseVisualStyleBackColor = true;
            this.btnEliminarEst.Click += new System.EventHandler(this.btnEliminarEst_Click);
            // 
            // btnActualizarEst
            // 
            this.btnActualizarEst.Enabled = false;
            this.btnActualizarEst.Image = ((System.Drawing.Image)(resources.GetObject("btnActualizarEst.Image")));
            this.btnActualizarEst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActualizarEst.Location = new System.Drawing.Point(390, 235);
            this.btnActualizarEst.Name = "btnActualizarEst";
            this.btnActualizarEst.Size = new System.Drawing.Size(117, 34);
            this.btnActualizarEst.TabIndex = 13;
            this.btnActualizarEst.Text = "Actualizar";
            this.btnActualizarEst.UseVisualStyleBackColor = true;
            this.btnActualizarEst.Click += new System.EventHandler(this.btnActualizarEst_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalir.Location = new System.Drawing.Point(509, 235);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(117, 34);
            this.btnSalir.TabIndex = 14;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.Image")));
            this.btnLimpiar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpiar.Location = new System.Drawing.Point(37, 235);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(117, 34);
            this.btnLimpiar.TabIndex = 15;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // FrmRegistrarEstadio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1203, 341);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnActualizarEst);
            this.Controls.Add(this.btnEliminarEst);
            this.Controls.Add(this.btnRegistrarEst);
            this.Controls.Add(this.dgvEstadios);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "FrmRegistrarEstadio";
            this.Text = "Mantenimiento de estadios";
            this.Load += new System.EventHandler(this.FrmRegistrarEstadio_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstadios)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtAforoEst;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLocalidadEst;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNombreEst;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIdEst;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvEstadios;
        private System.Windows.Forms.DataGridViewTextBoxColumn idEst;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreEst;
        private System.Windows.Forms.DataGridViewTextBoxColumn localidadEst;
        private System.Windows.Forms.DataGridViewTextBoxColumn aforoEst;
        private System.Windows.Forms.Button btnRegistrarEst;
        private System.Windows.Forms.Button btnEliminarEst;
        private System.Windows.Forms.Button btnActualizarEst;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnLimpiar;
    }
}