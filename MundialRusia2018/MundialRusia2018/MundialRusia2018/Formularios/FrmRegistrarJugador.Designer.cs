﻿namespace MundialRusia2018.Formularios
{
    partial class FrmRegistrarJugador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegistrarJugador));
            this.label1 = new System.Windows.Forms.Label();
            this.txtIdJug = new System.Windows.Forms.TextBox();
            this.txtIdEqui = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombresJug = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtApepatJug = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtApematJug = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.rbnSexoM = new System.Windows.Forms.RadioButton();
            this.rbnSexoF = new System.Windows.Forms.RadioButton();
            this.txtNumCamisetaJug = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtClubJug = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboIdPos = new System.Windows.Forms.ComboBox();
            this.txtDniJug = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dgvJugador = new System.Windows.Forms.DataGridView();
            this.dtpFecNacJug = new System.Windows.Forms.DateTimePicker();
            this.btnLimpiarJug = new System.Windows.Forms.Button();
            this.btnSalirJug = new System.Windows.Forms.Button();
            this.btnActualizarJug = new System.Windows.Forms.Button();
            this.btnEliminarJug = new System.Windows.Forms.Button();
            this.btnRegistrarJug = new System.Windows.Forms.Button();
            this.idJug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombresJug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apepatJug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apematJug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecNacJug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sexoJug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numCamisetaJug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dniJug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clubJug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idEqui = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJugador)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Jugador:";
            // 
            // txtIdJug
            // 
            this.txtIdJug.Enabled = false;
            this.txtIdJug.Location = new System.Drawing.Point(121, 33);
            this.txtIdJug.Name = "txtIdJug";
            this.txtIdJug.Size = new System.Drawing.Size(58, 20);
            this.txtIdJug.TabIndex = 1;
            // 
            // txtIdEqui
            // 
            this.txtIdEqui.Enabled = false;
            this.txtIdEqui.Location = new System.Drawing.Point(313, 33);
            this.txtIdEqui.Name = "txtIdEqui";
            this.txtIdEqui.Size = new System.Drawing.Size(52, 20);
            this.txtIdEqui.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(255, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "ID Equipo:";
            // 
            // txtNombresJug
            // 
            this.txtNombresJug.Location = new System.Drawing.Point(146, 78);
            this.txtNombresJug.Name = "txtNombresJug";
            this.txtNombresJug.Size = new System.Drawing.Size(130, 20);
            this.txtNombresJug.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nombre Jugador:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Fec. Nacimiento:";
            // 
            // txtApepatJug
            // 
            this.txtApepatJug.Location = new System.Drawing.Point(146, 128);
            this.txtApepatJug.Name = "txtApepatJug";
            this.txtApepatJug.Size = new System.Drawing.Size(130, 20);
            this.txtApepatJug.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(53, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Apellido Paterno:";
            // 
            // txtApematJug
            // 
            this.txtApematJug.Location = new System.Drawing.Point(146, 103);
            this.txtApematJug.Name = "txtApematJug";
            this.txtApematJug.Size = new System.Drawing.Size(130, 20);
            this.txtApematJug.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(53, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Apellido Materno:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(53, 181);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Sexo:";
            // 
            // rbnSexoM
            // 
            this.rbnSexoM.AutoSize = true;
            this.rbnSexoM.Checked = true;
            this.rbnSexoM.Location = new System.Drawing.Point(146, 181);
            this.rbnSexoM.Name = "rbnSexoM";
            this.rbnSexoM.Size = new System.Drawing.Size(34, 17);
            this.rbnSexoM.TabIndex = 13;
            this.rbnSexoM.TabStop = true;
            this.rbnSexoM.Text = "M";
            this.rbnSexoM.UseVisualStyleBackColor = true;
            // 
            // rbnSexoF
            // 
            this.rbnSexoF.AutoSize = true;
            this.rbnSexoF.Location = new System.Drawing.Point(196, 181);
            this.rbnSexoF.Name = "rbnSexoF";
            this.rbnSexoF.Size = new System.Drawing.Size(31, 17);
            this.rbnSexoF.TabIndex = 14;
            this.rbnSexoF.Text = "F";
            this.rbnSexoF.UseVisualStyleBackColor = true;
            // 
            // txtNumCamisetaJug
            // 
            this.txtNumCamisetaJug.Location = new System.Drawing.Point(146, 205);
            this.txtNumCamisetaJug.Name = "txtNumCamisetaJug";
            this.txtNumCamisetaJug.Size = new System.Drawing.Size(57, 20);
            this.txtNumCamisetaJug.TabIndex = 16;
            this.txtNumCamisetaJug.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumCamisetaJug_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(55, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Num. Camiseta:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(55, 230);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Posicion:";
            // 
            // txtClubJug
            // 
            this.txtClubJug.Location = new System.Drawing.Point(146, 253);
            this.txtClubJug.Name = "txtClubJug";
            this.txtClubJug.Size = new System.Drawing.Size(130, 20);
            this.txtClubJug.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(55, 253);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Club Deportivo:";
            // 
            // cboIdPos
            // 
            this.cboIdPos.FormattingEnabled = true;
            this.cboIdPos.Location = new System.Drawing.Point(146, 228);
            this.cboIdPos.Name = "cboIdPos";
            this.cboIdPos.Size = new System.Drawing.Size(130, 21);
            this.cboIdPos.TabIndex = 21;
            // 
            // txtDniJug
            // 
            this.txtDniJug.Location = new System.Drawing.Point(484, 33);
            this.txtDniJug.MaxLength = 8;
            this.txtDniJug.Name = "txtDniJug";
            this.txtDniJug.Size = new System.Drawing.Size(151, 20);
            this.txtDniJug.TabIndex = 23;
            this.txtDniJug.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDniJug_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(426, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "DNI:";
            // 
            // dgvJugador
            // 
            this.dgvJugador.AllowUserToAddRows = false;
            this.dgvJugador.AllowUserToDeleteRows = false;
            this.dgvJugador.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvJugador.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idJug,
            this.nombresJug,
            this.apepatJug,
            this.apematJug,
            this.fecNacJug,
            this.sexoJug,
            this.numCamisetaJug,
            this.dniJug,
            this.clubJug,
            this.idEqui});
            this.dgvJugador.Location = new System.Drawing.Point(298, 75);
            this.dgvJugador.Name = "dgvJugador";
            this.dgvJugador.ReadOnly = true;
            this.dgvJugador.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvJugador.Size = new System.Drawing.Size(1038, 198);
            this.dgvJugador.TabIndex = 24;
            this.dgvJugador.Click += new System.EventHandler(this.dgvJugador_Click);
            // 
            // dtpFecNacJug
            // 
            this.dtpFecNacJug.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecNacJug.Location = new System.Drawing.Point(146, 156);
            this.dtpFecNacJug.Name = "dtpFecNacJug";
            this.dtpFecNacJug.Size = new System.Drawing.Size(130, 20);
            this.dtpFecNacJug.TabIndex = 25;
            // 
            // btnLimpiarJug
            // 
            this.btnLimpiarJug.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiarJug.Image")));
            this.btnLimpiarJug.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpiarJug.Location = new System.Drawing.Point(453, 292);
            this.btnLimpiarJug.Name = "btnLimpiarJug";
            this.btnLimpiarJug.Size = new System.Drawing.Size(117, 34);
            this.btnLimpiarJug.TabIndex = 30;
            this.btnLimpiarJug.Text = "Limpiar";
            this.btnLimpiarJug.UseVisualStyleBackColor = true;
            this.btnLimpiarJug.Click += new System.EventHandler(this.btnLimpiarJug_Click);
            // 
            // btnSalirJug
            // 
            this.btnSalirJug.Image = ((System.Drawing.Image)(resources.GetObject("btnSalirJug.Image")));
            this.btnSalirJug.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalirJug.Location = new System.Drawing.Point(925, 292);
            this.btnSalirJug.Name = "btnSalirJug";
            this.btnSalirJug.Size = new System.Drawing.Size(117, 34);
            this.btnSalirJug.TabIndex = 29;
            this.btnSalirJug.Text = "Salir";
            this.btnSalirJug.UseVisualStyleBackColor = true;
            // 
            // btnActualizarJug
            // 
            this.btnActualizarJug.Enabled = false;
            this.btnActualizarJug.Image = ((System.Drawing.Image)(resources.GetObject("btnActualizarJug.Image")));
            this.btnActualizarJug.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActualizarJug.Location = new System.Drawing.Point(806, 292);
            this.btnActualizarJug.Name = "btnActualizarJug";
            this.btnActualizarJug.Size = new System.Drawing.Size(117, 34);
            this.btnActualizarJug.TabIndex = 28;
            this.btnActualizarJug.Text = "Actualizar";
            this.btnActualizarJug.UseVisualStyleBackColor = true;
            this.btnActualizarJug.Click += new System.EventHandler(this.btnActualizarJug_Click);
            // 
            // btnEliminarJug
            // 
            this.btnEliminarJug.Enabled = false;
            this.btnEliminarJug.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarJug.Image")));
            this.btnEliminarJug.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarJug.Location = new System.Drawing.Point(688, 292);
            this.btnEliminarJug.Name = "btnEliminarJug";
            this.btnEliminarJug.Size = new System.Drawing.Size(117, 34);
            this.btnEliminarJug.TabIndex = 27;
            this.btnEliminarJug.Text = "Eliminar";
            this.btnEliminarJug.UseVisualStyleBackColor = true;
            this.btnEliminarJug.Click += new System.EventHandler(this.btnEliminarJug_Click);
            // 
            // btnRegistrarJug
            // 
            this.btnRegistrarJug.Image = ((System.Drawing.Image)(resources.GetObject("btnRegistrarJug.Image")));
            this.btnRegistrarJug.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegistrarJug.Location = new System.Drawing.Point(570, 292);
            this.btnRegistrarJug.Name = "btnRegistrarJug";
            this.btnRegistrarJug.Size = new System.Drawing.Size(117, 34);
            this.btnRegistrarJug.TabIndex = 26;
            this.btnRegistrarJug.Text = "Registrar";
            this.btnRegistrarJug.UseVisualStyleBackColor = true;
            this.btnRegistrarJug.Click += new System.EventHandler(this.btnRegistrarJug_Click);
            // 
            // idJug
            // 
            this.idJug.HeaderText = "ID";
            this.idJug.Name = "idJug";
            this.idJug.ReadOnly = true;
            this.idJug.Width = 50;
            // 
            // nombresJug
            // 
            this.nombresJug.HeaderText = "NOMBRES";
            this.nombresJug.Name = "nombresJug";
            this.nombresJug.ReadOnly = true;
            // 
            // apepatJug
            // 
            this.apepatJug.HeaderText = "APELLIDO PATERNO";
            this.apepatJug.Name = "apepatJug";
            this.apepatJug.ReadOnly = true;
            this.apepatJug.Width = 150;
            // 
            // apematJug
            // 
            this.apematJug.HeaderText = "APELLIDO MATERNO";
            this.apematJug.Name = "apematJug";
            this.apematJug.ReadOnly = true;
            this.apematJug.Width = 150;
            // 
            // fecNacJug
            // 
            this.fecNacJug.HeaderText = "FECHA NACIMIENTO";
            this.fecNacJug.Name = "fecNacJug";
            this.fecNacJug.ReadOnly = true;
            // 
            // sexoJug
            // 
            this.sexoJug.HeaderText = "SEXO";
            this.sexoJug.Name = "sexoJug";
            this.sexoJug.ReadOnly = true;
            this.sexoJug.Width = 50;
            // 
            // numCamisetaJug
            // 
            this.numCamisetaJug.HeaderText = "NRO CAMISETA";
            this.numCamisetaJug.Name = "numCamisetaJug";
            this.numCamisetaJug.ReadOnly = true;
            this.numCamisetaJug.Width = 50;
            // 
            // dniJug
            // 
            this.dniJug.HeaderText = "DNI";
            this.dniJug.Name = "dniJug";
            this.dniJug.ReadOnly = true;
            // 
            // clubJug
            // 
            this.clubJug.HeaderText = "CLUB";
            this.clubJug.Name = "clubJug";
            this.clubJug.ReadOnly = true;
            this.clubJug.Width = 150;
            // 
            // idEqui
            // 
            this.idEqui.HeaderText = "ID EQUI";
            this.idEqui.Name = "idEqui";
            this.idEqui.ReadOnly = true;
            // 
            // FrmRegistrarJugador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 392);
            this.Controls.Add(this.btnLimpiarJug);
            this.Controls.Add(this.btnSalirJug);
            this.Controls.Add(this.btnActualizarJug);
            this.Controls.Add(this.btnEliminarJug);
            this.Controls.Add(this.btnRegistrarJug);
            this.Controls.Add(this.dtpFecNacJug);
            this.Controls.Add(this.dgvJugador);
            this.Controls.Add(this.txtDniJug);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cboIdPos);
            this.Controls.Add(this.txtClubJug);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtNumCamisetaJug);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.rbnSexoF);
            this.Controls.Add(this.rbnSexoM);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtApepatJug);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtApematJug);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNombresJug);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtIdEqui);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIdJug);
            this.Controls.Add(this.label1);
            this.Name = "FrmRegistrarJugador";
            this.Text = "FrmRegistrarJugador";
            this.Load += new System.EventHandler(this.FrmRegistrarJugador_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvJugador)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIdJug;
        private System.Windows.Forms.TextBox txtIdEqui;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNombresJug;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtApepatJug;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtApematJug;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rbnSexoM;
        private System.Windows.Forms.RadioButton rbnSexoF;
        private System.Windows.Forms.TextBox txtNumCamisetaJug;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtClubJug;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboIdPos;
        private System.Windows.Forms.TextBox txtDniJug;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dgvJugador;
        private System.Windows.Forms.DateTimePicker dtpFecNacJug;
        private System.Windows.Forms.Button btnLimpiarJug;
        private System.Windows.Forms.Button btnSalirJug;
        private System.Windows.Forms.Button btnActualizarJug;
        private System.Windows.Forms.Button btnEliminarJug;
        private System.Windows.Forms.Button btnRegistrarJug;
        private System.Windows.Forms.DataGridViewTextBoxColumn idJug;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombresJug;
        private System.Windows.Forms.DataGridViewTextBoxColumn apepatJug;
        private System.Windows.Forms.DataGridViewTextBoxColumn apematJug;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecNacJug;
        private System.Windows.Forms.DataGridViewTextBoxColumn sexoJug;
        private System.Windows.Forms.DataGridViewTextBoxColumn numCamisetaJug;
        private System.Windows.Forms.DataGridViewTextBoxColumn dniJug;
        private System.Windows.Forms.DataGridViewTextBoxColumn clubJug;
        private System.Windows.Forms.DataGridViewTextBoxColumn idEqui;
    }
}