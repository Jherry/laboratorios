﻿using MundialRusia2018.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MundialRusia2018
{
    public partial class FrmReporteEstadios : Form
    {
        public FrmReporteEstadios()
        {
            InitializeComponent();
        }

        MySqlConnection conexion = ConexionDB.obtenerConexion();
        private void FrmReporteEstadios_Load(object sender, EventArgs e)
        {

            this.rptEstadios.RefreshReport();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<Estadio> lsEstadios = new List<Estadio>();
            lsEstadios = Estadio.listarEstadios(conexion);

            ReportDataSource reportDataSource = new ReportDataSource("ESTADIOS",lsEstadios);


            rptEstadios.LocalReport.DataSources.Clear();
            rptEstadios.LocalReport.DataSources.Add(reportDataSource);
            rptEstadios.LocalReport.ReportEmbeddedResource="MundialRusia2018.InformeEstadio.rdlc";

            rptEstadios.LocalReport.Refresh();
            rptEstadios.Refresh();
            rptEstadios.RefreshReport();

        }
    }
}
