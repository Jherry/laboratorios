﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MundialRusia2018.Clase
{
    public class ConexionDB
    {
        private static string SERVER = ".";
        private static string DATABASE = "db_Mundial";
        //private static string USERID = "senati";
        //private static string PASSWORD = "senati";
        //private static int SSL_MODE = 0;

        
        public static SqlConnection obtenerConexion()
        {
            string cadenaConexion = string.Format($"Data Source={SERVER};Initial Catalog={DATABASE};Integrated Security=True");

            SqlConnection conexion = new SqlConnection(cadenaConexion);
            return conexion;
        }
       
    }
}
