﻿namespace _05Futbolistas
{
    partial class frmFutbolista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtClubFutb = new System.Windows.Forms.TextBox();
            this.txtNroCamisetaFutb = new System.Windows.Forms.TextBox();
            this.txtNacionalidadFutb = new System.Windows.Forms.TextBox();
            this.dtFecNacFutb = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNombresFutb = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtApematFutb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtApepatFutb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCodigoFutb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIdFutb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboPosicionFutb = new System.Windows.Forms.ComboBox();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvFutbolistas = new System.Windows.Forms.DataGridView();
            this.idFutb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoFutb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apepatFutb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apematFutb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombresFutb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecNacFutb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nacionalidadFutb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posicionFutb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nroCamisetaFutb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clubFutb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCodbus = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFutbolistas)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtClubFutb);
            this.groupBox1.Controls.Add(this.txtNroCamisetaFutb);
            this.groupBox1.Controls.Add(this.txtNacionalidadFutb);
            this.groupBox1.Controls.Add(this.dtFecNacFutb);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtNombresFutb);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtApematFutb);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtApepatFutb);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtCodigoFutb);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtIdFutb);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cboPosicionFutb);
            this.groupBox1.Location = new System.Drawing.Point(23, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 351);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // txtClubFutb
            // 
            this.txtClubFutb.Location = new System.Drawing.Point(117, 272);
            this.txtClubFutb.Name = "txtClubFutb";
            this.txtClubFutb.Size = new System.Drawing.Size(162, 20);
            this.txtClubFutb.TabIndex = 19;
            // 
            // txtNroCamisetaFutb
            // 
            this.txtNroCamisetaFutb.Location = new System.Drawing.Point(117, 246);
            this.txtNroCamisetaFutb.Name = "txtNroCamisetaFutb";
            this.txtNroCamisetaFutb.Size = new System.Drawing.Size(100, 20);
            this.txtNroCamisetaFutb.TabIndex = 18;
            this.txtNroCamisetaFutb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNroCamisetaFutb_KeyPress);
            // 
            // txtNacionalidadFutb
            // 
            this.txtNacionalidadFutb.Location = new System.Drawing.Point(117, 193);
            this.txtNacionalidadFutb.Name = "txtNacionalidadFutb";
            this.txtNacionalidadFutb.Size = new System.Drawing.Size(162, 20);
            this.txtNacionalidadFutb.TabIndex = 17;
            // 
            // dtFecNacFutb
            // 
            this.dtFecNacFutb.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFecNacFutb.Location = new System.Drawing.Point(117, 168);
            this.dtFecNacFutb.Name = "dtFecNacFutb";
            this.dtFecNacFutb.Size = new System.Drawing.Size(162, 20);
            this.dtFecNacFutb.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 274);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Club actual:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 249);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Nro camiseta:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 224);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Posicion:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 196);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Nacionalidad:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Fec. nacimiento:";
            // 
            // txtNombresFutb
            // 
            this.txtNombresFutb.Location = new System.Drawing.Point(117, 139);
            this.txtNombresFutb.Name = "txtNombresFutb";
            this.txtNombresFutb.Size = new System.Drawing.Size(162, 20);
            this.txtNombresFutb.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Nombres:";
            // 
            // txtApematFutb
            // 
            this.txtApematFutb.Location = new System.Drawing.Point(117, 113);
            this.txtApematFutb.Name = "txtApematFutb";
            this.txtApematFutb.Size = new System.Drawing.Size(162, 20);
            this.txtApematFutb.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Apellido Materno:";
            // 
            // txtApepatFutb
            // 
            this.txtApepatFutb.Location = new System.Drawing.Point(117, 87);
            this.txtApepatFutb.Name = "txtApepatFutb";
            this.txtApepatFutb.Size = new System.Drawing.Size(162, 20);
            this.txtApepatFutb.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Apellido Paterno:";
            // 
            // txtCodigoFutb
            // 
            this.txtCodigoFutb.Location = new System.Drawing.Point(117, 61);
            this.txtCodigoFutb.Name = "txtCodigoFutb";
            this.txtCodigoFutb.Size = new System.Drawing.Size(100, 20);
            this.txtCodigoFutb.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Codigo Futbolista:";
            // 
            // txtIdFutb
            // 
            this.txtIdFutb.Enabled = false;
            this.txtIdFutb.Location = new System.Drawing.Point(117, 35);
            this.txtIdFutb.Name = "txtIdFutb";
            this.txtIdFutb.Size = new System.Drawing.Size(100, 20);
            this.txtIdFutb.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "ID Futbolista:";
            // 
            // cboPosicionFutb
            // 
            this.cboPosicionFutb.FormattingEnabled = true;
            this.cboPosicionFutb.Items.AddRange(new object[] {
            "-SeleccioneOpcion-",
            "Goleador",
            "Delantero",
            "Cojo",
            "Arquero"});
            this.cboPosicionFutb.Location = new System.Drawing.Point(117, 219);
            this.cboPosicionFutb.Name = "cboPosicionFutb";
            this.cboPosicionFutb.Size = new System.Drawing.Size(162, 21);
            this.cboPosicionFutb.TabIndex = 0;
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(615, 421);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrar.TabIndex = 1;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(696, 421);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 23);
            this.btnEliminar.TabIndex = 2;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(777, 421);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 3;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(253, 19);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 4;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(362, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(322, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "Mantenimiento de Futbolistas";
            // 
            // dgvFutbolistas
            // 
            this.dgvFutbolistas.AllowUserToAddRows = false;
            this.dgvFutbolistas.AllowUserToDeleteRows = false;
            this.dgvFutbolistas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFutbolistas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idFutb,
            this.codigoFutb,
            this.apepatFutb,
            this.apematFutb,
            this.nombresFutb,
            this.fecNacFutb,
            this.nacionalidadFutb,
            this.posicionFutb,
            this.nroCamisetaFutb,
            this.clubFutb});
            this.dgvFutbolistas.Location = new System.Drawing.Point(334, 111);
            this.dgvFutbolistas.Name = "dgvFutbolistas";
            this.dgvFutbolistas.ReadOnly = true;
            this.dgvFutbolistas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFutbolistas.Size = new System.Drawing.Size(915, 304);
            this.dgvFutbolistas.TabIndex = 6;
            this.dgvFutbolistas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFutbolistas_CellContentClick);
            // 
            // idFutb
            // 
            this.idFutb.HeaderText = "ID";
            this.idFutb.Name = "idFutb";
            this.idFutb.ReadOnly = true;
            this.idFutb.Width = 30;
            // 
            // codigoFutb
            // 
            this.codigoFutb.HeaderText = "CODIGO";
            this.codigoFutb.Name = "codigoFutb";
            this.codigoFutb.ReadOnly = true;
            // 
            // apepatFutb
            // 
            this.apepatFutb.HeaderText = "APELLIDO PATERNO";
            this.apepatFutb.Name = "apepatFutb";
            this.apepatFutb.ReadOnly = true;
            // 
            // apematFutb
            // 
            this.apematFutb.HeaderText = "APELLIDO MATERNO";
            this.apematFutb.Name = "apematFutb";
            this.apematFutb.ReadOnly = true;
            // 
            // nombresFutb
            // 
            this.nombresFutb.HeaderText = "NOMBRES";
            this.nombresFutb.Name = "nombresFutb";
            this.nombresFutb.ReadOnly = true;
            // 
            // fecNacFutb
            // 
            this.fecNacFutb.HeaderText = "FECHA NACIMIENTO";
            this.fecNacFutb.Name = "fecNacFutb";
            this.fecNacFutb.ReadOnly = true;
            // 
            // nacionalidadFutb
            // 
            this.nacionalidadFutb.HeaderText = "NACIONALIDAD";
            this.nacionalidadFutb.Name = "nacionalidadFutb";
            this.nacionalidadFutb.ReadOnly = true;
            // 
            // posicionFutb
            // 
            this.posicionFutb.HeaderText = "POSICION";
            this.posicionFutb.Name = "posicionFutb";
            this.posicionFutb.ReadOnly = true;
            // 
            // nroCamisetaFutb
            // 
            this.nroCamisetaFutb.HeaderText = "NRO CAMISETA";
            this.nroCamisetaFutb.Name = "nroCamisetaFutb";
            this.nroCamisetaFutb.ReadOnly = true;
            this.nroCamisetaFutb.Width = 30;
            // 
            // clubFutb
            // 
            this.clubFutb.HeaderText = "CLUB ACTUAL";
            this.clubFutb.Name = "clubFutb";
            this.clubFutb.ReadOnly = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCodbus);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.btnBuscar);
            this.groupBox2.Location = new System.Drawing.Point(866, 43);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(334, 61);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parametro de busqueda";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Codigo Futbolista";
            // 
            // txtCodbus
            // 
            this.txtCodbus.Location = new System.Drawing.Point(109, 21);
            this.txtCodbus.Name = "txtCodbus";
            this.txtCodbus.Size = new System.Drawing.Size(118, 20);
            this.txtCodbus.TabIndex = 6;
            // 
            // frmFutbolista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1261, 481);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgvFutbolistas);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmFutbolista";
            this.Text = "Mantenimiento de Futbolistas";
            this.Load += new System.EventHandler(this.frmFutbolista_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFutbolistas)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNombresFutb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtApematFutb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtApepatFutb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCodigoFutb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIdFutb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboPosicionFutb;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtFecNacFutb;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtClubFutb;
        private System.Windows.Forms.TextBox txtNroCamisetaFutb;
        private System.Windows.Forms.TextBox txtNacionalidadFutb;
        private System.Windows.Forms.DataGridView dgvFutbolistas;
        private System.Windows.Forms.DataGridViewTextBoxColumn idFutb;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoFutb;
        private System.Windows.Forms.DataGridViewTextBoxColumn apepatFutb;
        private System.Windows.Forms.DataGridViewTextBoxColumn apematFutb;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombresFutb;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecNacFutb;
        private System.Windows.Forms.DataGridViewTextBoxColumn nacionalidadFutb;
        private System.Windows.Forms.DataGridViewTextBoxColumn posicionFutb;
        private System.Windows.Forms.DataGridViewTextBoxColumn nroCamisetaFutb;
        private System.Windows.Forms.DataGridViewTextBoxColumn clubFutb;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtCodbus;
        private System.Windows.Forms.Label label12;
    }
}

