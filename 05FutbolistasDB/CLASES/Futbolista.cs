﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05Futbolistas.Clases
{
    public class Futbolista
    {
        private int idFutb;
        private string codigoFutb;
        private string apepatFutb;
        private string apematFutb;
        private string nombresFutb;
        private string fecNacFutb;
        private string nacionalidadFutb;
        private string posicionFutb;
        private int nroCamisetaFutb;
        private string clubFutb;

        public int IdFutb { get => idFutb; set => idFutb = value; }
        public string CodigoFutb { get => codigoFutb; set => codigoFutb = value; }
        public string ApepatFutb { get => apepatFutb; set => apepatFutb = value; }
        public string ApematFutb { get => apematFutb; set => apematFutb = value; }
        public string NombresFutb { get => nombresFutb; set => nombresFutb = value; }
        public string FecNacFutb { get => fecNacFutb; set => fecNacFutb = value; }
        public string NacionalidadFutb { get => nacionalidadFutb; set => nacionalidadFutb = value; }
        public string PosicionFutb { get => posicionFutb; set => posicionFutb = value; }
        public int NroCamisetaFutb { get => nroCamisetaFutb; set => nroCamisetaFutb = value; }
        public string ClubFutb { get => clubFutb; set => clubFutb = value; }

        public Futbolista(int idFutb, string codigoFutb, string apepatFutb, string apematFutb, string nombresFutb, string fecNacFutb, string nacionalidadFutb, string posicionFutb, int nroCamisetaFutb, string clubFutb)
        {
            this.IdFutb = idFutb;
            this.CodigoFutb = codigoFutb;
            this.ApepatFutb = apepatFutb;
            this.ApematFutb = apematFutb;
            this.NombresFutb = nombresFutb;
            this.FecNacFutb = fecNacFutb;
            this.NacionalidadFutb = nacionalidadFutb;
            this.PosicionFutb = posicionFutb;
            this.NroCamisetaFutb = nroCamisetaFutb;
            this.ClubFutb = clubFutb;
        }
        public Futbolista()
        {

        }
        public static int insertFutbolista(Futbolista futbolista, MySqlConnection conexion)
        {
        
                string query = string.Format("INSERT INTO JHERRY_FUTBOLISTAS VALUES (" +
                    $"'{futbolista.IdFutb}'," +
                    $"'{futbolista.CodigoFutb}'," +
                    $"'{futbolista.ApepatFutb}'," +
                    $"'{futbolista.ApematFutb}'," +
                    $"'{futbolista.NombresFutb}'," +
                    $"'{futbolista.FecNacFutb}'," +
                    $"'{futbolista.NacionalidadFutb}'," +
                    $"'{futbolista.PosicionFutb}'," +
                    $"'{futbolista.NroCamisetaFutb}'," +
                    $"'{futbolista.ClubFutb}'" +
                    $");");
                conexion.Open();

                MySqlCommand command = new MySqlCommand(query, conexion); //Le enviamos la cadena y la conexion debe de esta abierta
                int nroFilasAfectadas = command.ExecuteNonQuery(); //Para obtener el nro de filas afectadas
                conexion.Close();
                return nroFilasAfectadas;
            }
            public static int updateFutbolista(MySqlConnection conexion,Futbolista futbolista)
            {
            string query = string.Format($" UPDATE JHERRY_FUTBOLISTAS SET" +
                $" CODIGOFUTB='{ futbolista.CodigoFutb}'," +
                $" APEPATFUTB='{ futbolista.ApepatFutb}'," +
                $" APEMATFUTB='{ futbolista.ApematFutb}'," +
                $" NOMBRESFUTB='{ futbolista.NombresFutb}'," +
                $" FECNACFUTB='{ futbolista.FecNacFutb}'," +
                $" NACIONALIDADFUTB='{ futbolista.NacionalidadFutb}'," +
                $" POSICIONFUTB='{ futbolista.PosicionFutb}'," +
                $" NROCAMISETAFUTB='{ futbolista.NroCamisetaFutb}'," +
                $" CLUBFUTB='{ futbolista.ClubFutb}'"+
                $" WHERE IDFUTB={futbolista.IdFutb};"
                );
            conexion.Open();
            MySqlCommand command = new MySqlCommand(query, conexion);
            int numFilasAfectadas = command.ExecuteNonQuery();
            conexion.Close();
            return numFilasAfectadas;
            }
            public static int deleteFutbolista(MySqlConnection conexion,int idFutb)
            {
            string query = string.Format($"DELETE FROM JHERRY_FUTBOLISTAS WHERE IDFUTB={idFutb};");
            conexion.Open();
            MySqlCommand command = new MySqlCommand(query, conexion); 
            int numFilasAfectadas = command.ExecuteNonQuery();
            conexion.Close();

                return numFilasAfectadas;
            }
        public static Futbolista getFutbolistaID(MySqlConnection conexion, int idFutb)
        {
            string query = string.Format($"SELECT * FROM JHERRY_FUTBOLISTAS WHERE IDFUTB={idFutb}");
            conexion.Open();
            MySqlCommand command = new MySqlCommand(query,conexion);//La conexion debe estar abierta
            MySqlDataReader reader = command.ExecuteReader();

            Futbolista futbolista = new Futbolista(); 

            if (reader.HasRows) //Si tiene filas cargadas
            {
                while (reader.Read())
                {
                    
                    futbolista.idFutb = reader.GetInt32(0);
                    futbolista.codigoFutb = reader.GetString(1);
                    futbolista.apepatFutb = reader.GetString(2);
                    futbolista.apematFutb = reader.GetString(3);
                    futbolista.nombresFutb = reader.GetString(4);
                    futbolista.fecNacFutb = reader.GetString(5);
                    futbolista.nacionalidadFutb = reader.GetString(6);
                    futbolista.posicionFutb = reader.GetString(7);
                    futbolista.nroCamisetaFutb = reader.GetInt32(8);
                    futbolista.clubFutb = reader.GetString(9);                 

                }
                
            }
            conexion.Close();
            return futbolista;
        }
            public static List<Futbolista> listarFutbolistas(MySqlConnection conexion)
            {
                conexion.Open();
                List<Futbolista> lsFutbolistas = new List<Futbolista>();


                string query = string.Format("SELECT * FROM JHERRY_FUTBOLISTAS ORDER BY IDFUTB ASC;");
                MySqlCommand command = new MySqlCommand(query, conexion);

                MySqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows) //Si tiene filas cargadas
                {
                    while (reader.Read())
                    {
                        Futbolista futbolista = new Futbolista();
                        futbolista.idFutb = reader.GetInt32(0);
                        futbolista.codigoFutb = reader.GetString(1);
                        futbolista.apepatFutb = reader.GetString(2);
                        futbolista.apematFutb = reader.GetString(3);
                        futbolista.nombresFutb = reader.GetString(4);
                        futbolista.fecNacFutb = reader.GetString(5);
                        futbolista.nacionalidadFutb = reader.GetString(6);
                        futbolista.posicionFutb = reader.GetString(7);
                        futbolista.nroCamisetaFutb = reader.GetInt32(8);
                        futbolista.clubFutb = reader.GetString(9);

                        lsFutbolistas.Add(futbolista);
                    }
                }
                conexion.Close();


                /*Map < String.Object >= map;
                map.add("Key", "Valor");
                map.add("Key", "Valor");
                map.add("Key", "Valor");*/
                return lsFutbolistas;
            }

        }
    }



