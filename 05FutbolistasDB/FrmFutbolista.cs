﻿using _05Futbolistas.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _05Futbolistas
{
    public partial class frmFutbolista : Form
    {
        public frmFutbolista()
        {
            InitializeComponent();
        }
        MySqlConnection conexion = ConexionDB.obtenerConexion();
        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                //int _idFutb = int.Parse(txtIdFutb.Text);
                string _codigoFutb = txtCodigoFutb.Text;
                string _apepatFutb = txtApepatFutb.Text;
                string _apematFutb = txtApematFutb.Text;
                string _nombresFutb = txtNombresFutb.Text;
                string _fecNacFutb = dtFecNacFutb.Value.ToString("yyyy/MM/dd");
                string _nacionalidadFutb = txtNacionalidadFutb.Text;
                string _posicionFutb = cboPosicionFutb.SelectedIndex.ToString();
                int _nroCamisetaFutb = int.Parse(txtNroCamisetaFutb.Text);
                string _clubFutb = txtClubFutb.Text;

                //Instanciando el futbolista
                Futbolista futbolista = new Futbolista(0, _codigoFutb, _apepatFutb, _apematFutb, _nombresFutb, _fecNacFutb, _nacionalidadFutb, _posicionFutb, _nroCamisetaFutb,
                   _clubFutb);

                //Insertar futbolista en la DB
                int numFilasAfectadas = Futbolista.insertFutbolista(futbolista, conexion);

                if (numFilasAfectadas > 0)
                {
                    MessageBox.Show($"Futbolista creado correctamente.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgvFutbolistas.Rows.Clear();
                    listarFutbolistas();
                    limpiarControles();
                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show($"ERROR{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void txtNroCamisetaFutb_KeyPress(object sender, KeyPressEventArgs e)
        {



        }
        private void listarFutbolistas()
        {
            List<Futbolista> lsFutbolistas = Futbolista.listarFutbolistas(conexion);

            foreach (Futbolista f in lsFutbolistas)
            {
                dgvFutbolistas.Rows.Add(f.IdFutb, f.CodigoFutb, f.ApepatFutb, f.ApematFutb, f.NombresFutb, f.FecNacFutb, f.NacionalidadFutb, f.PosicionFutb, f.NroCamisetaFutb, f.ClubFutb);
            }
        }
        private void frmFutbolista_Load(object sender, EventArgs e)
        {
            cboPosicionFutb.SelectedIndex = 0;
            listarFutbolistas();

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            int idFutbToDelete = Convert.ToInt32(dgvFutbolistas.CurrentRow.Cells[0].Value);
            int numFilasAfectadas = Futbolista.deleteFutbolista(conexion, idFutbToDelete);

            if (numFilasAfectadas > 0)
            {
                MessageBox.Show($"El Futbolista de ID:{idFutbToDelete} se ha eliminado correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show($"Error al eliminar un Futbolista,intente nuevamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            dgvFutbolistas.Rows.Clear();
            listarFutbolistas();
            limpiarControles();
        }
        private void limpiarControles()
        {
            txtCodigoFutb.Clear();
            txtApepatFutb.Clear();
            txtApematFutb.Clear();
            txtNombresFutb.Clear();
            dtFecNacFutb.Value = DateTime.Today;
            txtNacionalidadFutb.Clear();
            cboPosicionFutb.SelectedIndex = 0;
            txtNroCamisetaFutb.Clear();
            txtClubFutb.Clear();
        }

        private void dgvFutbolistas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int idFutbSeleccionado = Convert.ToInt32(dgvFutbolistas.CurrentRow.Cells[0].Value);
            Futbolista futbolista = Futbolista.getFutbolistaID(conexion, idFutbSeleccionado);

            MessageBox.Show($"Haz seleccionado un futbolista correctamente");

            txtIdFutb.Text = futbolista.IdFutb.ToString();
            txtCodigoFutb.Text = futbolista.CodigoFutb;
            txtApepatFutb.Text = futbolista.ApepatFutb;
            txtApematFutb.Text = futbolista.ApematFutb;
            txtNombresFutb.Text = futbolista.NombresFutb;
            dtFecNacFutb.Text = futbolista.FecNacFutb;
            txtNacionalidadFutb.Text = futbolista.NacionalidadFutb;
            cboPosicionFutb.Text = futbolista.PosicionFutb;
            txtNroCamisetaFutb.Text = futbolista.NroCamisetaFutb.ToString();
            txtClubFutb.Text = futbolista.ClubFutb;
            
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            int _idFutb = int.Parse(txtIdFutb.Text);
            string _codigoFutb = txtCodigoFutb.Text;
            string _apepatFutb = txtApepatFutb.Text;
            string _apematFutb = txtApematFutb.Text;
            string _nombresFutb = txtNombresFutb.Text;
            string _fecNacFutb = dtFecNacFutb.Value.ToString("yyyy/MM/dd");
            string _nacionalidadFutb = txtNacionalidadFutb.Text;
            string _posicionFutb = cboPosicionFutb.SelectedIndex.ToString();
            int _nroCamisetaFutb = int.Parse(txtNroCamisetaFutb.Text);
            string _clubFutb = txtClubFutb.Text;

            Futbolista futbolista = new Futbolista(_idFutb, _codigoFutb, _apepatFutb, _apematFutb, _nombresFutb, _fecNacFutb, _nacionalidadFutb, _posicionFutb, _nroCamisetaFutb,
                   _clubFutb);

            int numFilasAfectadas = Futbolista.updateFutbolista(conexion,futbolista);
            if(numFilasAfectadas>0)
            {
                MessageBox.Show($"El futbolista de ID:{_idFutb} se actualizo correctamente","Mensaje",
                    MessageBoxButtons.OK,MessageBoxIcon.Information);
                dgvFutbolistas.Rows.Clear();
                dgvFutbolistas.Rows.Add(futbolista.IdFutb, futbolista.CodigoFutb, futbolista.ApepatFutb, futbolista.ApematFutb, futbolista.NombresFutb, futbolista.FecNacFutb, futbolista.NacionalidadFutb, futbolista.PosicionFutb, futbolista.NroCamisetaFutb, futbolista.ClubFutb)
;
                limpiarControles();
            }
            else
            {
                MessageBox.Show($"El futbolista de ID:{_idFutb} NO se actualizo correctamente", "Mensaje",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                int _idFutb = int.Parse(txtCodbus.Text);
                Futbolista futbolista = Futbolista.getFutbolistaID(conexion, _idFutb);

                if(futbolista.CodigoFutb==null)
                {
                    MessageBox.Show($"No se encontro al Futbolista buscado", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    dgvFutbolistas.Rows.Clear();
                    dgvFutbolistas.Rows.Add(futbolista.IdFutb, futbolista.CodigoFutb, futbolista.ApepatFutb, futbolista.ApematFutb, futbolista.NombresFutb, futbolista.FecNacFutb, futbolista.NacionalidadFutb, futbolista.PosicionFutb, futbolista.NroCamisetaFutb, futbolista.ClubFutb);

                }
            }
            catch (Exception)
            {

                MessageBox.Show($"Debe ingresar solo numeros...", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            


            

    }
    }
}

