/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     9/10/2018 21:05:34                           */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ENCUENTRO') and o.name = 'FK_ENCUENTR_ENCUENTRO_ESTADIO')
alter table ENCUENTRO
   drop constraint FK_ENCUENTR_ENCUENTRO_ESTADIO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ENCUENTRO') and o.name = 'FK_ENCUENTR_EQUIPO_EN_EQUIPO')
alter table ENCUENTRO
   drop constraint FK_ENCUENTR_EQUIPO_EN_EQUIPO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('EQUIPO') and o.name = 'FK_EQUIPO_EQUIPO_GR_GRUPO')
alter table EQUIPO
   drop constraint FK_EQUIPO_EQUIPO_GR_GRUPO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('JUGADOR') and o.name = 'FK_JUGADOR_JUGADOR_E_EQUIPO')
alter table JUGADOR
   drop constraint FK_JUGADOR_JUGADOR_E_EQUIPO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('JUGADOR_POSICION') and o.name = 'FK_JUGADOR__JUGADOR_P_POSICION')
alter table JUGADOR_POSICION
   drop constraint FK_JUGADOR__JUGADOR_P_POSICION
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('JUGADOR_POSICION') and o.name = 'FK_JUGADOR__JUGADOR_P_JUGADOR')
alter table JUGADOR_POSICION
   drop constraint FK_JUGADOR__JUGADOR_P_JUGADOR
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ENCUENTRO')
            and   name  = 'EQUIPO_ENCUENTRO_FK'
            and   indid > 0
            and   indid < 255)
   drop index ENCUENTRO.EQUIPO_ENCUENTRO_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ENCUENTRO')
            and   name  = 'ENCUENTRO_ESTADIO_FK'
            and   indid > 0
            and   indid < 255)
   drop index ENCUENTRO.ENCUENTRO_ESTADIO_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ENCUENTRO')
            and   type = 'U')
   drop table ENCUENTRO
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('EQUIPO')
            and   name  = 'EQUIPO_GRUPO_FK'
            and   indid > 0
            and   indid < 255)
   drop index EQUIPO.EQUIPO_GRUPO_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EQUIPO')
            and   type = 'U')
   drop table EQUIPO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ESTADIO')
            and   type = 'U')
   drop table ESTADIO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('GRUPO')
            and   type = 'U')
   drop table GRUPO
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('JUGADOR')
            and   name  = 'JUGADOR_EQUIPO_FK'
            and   indid > 0
            and   indid < 255)
   drop index JUGADOR.JUGADOR_EQUIPO_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('JUGADOR')
            and   type = 'U')
   drop table JUGADOR
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('JUGADOR_POSICION')
            and   name  = 'JUGADOR_POSICION_FK'
            and   indid > 0
            and   indid < 255)
   drop index JUGADOR_POSICION.JUGADOR_POSICION_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('JUGADOR_POSICION')
            and   name  = 'JUGADOR_POSICION2_FK'
            and   indid > 0
            and   indid < 255)
   drop index JUGADOR_POSICION.JUGADOR_POSICION2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('JUGADOR_POSICION')
            and   type = 'U')
   drop table JUGADOR_POSICION
go

if exists (select 1
            from  sysobjects
           where  id = object_id('POSICION')
            and   type = 'U')
   drop table POSICION
go

/*==============================================================*/
/* Table: ENCUENTRO                                             */
/*==============================================================*/
create table ENCUENTRO (
   IDENC                int                  identity,
   IDEST                int                  null,
   IDEQUI               int                  not null,
   IDEQUIPOAENC         int                  null,
   IDEQUIPOBENC         int                  null,
   FECENC               datetime             null,
   ARBITROENC           varchar(150)         null,
   GANADORENC           varchar(150)         null,
   MARCADORENC          varchar(50)          null,
   IDESTENC             int                  null,
   constraint PK_ENCUENTRO primary key nonclustered (IDENC)
)
go

/*==============================================================*/
/* Index: ENCUENTRO_ESTADIO_FK                                  */
/*==============================================================*/
create index ENCUENTRO_ESTADIO_FK on ENCUENTRO (
IDEST ASC
)
go

/*==============================================================*/
/* Index: EQUIPO_ENCUENTRO_FK                                   */
/*==============================================================*/
create index EQUIPO_ENCUENTRO_FK on ENCUENTRO (
IDEQUI ASC
)
go

/*==============================================================*/
/* Table: EQUIPO                                                */
/*==============================================================*/
create table EQUIPO (
   IDEQUI               int                  identity,
   IDGRUP               int                  null,
   NOMBREEQUI           varchar(100)         null,
   FECCREACIONEQUI      datetime             null,
   ENTRENADOREQUI       varchar(150)         null,
   CANTMIEMBROSEQUI     int                  null,
   PAISDESTINOEQUI      varchar(50)          null,
   constraint PK_EQUIPO primary key nonclustered (IDEQUI)
)
go

/*==============================================================*/
/* Index: EQUIPO_GRUPO_FK                                       */
/*==============================================================*/
create index EQUIPO_GRUPO_FK on EQUIPO (
IDGRUP ASC
)
go

/*==============================================================*/
/* Table: ESTADIO                                               */
/*==============================================================*/
create table ESTADIO (
   IDEST                int                  identity,
   NOMBREEST            varchar(150)         null,
   LOCALIDADEST         varchar(150)         null,
   AFOROEST             int                  null,
   constraint PK_ESTADIO primary key nonclustered (IDEST)
)
go

/*==============================================================*/
/* Table: GRUPO                                                 */
/*==============================================================*/
create table GRUPO (
   IDGRUP               int                  identity,
   CANTEQUIPOSGRUP      int                  null,
   DESCGRUP             char(1)              null,
   constraint PK_GRUPO primary key nonclustered (IDGRUP)
)
go

/*==============================================================*/
/* Table: JUGADOR                                               */
/*==============================================================*/
create table JUGADOR (
   IDJUG                int                  identity,
   IDEQUI               int                  null,
   IDPOSJUG             int                  null,
   IDEQUIJUG            int                  null,
   NOMBRESJUG           varchar(50)          null,
   APEPATJUG            varchar(50)          null,
   APEMATJUG            varchar(50)          null,
   FECNACJUG            datetime             null,
   SEXOJUG              char(1)              null,
   NUMCAMISETAJUG       int                  null,
   DNIJUG               char(8)              null,
   CLUBJUG              varchar(100)         null,
   constraint PK_JUGADOR primary key nonclustered (IDJUG)
)
go

/*==============================================================*/
/* Index: JUGADOR_EQUIPO_FK                                     */
/*==============================================================*/
create index JUGADOR_EQUIPO_FK on JUGADOR (
IDEQUI ASC
)
go

/*==============================================================*/
/* Table: JUGADOR_POSICION                                      */
/*==============================================================*/
create table JUGADOR_POSICION (
   IDPOS                int                  not null,
   IDJUG                int                  not null,
   constraint PK_JUGADOR_POSICION primary key nonclustered (IDPOS, IDJUG)
)
go

/*==============================================================*/
/* Index: JUGADOR_POSICION2_FK                                  */
/*==============================================================*/
create index JUGADOR_POSICION2_FK on JUGADOR_POSICION (
IDJUG ASC
)
go

/*==============================================================*/
/* Index: JUGADOR_POSICION_FK                                   */
/*==============================================================*/
create index JUGADOR_POSICION_FK on JUGADOR_POSICION (
IDPOS ASC
)
go

/*==============================================================*/
/* Table: POSICION                                              */
/*==============================================================*/
create table POSICION (
   IDPOS                int                  identity,
   DESCPOS              varchar(150)         null,
   constraint PK_POSICION primary key nonclustered (IDPOS)
)
go

alter table ENCUENTRO
   add constraint FK_ENCUENTR_ENCUENTRO_ESTADIO foreign key (IDEST)
      references ESTADIO (IDEST)
go

alter table ENCUENTRO
   add constraint FK_ENCUENTR_EQUIPO_EN_EQUIPO foreign key (IDEQUI)
      references EQUIPO (IDEQUI)
go

alter table EQUIPO
   add constraint FK_EQUIPO_EQUIPO_GR_GRUPO foreign key (IDGRUP)
      references GRUPO (IDGRUP)
go

alter table JUGADOR
   add constraint FK_JUGADOR_JUGADOR_E_EQUIPO foreign key (IDEQUI)
      references EQUIPO (IDEQUI)
go

alter table JUGADOR_POSICION
   add constraint FK_JUGADOR__JUGADOR_P_POSICION foreign key (IDPOS)
      references POSICION (IDPOS)
go

alter table JUGADOR_POSICION
   add constraint FK_JUGADOR__JUGADOR_P_JUGADOR foreign key (IDJUG)
      references JUGADOR (IDJUG)
go

