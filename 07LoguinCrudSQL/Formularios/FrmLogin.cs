﻿using _07LoguinCrudSQL.Clases;
using _07LoguinCrudSQL.Formularios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _07LoguinCrudSQL
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        SqlConnection conexion = ConexionDB.obtenerConexion();

        private void btnConexion_Click(object sender, EventArgs e)
        {
            try
            {
                conexion.Open();
                MessageBox.Show($"Conexion exitosa. ");
                conexion.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show($"ERROR:{ex.Message}");
            }
        }

        private void lnkRegistroNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FrmRegistrarUsuario frm = new FrmRegistrarUsuario();
            frm.Show();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            {
                string _usuario = txtUsuario.Text;
                string _password = txtContraseña.Text;

                if (_usuario.Equals(string.Empty) || _password.Equals(string.Empty)) //CREDENCIALES INCORRECTAS
                {
                    MessageBox.Show("Ingrese usuario y/o password", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else //CREDENCIALES CORRECTAS
                {
                    Dictionary<String,Object> resultado = Usuario.validarCredenciales(conexion, _usuario, _password);
                    if (bool.Parse(resultado["indOperacion"].ToString())) //INGRESO
                    {
                        MessageBox.Show(resultado["mensaje"].ToString(), "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //MOSTRAR EL FORMULARIO DE BIENVENIDA

                        //USUARIO=_usuario


                        FrmBienvenida frmBienvenida = new FrmBienvenida(); //instanciar
                        FrmBienvenida.mensaje = $"Sr.(a):{_usuario},usted a ingresado a nuestro sistema.";
                        frmBienvenida.Show();

                    }
                    else
                    {
                        MessageBox.Show(resultado["mensaje"].ToString(), "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
