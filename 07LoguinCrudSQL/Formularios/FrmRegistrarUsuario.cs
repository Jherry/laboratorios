﻿using _07LoguinCrudSQL.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _07LoguinCrudSQL.Formularios
{
    public partial class FrmRegistrarUsuario : Form
    {
        public FrmRegistrarUsuario()
        {
            InitializeComponent();
        }
        SqlConnection conexion = ConexionDB.obtenerConexion();
        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            // string _ID = txtID.Text;
            string _usuario = txtUsuario.Text;
            string _clave = txtClave.Text;
            string _rol = cboRol.Text;
            string _email = txtEmail.Text;
            char _estado = (rbtA.Checked ? 'A' : 'I');

            Usuario usuario = new Usuario(0, _usuario, _clave, _rol, _email, _estado);

            int numFilasAfectadas = Usuario.InsertarUsuario(conexion, usuario);
            if (numFilasAfectadas > 0)
            {
                MessageBox.Show("Insetado con exito");
            }
            else
            {
                MessageBox.Show("Error al insertar");
            }
        }

        private void FrmRegistrarUsuario_Load(object sender, EventArgs e)
        {
            List<Usuario> lsUsuarios = Usuario.listarUsuarios(conexion);

            foreach(Usuario u in lsUsuarios)
            dgvUsuarios.Rows.Add(u.Iduser,u.UsuarioUser,u.ClaveUser,u.RolUser,u.EmailUser,u.EstadoUser);
        }

        private void txtClave_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
