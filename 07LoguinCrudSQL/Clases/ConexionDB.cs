﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07LoguinCrudSQL.Clases
{
    public class ConexionDB
    {

        public static SqlConnection obtenerConexion()
        {
            SqlConnection conexion = new SqlConnection(
                ConfigurationManager.ConnectionStrings["mi_conexion"].ConnectionString);
            return conexion;
        }
    }
}
