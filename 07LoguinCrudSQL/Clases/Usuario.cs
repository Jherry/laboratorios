﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07LoguinCrudSQL.Clases
{
    class Usuario
    {
        private int iduser;
        private string usuarioUser;
        private string claveUser;
        private string rolUser;
        private string emailUser;
        private char estadoUser;

        public int Iduser { get => iduser; set => iduser = value; }
        public string UsuarioUser { get => usuarioUser; set => usuarioUser = value; }
        public string ClaveUser { get => claveUser; set => claveUser = value; }
        public string RolUser { get => rolUser; set => rolUser = value; }
        public string EmailUser { get => emailUser; set => emailUser = value; }
        public char EstadoUser { get => estadoUser; set => estadoUser = value; }

        public Usuario()
        {
        }

        public Usuario(int iduser, string usuarioUser, string claveUser, string rolUser, string emailUser, char estadoUser)
        {
            this.Iduser = iduser;
            this.UsuarioUser = usuarioUser;
            this.ClaveUser = claveUser;
            this.RolUser = rolUser;
            this.EmailUser = emailUser;
            this.EstadoUser = estadoUser;
        }

        public static Dictionary<String,Object> validarCredenciales(SqlConnection conexion, string usuario, string password)
        {
            bool rpta = false;
            Dictionary<String, Object> respuesta = new Dictionary<String, Object>();

            string queryUsuario = string.Format($"SELECT * FROM USUARIO WHERE USUARIOUSER = '{usuario}'");
            string queryClave = string.Format($"SELECT * FROM USUARIO WHERE CLAVEUSER = '{password}'");



            conexion.Open();
            SqlCommand commandUsuario = new SqlCommand(queryUsuario, conexion);
            SqlDataReader readerUsuario = commandUsuario.ExecuteReader();
            bool esUsuarioOK = readerUsuario.HasRows; //Cuando tiene filas
            conexion.Close();

            conexion.Open();
            SqlCommand commandClave = new SqlCommand(queryClave, conexion);
            SqlDataReader readerClave = commandClave.ExecuteReader();
            bool esClaveOK = readerClave.HasRows; //Cuando tiene filas
            conexion.Close();


            if (esUsuarioOK)
            {
                if (esClaveOK)
                {
                    respuesta.Add("indOperacion", true);
                    respuesta.Add("mensaje", "Bienvenido a nuestro sistema.");

                }
                else
                {
                    respuesta.Add("indOperacion", false);
                    respuesta.Add("mensaje","Su contraseña no corresponde");
                }
                
            }
            else
            {
                respuesta.Add("indOperacion", false);
                respuesta.Add("mensaje", "Su usuario no corresponde.");
            }

            
            return respuesta;

        }

        public static int InsertarUsuario(SqlConnection conexion, Usuario usuario)
        {
            string query = string.Format($"INSERT INTO USUARIO VALUES (" +
                //$"'0', " +
                $"'{usuario.UsuarioUser}', " +
                $"'{usuario.ClaveUser}', " +
                $"'{usuario.RolUser}', " +
                $"'{usuario.EmailUser}', " +
                $"'{usuario.EstadoUser}' " +
                $");");
            conexion.Open();

            SqlCommand command = new SqlCommand(query, conexion);

            int numFilasAfectadas = command.ExecuteNonQuery();
            conexion.Close();

            return numFilasAfectadas;
        }
        public static List<Usuario>listarUsuarios(SqlConnection conexion)
        {
            List<Usuario> lsUsuario = new List<Usuario>();
            conexion.Open();
            string query = string.Format("SELECT * FROM USUARIO");
            SqlCommand command = new SqlCommand(query, conexion);

            SqlDataReader reader =command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Usuario usuario = new Usuario();
                    usuario.Iduser=reader.GetInt32(0);
                    usuario.UsuarioUser=reader.GetString(1);
                    usuario.ClaveUser=reader.GetString(2);
                    usuario.RolUser=reader.GetString(3);
                    usuario.EmailUser=reader.GetString(4);
                        usuario.EstadoUser=Char.Parse(reader.GetString(5));

                    lsUsuario.Add(usuario);
                }
            }
            conexion.Close();


           
            return lsUsuario;
        }

        }
    }


