﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EscribirArchivos
{
    class Programpu
    {
        static string RUTA_DE_ARCHIVO = "C:/dasdadsd/EscribirArchivos/archivos/";
        public static void crearArchivo()
        {
            Console.WriteLine("Ingrese nombre de archivo:");
            string nombreArchivo = Console.ReadLine();
        //1.Crear y abrir el archivo
        TextWriter miArchivo = new StreamWriter(RUTA_DE_ARCHIVO + nombreArchivo + ".txt");

        //2.Leer el contenido del archivo ingresado por consola
        Console.WriteLine("Ingrese el contenido del archivo: ");
            string mensaje = Console.ReadLine();
        //3.Escribir en el archivo el contenido
        miArchivo.WriteLine(mensaje);

            Console.WriteLine("El archivo fue creado correctamente");
            //4.Cerrar el archivo para desbloquear
            miArchivo.Close();


            Console.ReadKey();
            }
        public static void leerArchivo()
        {
            Console.WriteLine("Ingrese nombre de archivo:");
            string nombreArchivo = Console.ReadLine();
            TextReader miArchivo = new StreamReader(RUTA_DE_ARCHIVO + nombreArchivo + ".txt");
            //string contenido = miArchivo.ReadLine();
            string contenido = miArchivo.ReadToEnd();
            Console.WriteLine($"CONTENIDO: {contenido}");
            Console.ReadKey();
        }
        static void Main(string[] args)
        {
            Console.WriteLine("OPERACIONES CON ARCHIVOS");
            Console.WriteLine("============================");
            Console.WriteLine("1.Crear  un archivo en un directorio");
            Console.WriteLine("2.Leer  un archivo en un directorio");
            Console.WriteLine(">>Seleccion una opcion(1-2)");
            int opcion = 0;
            try
            {
                opcion = int.Parse(Console.ReadLine());
                switch(opcion)
                {
                    case 1:
                        crearArchivo();
                        break;
                    case 2:
                        leerArchivo();
                        break;
                    default:
                        Console.WriteLine("Opcion incorrecta vuelva a intentar...");
                        break;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"ERROR:{ex.Message}");
            }

            Console.ReadKey();

        }
    }
}
