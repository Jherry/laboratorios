﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pokemons
{
    public partial class frmPokemons : Form
    {
        static int numFilas = 0;
        static string RUTA_IMAGENES= "C:/dasdadsd/img/";
        static List<Pokemons> lsPokesRivales = new List<Pokemons>();

        public Pokemons obtenerPokemonRival()
        {
            Random rnd = new Random();
            int indice = rnd.Next(0, 4);
            return lsPokesRivales[indice];
            
        }
        public void crearPokemonRival()
        {
            Pokemons miPokemon1 = new Pokemons("Onix1", "Agua1", "Nadar1", 10.5, 2.5, "Azul1", "Pelear1");
            Pokemons miPokemon2 = new Pokemons("Onix2", "Agua2", "Nadar2", 10.5, 2.5, "Azul2", "Pelear2");
            Pokemons miPokemon3 = new Pokemons("Onix3", "Agua3", "Nadar3", 10.5, 2.5, "Azul3", "Pelear3");
            Pokemons miPokemon4 = new Pokemons("Onix4", "Agua4", "Nadar4", 10.5, 2.5, "Azul4", "Pelear4");
            Pokemons miPokemon5 = new Pokemons("Onix5", "Agua5", "Nadar5", 10.5, 2.5, "Azul5", "Pelear5");
            lsPokesRivales.Add(miPokemon1);
            lsPokesRivales.Add(miPokemon2);
            lsPokesRivales.Add(miPokemon3);
            lsPokesRivales.Add(miPokemon4);
            lsPokesRivales.Add(miPokemon5);
        }
        public void mostrarDatosPokemonRival()
        {
            Pokemons pokeRival = obtenerPokemonRival();
            txtNomSistema.Text = pokeRival.Nombre.ToString();
            txtAlturaSistema.Text = pokeRival.Altura.ToString();
            txtHabilidadSistema.Text = pokeRival.Habilidad.ToString();
            txtPesoSistema.Text = pokeRival.Peso.ToString();
            txtEspecieSistema.Text = pokeRival.Especie.ToString();
            txtHabitatSistema.Text = pokeRival.Habitat.ToString();
        }
        public frmPokemons()
        {
            InitializeComponent();
        }

        public void limpiarControles()
        {
            
            txtEspecie.Clear();
            txtAltura.Clear();
            txtHabilidad.Clear();
            txtHabitat.Clear();
            txtPeso.Clear();

            
        }

        private void frmPokemons_Load(object sender, EventArgs e)
        {
            //ash:(pikachu y bulbasur)
            //misty:(charizard,onix)

            //Inicializando Nombres de entrenadores
            cboEntrenador.Items.Add("--Seleccione un Entrenador--");
            cboEntrenador.Items.Add("Ash");
            cboEntrenador.Items.Add("Brook");
            cboEntrenador.Items.Add("Misty");


            //imgFotoPokemon.ImageLocation= "C:/img/bulbasur.jpg";

            cboPokemon.Items.Add("--Seleccione un Pokemon--");//index=0;
            /*cboPokemon.Items.Add("Pikachu");
            cboPokemon.Items.Add("Bulbasur");
            cboPokemon.Items.Add("Pikachu");
            cboPokemon.Items.Add("Bulbasur");
            */

            //Inicializando colores del comboBox
            cboColor.Items.Add("--Seleccione Color--");
            cboColor.Items.Add("Amarillo");
            cboColor.Items.Add("Verde");
            cboColor.Items.Add("Anaranjado");
            cboColor.Items.Add("Marron");
            cboColor.Items.Add("Rojo");

            cboColor.SelectedIndex = 0;
            cboPokemon.SelectedIndex = 0;
            cboEntrenador.SelectedIndex = 0;



            /*MessageBox.Show("Bienvenido al entorno Pokemon ...\n " +
                "Deberá crear sus pokemones y agregarlos a la lista.", "Mensaje");
                */
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try {
                string _nombre = cboPokemon.Text;
                string _especie = txtEspecie.Text;
                string _habilidad = txtHabilidad.Text;
                double _peso = Double.Parse(txtPeso.Text);
                double _altura = Double.Parse(txtAltura.Text);
                string _color = cboColor.SelectedItem.ToString();
                string _habitat = txtHabitat.Text;

                if (_nombre.Equals(string.Empty))
                {
                    MessageBox.Show("Debe seleccionar un Pokemon para crearlo");
                }
                else
                {
                    //Crear pokemon (Instanciar)
                    Pokemons pokemon1 = new Pokemons(_nombre, _especie, _habilidad, _peso, _altura, _color, _habitat);

                    //MessageBox.Show($"nombre: {pokemon1.Nombre}","titulo");
                    limpiarControles();

                    //Agregar el pokemos a la Grilla/Grid/Tabla/GridView
                    dgvListaPokemon.Rows.Add(++numFilas, pokemon1.Nombre, pokemon1.Especie, pokemon1.Habilidad, pokemon1.Peso, pokemon1.Altura, pokemon1.Color, pokemon1.Habitat);
                }

                //Elegir Pokemon Rival
                crearPokemonRival();

                //Imprimir
                mostrarDatosPokemonRival();
               
            }
            catch(Exception ex)
            {
                MessageBox.Show($"hubo un error: {ex.Message}");
            }
            
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }

        private void imgFotoPokemon_Click(object sender, EventArgs e)
        {

        }

        private void cboNombre_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _nombre = cboPokemon.SelectedItem.ToString();
            switch(_nombre)
            {
                case "Bulbasur":
                    imgFotoPokemon.ImageLocation = "C:/dasdadsd/img/bulbasur.jpg";
                    btnAgregar.Enabled=true;
                        break;
                case "Charizard":
                    imgFotoPokemon.ImageLocation = "C:/dasdadsd/img/charizard.jpg";
                    btnAgregar.Enabled = true;
                    break;
                case "Pikachu":
                    imgFotoPokemon.ImageLocation = "C:/dasdadsd/img/pikachu.jfif";
                    btnAgregar.Enabled = true;
                    break;
                case "Onix":
                    imgFotoPokemon.ImageLocation = "C:/dasdadsd/img/onix.jfif";
                    btnAgregar.Enabled = true;
                    break;
                default:
                    btnAgregar.Enabled = false;
                    imgFotoPokemon.ImageLocation= "";
                    break;



            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _nombre = cboEntrenador.SelectedItem.ToString();
            switch (_nombre)
            {
                case "Ash":
                    cboPokemon.Items.Clear();
                    cboPokemon.Items.Add("Pikachu");
                    cboPokemon.Items.Add("Bulbasur");
                    
                    imgFotoEntrenador.ImageLocation = RUTA_IMAGENES +"ash.png";
                    btnAgregar.Enabled = true;
                   

                    break;
                case "Brook":
                    cboPokemon.Items.Clear();
                    
                    imgFotoEntrenador.ImageLocation = RUTA_IMAGENES + "brook.jfif";
                    btnAgregar.Enabled = true;
                    break;
                case "Misty":
                    cboPokemon.Items.Clear();
                    cboPokemon.Items.Add("Charizard");
                    cboPokemon.Items.Add("Onix");
                    
                    imgFotoEntrenador.ImageLocation = RUTA_IMAGENES + "misty.jfif";
                    btnAgregar.Enabled = true;
                    
                    break;
                    
                default:
                    cboPokemon.Items.Add("--Seleccione Pokemon--");
                    
                    imgFotoEntrenador.ImageLocation = RUTA_IMAGENES + "incogen.png";
                    imgFotoPokemon.ImageLocation = RUTA_IMAGENES + "pokebola.jpg";
                    imgSistema.ImageLocation = RUTA_IMAGENES + "pokebola.jpg";
                    btnAgregar.Enabled = false;
                    


                    break;



            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void picturesistema_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }
    }
}
