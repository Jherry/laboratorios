﻿namespace Pokemons
{
    partial class frmPokemons
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtEspecie = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtHabilidad = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAltura = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtHabitat = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cboColor = new System.Windows.Forms.ComboBox();
            this.imgFotoPokemon = new System.Windows.Forms.PictureBox();
            this.dgvListaPokemon = new System.Windows.Forms.DataGridView();
            this.nro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.especie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.habilidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.peso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.altura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.color = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.habitat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cboPokemon = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.imgFotoEntrenador = new System.Windows.Forms.PictureBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.cboEntrenador = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtEspecieSistema = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtHabilidadSistema = new System.Windows.Forms.TextBox();
            this.imgSistema = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPesoSistema = new System.Windows.Forms.TextBox();
            this.txtHabitatSistema = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtAlturaSistema = new System.Windows.Forms.TextBox();
            this.txtNomSistema = new System.Windows.Forms.TextBox();
            this.txtColorSistema = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgFotoPokemon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaPokemon)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFotoEntrenador)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSistema)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre:";
            // 
            // txtEspecie
            // 
            this.txtEspecie.Location = new System.Drawing.Point(93, 81);
            this.txtEspecie.Name = "txtEspecie";
            this.txtEspecie.Size = new System.Drawing.Size(136, 20);
            this.txtEspecie.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Especie:";
            // 
            // txtHabilidad
            // 
            this.txtHabilidad.Location = new System.Drawing.Point(93, 115);
            this.txtHabilidad.Name = "txtHabilidad";
            this.txtHabilidad.Size = new System.Drawing.Size(136, 20);
            this.txtHabilidad.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Habilidad:";
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(93, 145);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(65, 20);
            this.txtPeso.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Peso (Kg):";
            // 
            // txtAltura
            // 
            this.txtAltura.Location = new System.Drawing.Point(93, 179);
            this.txtAltura.Name = "txtAltura";
            this.txtAltura.Size = new System.Drawing.Size(65, 20);
            this.txtAltura.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Altura:";
            // 
            // txtHabitat
            // 
            this.txtHabitat.Location = new System.Drawing.Point(93, 248);
            this.txtHabitat.Name = "txtHabitat";
            this.txtHabitat.Size = new System.Drawing.Size(136, 20);
            this.txtHabitat.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 251);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Hbitat:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Color:";
            // 
            // cboColor
            // 
            this.cboColor.FormattingEnabled = true;
            this.cboColor.Location = new System.Drawing.Point(93, 208);
            this.cboColor.Name = "cboColor";
            this.cboColor.Size = new System.Drawing.Size(121, 21);
            this.cboColor.TabIndex = 12;
            // 
            // imgFotoPokemon
            // 
            this.imgFotoPokemon.Location = new System.Drawing.Point(263, 49);
            this.imgFotoPokemon.Name = "imgFotoPokemon";
            this.imgFotoPokemon.Size = new System.Drawing.Size(182, 219);
            this.imgFotoPokemon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgFotoPokemon.TabIndex = 13;
            this.imgFotoPokemon.TabStop = false;
            this.imgFotoPokemon.Click += new System.EventHandler(this.imgFotoPokemon_Click);
            // 
            // dgvListaPokemon
            // 
            this.dgvListaPokemon.AllowUserToAddRows = false;
            this.dgvListaPokemon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListaPokemon.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nro,
            this.nombre,
            this.especie,
            this.habilidad,
            this.peso,
            this.altura,
            this.color,
            this.habitat});
            this.dgvListaPokemon.Location = new System.Drawing.Point(12, 608);
            this.dgvListaPokemon.Name = "dgvListaPokemon";
            this.dgvListaPokemon.Size = new System.Drawing.Size(706, 66);
            this.dgvListaPokemon.TabIndex = 14;
            // 
            // nro
            // 
            this.nro.HeaderText = "Nro";
            this.nro.Name = "nro";
            this.nro.Width = 50;
            // 
            // nombre
            // 
            this.nombre.HeaderText = "POKEMON";
            this.nombre.Name = "nombre";
            // 
            // especie
            // 
            this.especie.HeaderText = "ESPECIE";
            this.especie.Name = "especie";
            // 
            // habilidad
            // 
            this.habilidad.HeaderText = "HABILIDAD";
            this.habilidad.Name = "habilidad";
            // 
            // peso
            // 
            this.peso.HeaderText = "PESO";
            this.peso.Name = "peso";
            this.peso.Width = 50;
            // 
            // altura
            // 
            this.altura.HeaderText = "ALTURA";
            this.altura.Name = "altura";
            this.altura.Width = 60;
            // 
            // color
            // 
            this.color.HeaderText = "COLOR";
            this.color.Name = "color";
            // 
            // habitat
            // 
            this.habitat.HeaderText = "HABITAT";
            this.habitat.Name = "habitat";
            // 
            // btnAgregar
            // 
            this.btnAgregar.Enabled = false;
            this.btnAgregar.Location = new System.Drawing.Point(509, 237);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(136, 45);
            this.btnAgregar.TabIndex = 15;
            this.btnAgregar.Text = "Crear/Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(509, 288);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(136, 45);
            this.btnLimpiar.TabIndex = 16;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(540, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(259, 24);
            this.label8.TabIndex = 17;
            this.label8.Text = "MUNDO POKEMON - 2018";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // cboPokemon
            // 
            this.cboPokemon.FormattingEnabled = true;
            this.cboPokemon.Location = new System.Drawing.Point(93, 46);
            this.cboPokemon.Name = "cboPokemon";
            this.cboPokemon.Size = new System.Drawing.Size(154, 21);
            this.cboPokemon.TabIndex = 18;
            this.cboPokemon.SelectedIndexChanged += new System.EventHandler(this.cboNombre_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboPokemon);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtEspecie);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtHabilidad);
            this.groupBox1.Controls.Add(this.imgFotoPokemon);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cboColor);
            this.groupBox1.Controls.Add(this.txtPeso);
            this.groupBox1.Controls.Add(this.txtHabitat);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtAltura);
            this.groupBox1.Location = new System.Drawing.Point(12, 288);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(471, 302);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos de Pokemons";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.imgFotoEntrenador);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.cboEntrenador);
            this.groupBox2.Location = new System.Drawing.Point(12, 46);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(471, 236);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos de Entrenadores:";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(166, 78);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(31, 17);
            this.radioButton2.TabIndex = 11;
            this.radioButton2.Text = "F";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(117, 78);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(34, 17);
            this.radioButton1.TabIndex = 10;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "M";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 123);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Edad:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Genero:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 40);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Nombre Entrenador:";
            // 
            // imgFotoEntrenador
            // 
            this.imgFotoEntrenador.Location = new System.Drawing.Point(263, 19);
            this.imgFotoEntrenador.Name = "imgFotoEntrenador";
            this.imgFotoEntrenador.Size = new System.Drawing.Size(182, 211);
            this.imgFotoEntrenador.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgFotoEntrenador.TabIndex = 6;
            this.imgFotoEntrenador.TabStop = false;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(117, 123);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(57, 20);
            this.textBox2.TabIndex = 5;
            // 
            // cboEntrenador
            // 
            this.cboEntrenador.FormattingEnabled = true;
            this.cboEntrenador.Location = new System.Drawing.Point(117, 40);
            this.cboEntrenador.Name = "cboEntrenador";
            this.cboEntrenador.Size = new System.Drawing.Size(137, 21);
            this.cboEntrenador.TabIndex = 3;
            this.cboEntrenador.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(668, 251);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 78);
            this.label12.TabIndex = 21;
            this.label12.Text = "VS";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtColorSistema);
            this.groupBox3.Controls.Add(this.txtNomSistema);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.txtEspecieSistema);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.txtHabilidadSistema);
            this.groupBox3.Controls.Add(this.imgSistema);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txtPesoSistema);
            this.groupBox3.Controls.Add(this.txtHabitatSistema);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.txtAlturaSistema);
            this.groupBox3.Location = new System.Drawing.Point(816, 151);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(471, 302);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Datos de Pokemons Sistema";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Nombre:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 84);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Especie:";
            // 
            // txtEspecieSistema
            // 
            this.txtEspecieSistema.Location = new System.Drawing.Point(93, 81);
            this.txtEspecieSistema.Name = "txtEspecieSistema";
            this.txtEspecieSistema.ReadOnly = true;
            this.txtEspecieSistema.Size = new System.Drawing.Size(136, 20);
            this.txtEspecieSistema.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 118);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Habilidad:";
            // 
            // txtHabilidadSistema
            // 
            this.txtHabilidadSistema.Location = new System.Drawing.Point(93, 115);
            this.txtHabilidadSistema.Name = "txtHabilidadSistema";
            this.txtHabilidadSistema.ReadOnly = true;
            this.txtHabilidadSistema.Size = new System.Drawing.Size(136, 20);
            this.txtHabilidadSistema.TabIndex = 5;
            // 
            // imgSistema
            // 
            this.imgSistema.Location = new System.Drawing.Point(263, 49);
            this.imgSistema.Name = "imgSistema";
            this.imgSistema.Size = new System.Drawing.Size(182, 219);
            this.imgSistema.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgSistema.TabIndex = 13;
            this.imgSistema.TabStop = false;
            this.imgSistema.Click += new System.EventHandler(this.picturesistema_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 152);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "Peso (Kg):";
            // 
            // txtPesoSistema
            // 
            this.txtPesoSistema.Location = new System.Drawing.Point(93, 145);
            this.txtPesoSistema.Name = "txtPesoSistema";
            this.txtPesoSistema.ReadOnly = true;
            this.txtPesoSistema.Size = new System.Drawing.Size(65, 20);
            this.txtPesoSistema.TabIndex = 7;
            // 
            // txtHabitatSistema
            // 
            this.txtHabitatSistema.Location = new System.Drawing.Point(93, 248);
            this.txtHabitatSistema.Name = "txtHabitatSistema";
            this.txtHabitatSistema.ReadOnly = true;
            this.txtHabitatSistema.Size = new System.Drawing.Size(136, 20);
            this.txtHabitatSistema.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 186);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Altura:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 251);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 13);
            this.label18.TabIndex = 10;
            this.label18.Text = "Hbitat:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(16, 221);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 13);
            this.label19.TabIndex = 8;
            this.label19.Text = "Color:";
            // 
            // txtAlturaSistema
            // 
            this.txtAlturaSistema.Location = new System.Drawing.Point(93, 179);
            this.txtAlturaSistema.Name = "txtAlturaSistema";
            this.txtAlturaSistema.ReadOnly = true;
            this.txtAlturaSistema.Size = new System.Drawing.Size(65, 20);
            this.txtAlturaSistema.TabIndex = 9;
            // 
            // txtNomSistema
            // 
            this.txtNomSistema.Location = new System.Drawing.Point(93, 49);
            this.txtNomSistema.Name = "txtNomSistema";
            this.txtNomSistema.ReadOnly = true;
            this.txtNomSistema.Size = new System.Drawing.Size(145, 20);
            this.txtNomSistema.TabIndex = 14;
            // 
            // txtColorSistema
            // 
            this.txtColorSistema.Location = new System.Drawing.Point(93, 218);
            this.txtColorSistema.Name = "txtColorSistema";
            this.txtColorSistema.ReadOnly = true;
            this.txtColorSistema.Size = new System.Drawing.Size(136, 20);
            this.txtColorSistema.TabIndex = 15;
            // 
            // frmPokemons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1328, 695);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.dgvListaPokemon);
            this.Name = "frmPokemons";
            this.Text = "Crear pokemons - PLOP";
            this.Load += new System.EventHandler(this.frmPokemons_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgFotoPokemon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaPokemon)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFotoEntrenador)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSistema)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEspecie;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtHabilidad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAltura;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtHabitat;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboColor;
        private System.Windows.Forms.PictureBox imgFotoPokemon;
        private System.Windows.Forms.DataGridView dgvListaPokemon;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboPokemon;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox imgFotoEntrenador;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox cboEntrenador;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nro;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn especie;
        private System.Windows.Forms.DataGridViewTextBoxColumn habilidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn peso;
        private System.Windows.Forms.DataGridViewTextBoxColumn altura;
        private System.Windows.Forms.DataGridViewTextBoxColumn color;
        private System.Windows.Forms.DataGridViewTextBoxColumn habitat;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtEspecieSistema;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtHabilidadSistema;
        private System.Windows.Forms.PictureBox imgSistema;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPesoSistema;
        private System.Windows.Forms.TextBox txtHabitatSistema;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtAlturaSistema;
        private System.Windows.Forms.TextBox txtNomSistema;
        private System.Windows.Forms.TextBox txtColorSistema;
    }
}

