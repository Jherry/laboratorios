﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _08ConexionDataset
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void limpiarControles()
        {
            
                txtUsuario.Clear();
                txtClave.Clear();
                txtRol.Clear();
                txtemail.Clear();
                txtEstado.Clear();
            
        }
        private void uSUARIOBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.uSUARIOBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.db_senatiDataSet);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'db_senatiDataSet.USUARIO' Puede moverla o quitarla según sea necesario.
            this.uSUARIOTableAdapter.Fill(this.db_senatiDataSet.USUARIO);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            DataGridViewRow fila = uSUARIODataGridView.CurrentRow;
            int _id = Convert.ToInt32(uSUARIODataGridView[0, fila.Index].Value);

            limpiarControles();

            
            if (MessageBox.Show("Esta seguro de querer eliminar al usuario?", "Mensaje",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                this.uSUARIOTableAdapter.EliminarUsuario(_id);
                MessageBox.Show("Usuario eliminado con exito", "Titulo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
    
            this.uSUARIOTableAdapter.Fill(this.db_senatiDataSet.USUARIO);
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            string _usuario = txtUsuario.Text;
            string _clave = txtClave.Text;
            string _rol = txtRol.Text;
            string _email = txtemail.Text;
            string _estado = txtEstado.Text;


            this.uSUARIOTableAdapter.insertarUsuario(_usuario, _clave, _rol, _email, _estado);

            limpiarControles();

            MessageBox.Show("Usuario registrado exitosamente!","Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.uSUARIOTableAdapter.Fill(this.db_senatiDataSet.USUARIO);

            
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtUsuario.Clear();
            txtClave.Clear();
            txtRol.Clear();
            txtemail.Clear();
            txtEstado.Clear();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            string _usuario = txtUsuario.Text;
            string _clave = txtClave.Text;
            string _rol = txtRol.Text;
            string _email = txtemail.Text;
            string _estado = txtEstado.Text;

            //Obtener el Id de la fila seleccionada en el gridview

            DataGridViewRow fila =uSUARIODataGridView.CurrentRow;
            int _id = Convert.ToInt32(uSUARIODataGridView[0, fila.Index].Value);

            limpiarControles();

            this.uSUARIOTableAdapter.actualizarUsuario(_usuario, _clave, _rol, _email, _estado,_id);
            MessageBox.Show("Usuario actualizado exitosamente!", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.uSUARIOTableAdapter.Fill(this.db_senatiDataSet.USUARIO);

        }
    }
}
